const master = [
  //GROUP MASTER
  {
    path: '/master/approval-reason',
    name: 'master approval reason',
    component: () => import('@/views/master/approval-reason'),
  },
  {
    path: '/master/approval-reason/form',
    name: 'master approval reason form',
    component: () => import('@/views/master/approval-reason/form'),
  },
  {
    path: '/master/bank',
    name: 'master bank',
    component: () => import('@/views/master/bank'),
  },
  {
    path: '/master/bank/form',
    name: 'master bank form',
    component: () => import('@/views/master/bank/form'),
  },
  {
    path: '/master/bank-account',
    name: 'master bank account',
    component: () => import('@/views/master/bank-account'),
  },
  {
    path: '/master/bank-account/form',
    name: 'master bank account form',
    component: () => import('@/views/master/bank-account/form'),
  },
  {
    path: '/master/bill-of-material',
    name: 'master bill of material',
    component: () => import('@/views/master/bill-of-material'),
  },
  {
    path: '/master/bill-of-material/form',
    name: 'master bill of material form',
    component: () => import('@/views/master/bill-of-material/form'),
  },
  {
    path: '/master/branch',
    name: 'master branch',
    component: () => import('@/views/master/branch'),
  },
  {
    path: '/master/branch/form',
    name: 'master branch form',
    component: () => import('@/views/master/branch/form'),
  },
  {
    path: '/master/business-entity',
    name: 'master business-entity',
    component: () => import('@/views/master/business-entity'),
  },
  {
    path: '/master/business-entity/form',
    name: 'master business-entity form',
    component: () => import('@/views/master/business-entity/form'),
  },
  {
    path: '/master/cash-account',
    name: 'master cash account',
    component: () => import('@/views/master/cash-account'),
  },
  {
    path: '/master/cash-account/form',
    name: 'master cash account form',
    component: () => import('@/views/master/cash-account/form'),
  },
  {
    path: '/master/chart-of-account',
    name: 'master chart of account',
    component: () => import('@/views/master/chart-of-account'),
  },
  {
    path: '/master/chart-of-account/form',
    name: 'master chart of account form',
    component: () => import('@/views/master/chart-of-account/form'),
  },
  {
    path: '/master/city',
    name: 'master city',
    component: () => import('@/views/master/city'),
  },
  {
    path: '/master/city/form',
    name: 'master city form',
    component: () => import('@/views/master/city/form'),
  },
  {
    path: '/master/color',
    name: 'master color',
    component: () => import('@/views/master/color'),
  },
  {
    path: '/master/color/form',
    name: 'master color form',
    component: () => import('@/views/master/color/form'),
  },
  {
    path: '/master/country',
    name: 'master country',
    component: () => import('@/views/master/country'),
  },
  {
    path: '/master/country/form',
    name: 'master country form',
    component: () => import('@/views/master/country/form'),
  },
  {
    path: '/master/currency',
    name: 'master currency',
    component: () => import('@/views/master/currency'),
  },
  {
    path: '/master/currency/form',
    name: 'master currency form',
    component: () => import('@/views/master/currency/form'),
  },
  {
    path: '/master/customer',
    name: 'master customer',
    component: () => import('@/views/master/customer'),
  },
  {
    path: '/master/customer/form',
    name: 'master customer form',
    component: () => import('@/views/master/customer/form'),
  },
  {
    path: '/master/customer-category',
    name: 'master customer category',
    component: () => import('@/views/master/customer-category'),
  },
  {
    path: '/master/customer-category/form',
    name: 'master customer category form',
    component: () => import('@/views/master/customer-category/form'),
  },
  {
    path: '/master/customer-deposit-type',
    name: 'master customer deposit type category',
    component: () => import('@/views/master/customer-deposit-type'),
  },
  {
    path: '/master/customer-deposit-type/form',
    name: 'master customer deposit type form',
    component: () => import('@/views/master/customer-deposit-type/form'),
  },
  {
    path: '/master/customer-jn-address',
    name: 'master customer jn address',
    component: () => import('@/views/master/customer-jn-address'),
  },
  {
    path: '/master/customer-jn-address/form',
    name: 'master customer jn address form',
    component: () => import('@/views/master/customer-jn-address/form'),
  },
  {
    path: '/master/customer-jn-contact',
    name: 'master customer jn contact',
    component: () => import('@/views/master/customer-jn-contact'),
  },
  {
    path: '/master/customer-jn-contact/form',
    name: 'master customer jn contact form',
    component: () => import('@/views/master/customer-jn-contact/form'),
  },
  {
    path: '/master/department',
    name: 'master department',
    component: () => import('@/views/master/department'),
  },
  {
    path: '/master/department/form',
    name: 'master department form',
    component: () => import('@/views/master/department/form'),
  },
  {
    path: '/master/discount-type',
    name: 'master discount type',
    component: () => import('@/views/master/discount-type'),
  },
  {
    path: '/master/discount-type/form',
    name: 'master discount type form',
    component: () => import('@/views/master/discount-type/form'),
  },
  {
    path: '/master/distribution-channel',
    name: 'master distribution channel',
    component: () => import('@/views/master/distribution-channel'),
  },
  {
    path: '/master/distribution-channel/form',
    name: 'master distribution channel form',
    component: () => import('@/views/master/distribution-channel/form'),
  },
  {
    path: '/master/division',
    name: 'master division',
    component: () => import('@/views/master/division'),
  },
  {
    path: '/master/division/form',
    name: 'master division form',
    component: () => import('@/views/master/division/form'),
  },
  {
    path: '/master/document-type',
    name: 'master document type',
    component: () => import('@/views/master/document-type'),
  },
  {
    path: '/master/document-type/form',
    name: 'master document type form',
    component: () => import('@/views/master/document-type/form'),
  },
  {
    path: '/master/education',
    name: 'master education',
    component: () => import('@/views/master/education'),
  },
  {
    path: '/master/education/form',
    name: 'master education form',
    component: () => import('@/views/master/education/form'),
  },
  {
    path: '/master/employee',
    name: 'master employee',
    component: () => import('@/views/master/employee'),
  },
  {
    path: '/master/employee/form',
    name: 'master employee form',
    component: () => import('@/views/master/employee/form'),
  },
  {
    path: '/master/employee-type',
    name: 'master employee type',
    component: () => import('@/views/master/employee-type'),
  },
  {
    path: '/master/employee-type/form',
    name: 'master employee type form',
    component: () => import('@/views/master/employee-type/form'),
  },
  {
    path: '/master/exchange-rate',
    name: 'master exchange rate',
    component: () => import('@/views/master/exchange-rate'),
  },
  {
    path: '/master/inventory-adj-reason',
    name: 'master inventory adj reason',
    component: () => import('@/views/master/inventory-adj-reason'),
  },
  {
    path: '/master/inventory-adj-reason/form',
    name: 'master inventory adj reason form',
    component: () => import('@/views/master/inventory-adj-reason/form'),
  },
  {
    path: '/master/inventory-adj-reason-jn-branch',
    name: 'master inventory adj reason jn branch',
    component: () => import('@/views/master/inventory-adj-reason-jn-branch'),
  },
  {
    path: '/master/inventory-adj-reason-jn-branch/form',
    name: 'master inventory adj reason jn branch form',
    component: () => import('@/views/master/inventory-adj-reason-jn-branch/form'),
  },
  {
    path: '/master/island',
    name: 'master island',
    component: () => import('@/views/master/island'),
  },
  {
    path: '/master/island/form',
    name: 'master island form',
    component: () => import('@/views/master/island/form'),
  },
  {
    path: '/master/item',
    name: 'master item',
    component: () => import('@/views/master/item'),
  },
  {
    path: '/master/item/form',
    name: 'master item form',
    component: () => import('@/views/master/item/form'),
  },
  {
    path: '/master/item-brand',
    name: 'master item brand',
    component: () => import('@/views/master/item-brand'),
  },
  {
    path: '/master/item-brand/form',
    name: 'master item brand form',
    component: () => import('@/views/master/item-brand/form'),
  },
  {
    path: '/master/item-category',
    name: 'master item category',
    component: () => import('@/views/master/item-category'),
  },
  {
    path: '/master/item-category/form',
    name: 'master item category form',
    component: () => import('@/views/master/item-category/form'),
  },
  {
    path: '/master/item-classification',
    name: 'master item classification',
    component: () => import('@/views/master/item-classification'),
  },
  {
    path: '/master/item-classification/form',
    name: 'master item classification form',
    component: () => import('@/views/master/item-classification/form'),
  },
  {
    path: '/master/item-division',
    name: 'master item division',
    component: () => import('@/views/master/item-division'),
  },
  {
    path: '/master/item-division/form',
    name: 'master item division form',
    component: () => import('@/views/master/item-division/form'),
  },
  {
    path: '/master/item-jn-catalog-number',
    name: 'master item jn catalog number',
    component: () => import('@/views/master/item-jn-catalog-number'),
  },
  {
    path: '/master/item-jn-catalog-number/form',
    name: 'master item jn catalog number form',
    component: () => import('@/views/master/item-jn-catalog-number/form'),
  },
  // {
  //   path: '/master/item-jn-cogsidr',
  //   name: 'master item jn cogsidr',
  //   component: () => import('@/views/master/item-jn-cogsidr'),
  // },
  // {
  //   path: '/master/item-jn-cogsidr/form',
  //   name: 'master item jn cogsidr form',
  //   component: () => import('@/views/master/item-jn-cogsidr/form'),
  // },
  {
    path: '/master/item-sub-category',
    name: 'master item sub category',
    component: () => import('@/views/master/item-sub-category'),
  },
  {
    path: '/master/item-sub-category/form',
    name: 'master item sub category form',
    component: () => import('@/views/master/item-sub-category/form'),
  },
  {
    path: '/master/job-position',
    name: 'master job position',
    component: () => import('@/views/master/job-position'),
  },
  {
    path: '/master/job-position/form',
    name: 'master job position form',
    component: () => import('@/views/master/job-position/form'),
  },
  {
    path: '/master/journal',
    name: 'master journal',
    component: () => import('@/views/master/journal'),
  },
  {
    path: '/master/journal/form',
    name: 'master journal form',
    component: () => import('@/views/master/journal/form'),
  },
  {
    path: '/master/journal-chart-of-account',
    name: 'master journal chart of account',
    component: () => import('@/views/master/journal-chart-of-account'),
  },
  {
    path: '/master/journal-chart-of-account/form',
    name: 'master journal chart of account form',
    component: () => import('@/views/master/journal-chart-of-account/form'),
  },
  {
    path: '/master/journal-posting-type',
    name: 'master journal posting type',
    component: () => import('@/views/master/journal-posting-type'),
  },
  {
    path: '/master/journal-posting-type/form',
    name: 'master journal posting type form',
    component: () => import('@/views/master/journal-posting-type/form'),
  },
  {
    path: '/master/journal-type',
    name: 'master journal type',
    component: () => import('@/views/master/journal-type'),
  },
  {
    path: '/master/journal-type/form',
    name: 'master journal type form',
    component: () => import('@/views/master/journal-type/form'),
  },
  {
    path: '/master/marital-status',
    name: 'master marital status',
    component: () => import('@/views/master/marital-status'),
  },
  {
    path: '/master/marital-status/form',
    name: 'master marital status form',
    component: () => import('@/views/master/marital-status/form'),
  },
  {
    path: '/master/minus-stock/form',
    name: 'master minus stock form',
    component: () => import('@/views/master/minus-stock/form'),
  },
  {
    path: '/master/payment-term',
    name: 'master payment term',
    component: () => import('@/views/master/payment-term'),
  },
  {
    path: '/master/payment-term/form',
    name: 'master payment term form',
    component: () => import('@/views/master/payment-term/form'),
  },
  {
    path: '/master/price-list',
    name: 'master price list',
    component: () => import('@/views/master/price-list'),
  },
  {
    path: '/master/price-list/form',
    name: 'master price list form',
    component: () => import('@/views/master/price-list/form'),
  },
  {
    path: '/master/price-list-item',
    name: 'master price list item',
    component: () => import('@/views/master/price-list-item'),
  },
  {
    path: '/master/price-list-item/form',
    name: 'master price list item form',
    component: () => import('@/views/master/price-list-item/form'),
  },
  {
    path: '/master/price-type',
    name: 'master price type',
    component: () => import('@/views/master/price-type'),
  },
  {
    path: '/master/price-type/form',
    name: 'master price type form',
    component: () => import('@/views/master/price-type/form'),
  },
  {
    path: '/master/project',
    name: 'master project',
    component: () => import('@/views/master/project'),
  },
  {
    path: '/master/project/form',
    name: 'master project form',
    component: () => import('@/views/master/project/form'),
  },
  {
    path: '/master/project-parent',
    name: 'master project parent',
    component: () => import('@/views/master/project-parent'),
  },
  {
    path: '/master/project-parent/form',
    name: 'master project parent form',
    component: () => import('@/views/master/project-parent/form'),
  },
  {
    path: '/master/province',
    name: 'master province',
    component: () => import('@/views/master/province'),
  },
  {
    path: '/master/province/form',
    name: 'master province form',
    component: () => import('@/views/master/province/form'),
  },
  {
    path: '/master/purchase-destination',
    name: 'master purchase destination',
    component: () => import('@/views/master/purchase-destination'),
  },
  {
    path: '/master/purchase-destination/form',
    name: 'master purchase destination form',
    component: () => import('@/views/master/purchase-destination/form'),
  },
  {
    path: '/master/rack',
    name: 'master rack',
    component: () => import('@/views/master/rack'),
  },
  {
    path: '/master/rack/form',
    name: 'master rack form',
    component: () => import('@/views/master/rack/form'),
  },
  {
    path: '/master/religion',
    name: 'master religion',
    component: () => import('@/views/master/religion'),
  },
  {
    path: '/master/religion/form',
    name: 'master religion form',
    component: () => import('@/views/master/religion/form'),
  },
  {
    path: '/master/sales-person',
    name: 'master sales person',
    component: () => import('@/views/master/sales-person'),
  },
  {
    path: '/master/sales-person/form',
    name: 'master sales person form',
    component: () => import('@/views/master/sales-person/form'),
  },
  {
    path: '/master/sales-price-list',
    name: 'master sales price list',
    component: () => import('@/views/master/sales-price-list'),
  },
  {
    path: '/master/sales-price-list/form',
    name: 'master sales price list form',
    component: () => import('@/views/master/sales-price-list/form'),
  },
  {
    path: '/master/sales-price-list-approval',
    name: 'master sales price list approval',
    component: () => import('@/views/master/sales-price-list-approval'),
  },
  {
    path: '/master/search-document',
    name: 'master search document',
    component: () => import('@/views/master/search-document'),
  },
  {
    path: '/master/search-document/form',
    name: 'master search document form',
    component: () => import('@/views/master/search-document/form'),
  },
  {
    path: '/master/supervisory',
    name: 'master supervisory',
    component: () => import('@/views/master/supervisory'),
  },
  {
    path: '/master/supervisory/form',
    name: 'master supervisory form',
    component: () => import('@/views/master/supervisory/form'),
  },
  {
    path: '/master/unit-of-measure',
    name: 'master unit of measure',
    component: () => import('@/views/master/unit-of-measure'),
  },
  {
    path: '/master/unit-of-measure/form',
    name: 'master unit of measure form',
    component: () => import('@/views/master/unit-of-measure/form'),
  },
  {
    path: '/master/vendor',
    name: 'master vendor',
    component: () => import('@/views/master/vendor'),
  },
  {
    path: '/master/vendor/form',
    name: 'master vendor form',
    component: () => import('@/views/master/vendor/form'),
  },
  {
    path: '/master/vendor-category',
    name: 'master vendor category',
    component: () => import('@/views/master/vendor-category'),
  },
  {
    path: '/master/vendor-category/form',
    name: 'master vendor category form',
    component: () => import('@/views/master/vendor-category/form'),
  },
  {
    path: '/master/vendor-deposit-type',
    name: 'master vendor deposit type',
    component: () => import('@/views/master/vendor-deposit-type'),
  },
  {
    path: '/master/vendor-deposit-type/form',
    name: 'master vendor deposit type form',
    component: () => import('@/views/master/vendor-deposit-type/form'),
  },
  {
    path: '/master/vendor-jn-contact',
    name: 'master vendor jn contact',
    component: () => import('@/views/master/vendor-jn-contact'),
  },
  {
    path: '/master/vendor-jn-contact/form',
    name: 'master vendor jn contact form',
    component: () => import('@/views/master/vendor-jn-contact/form'),
  },
  {
    path: '/master/warehouse',
    name: 'master warehouse',
    component: () => import('@/views/master/warehouse'),
  },
  {
    path: '/master/warehouse/form',
    name: 'master warehouse form',
    component: () => import('@/views/master/warehouse/form'),
  },
];

export default master;
