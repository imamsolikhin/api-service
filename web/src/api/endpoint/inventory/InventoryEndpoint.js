export const searchInventoryObjectDefault = (module, data) => {
  return `/${module}/search?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const searchInventoryDefault = (module, data) => {
  return `/${module}/search?qry=`+JSON.stringify(data).replaceAll("{","%7B").replaceAll("}","%7D").replaceAll("[","%5B").replaceAll("]","%5D")
}

export const lookupInventoryDefault = (module, path, data) => {
  return `/${module}/`+path+'?'+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}
 
export const dataInventoryDefault = (module, data) => {
  return `/${module}/data?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const saveInventoryDefault = (module) => {
  return `/${module}/save`
}

export const updateInventoryDefault = (module) => {
  return `/${module}/update`
}

export const deleteInventoryDefault = (module,data) => {
  return `/${module}/delete?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const authInventoryPrint = (module) => {
  return `/${module}/request`
}
