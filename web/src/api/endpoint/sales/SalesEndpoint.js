export const searchSalesDefault = (module, data) => {
  return `/${module}/search?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const searchDataArray = (module, data) => {
  return `/${module}/search?qry=`+JSON.stringify(data).replaceAll("{","%7B").replaceAll("}","%7D").replaceAll("[","%5B").replaceAll("]","%5D")
}
 
export const lookupSalesDefault = (module, data) => {
  return `/${module}/search?qry=`+JSON.stringify(data).replaceAll("{","%7B").replaceAll("}","%7D").replaceAll("[","%5B").replaceAll("]","%5D")
}
 
export const dataSalesDefault = (module, data) => {
  return `/${module}/data?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const saveSalesDefault = (module) => {
  return `/${module}/save`
}

export const updateSalesDefault = (module) => {
  return `/${module}/update`
}

export const deleteSalesDefault = (module,data) => {
  return `/${module}/delete?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const authSalesPrint = (module) => {
  return `/${module}/request`
}
