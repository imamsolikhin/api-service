// export const searchFinanceDefault = (module, data) => {
//   return `/${module}/search?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
// }
export const searchFinanceDefault = (module, data) => {
  return `/${module}/search?qry=`+JSON.stringify(data).replaceAll("{","%7B").replaceAll("}","%7D").replaceAll("[","%5B").replaceAll("]","%5D")
}

export const lookupFinanceDefault = (module, path, data) => {
  return `/${module}/`+path+'?'+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}
 
export const dataFinanceDefault = (module, data) => {
  return `/${module}/data?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const saveFinanceDefault = (module) => {
  return `/${module}/save`
}

export const updateFinanceDefault = (module) => {
  return `/${module}/update`
}

export const deleteFinanceDefault = (module,data) => {
  return `/${module}/delete?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const authFinancePrint = (module) => {
  return `/${module}/request`
}
