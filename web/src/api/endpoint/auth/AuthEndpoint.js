export const Login = () => {
  return `/v1/authenticate`
}

export const Validates = () => {
  return `/v1/validate`
}

export const LoadMenu = () => {
  return `/menus`
}
