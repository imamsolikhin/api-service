import axios from 'axios';
import { searchPurchaseDefault, lookupPurchaseDefault, savePurchaseDefault, dataPurchaseDefault, updatePurchaseDefault, deletePurchaseDefault, authPurchasePrint } from '@/api/endpoint/purchase/PurchaseEndpoint';
import { BasePurchaseUrl, BasePrintPurchaseUrl } from '@/api/endpoint/base';
import { store } from '@/store/index.js';

export const searchPurchaseDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BasePurchaseUrl()}${searchPurchaseDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const lookupPurchaseDefaultRequest = (module, path, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BasePurchaseUrl()}${lookupPurchaseDefault(module, path, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const dataPurchaseDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BasePurchaseUrl()}${dataPurchaseDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const savePurchaseDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BasePurchaseUrl()}${savePurchaseDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const updatePurchaseDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BasePurchaseUrl()}${updatePurchaseDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const deletePurchaseDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BasePurchaseUrl()}${deletePurchaseDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const authPurchasePrintRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BasePrintPurchaseUrl()}${authPurchasePrint(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
    data: data,
  }).then((response) => {
    return response;
  });
  return resp;
};
