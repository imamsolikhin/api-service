import axios from 'axios';
import { searchFinanceDefault, lookupFinanceDefault, saveFinanceDefault, dataFinanceDefault, updateFinanceDefault, deleteFinanceDefault, authFinancePrint } from '@/api/endpoint/finance/FinanceEndpoint';
import { BaseFinanceUrl, BasePrintFinanceUrl } from '@/api/endpoint/base';
import { store } from '@/store/index.js';

export const searchFinanceObjectRequest = (module, params, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseFinanceUrl()}${searchFinanceDefault(module, params)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const searchFinanceDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseFinanceUrl()}${searchFinanceDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const lookupFinanceDefaultRequest = (module, path, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseFinanceUrl()}${lookupFinanceDefault(module, path, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const dataFinanceDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseFinanceUrl()}${dataFinanceDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const saveFinanceDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseFinanceUrl()}${saveFinanceDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const updateFinanceDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseFinanceUrl()}${updateFinanceDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const deleteFinanceDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BaseFinanceUrl()}${deleteFinanceDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const authFinancePrintRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BasePrintFinanceUrl()}${authFinancePrint(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
    data: data,
  }).then((response) => {
    return response;
  });
  return resp;
};
