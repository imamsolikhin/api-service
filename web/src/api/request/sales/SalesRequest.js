import axios from 'axios';
import { searchSalesDefault, searchDataArray, lookupSalesDefault, saveSalesDefault, dataSalesDefault, updateSalesDefault, deleteSalesDefault, authSalesPrint } from '@/api/endpoint/sales/SalesEndpoint';
import { BaseSalesUrl, BasePrintSalesUrl } from '@/api/endpoint/base';
import { store } from '@/store/index.js';

export const searchSalesObjectRequest = (module, params, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseSalesUrl()}${searchSalesDefault(module, params)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const searchSalesDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseSalesUrl()}${searchSalesDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const searchDataArrayRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseSalesUrl()}${searchDataArray(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const lookupSalesDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseSalesUrl()}${lookupSalesDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const dataSalesDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseSalesUrl()}${dataSalesDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const saveSalesDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseSalesUrl()}${saveSalesDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const updateSalesDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseSalesUrl()}${updateSalesDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const deleteSalesDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BaseSalesUrl()}${deleteSalesDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const authSalesPrintRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BasePrintSalesUrl()}${authSalesPrint(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
    data: data,
  }).then((response) => {
    return response;
  });
  return resp;
};
