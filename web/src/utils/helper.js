import moment from 'moment';

export const SPV_CODE = "SPV_01"
export const MANAGERIAL_CODE = "AMBRT"
export const DIRECTORATE_CODE = "BOD_OPR"
export const SALES_PERSON_CODE = "SLS001"
export const SALES_ORDER_ITEM_CATEGORY = "UNIT"
export const dateToDateString = (
    date,
    shortMonth = true,
    time = false
) => {
    const format = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09"];
    const month = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    ];
    const shortMonthData = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "Mei",
        "Jun",
        "Jul",
        "Agu",
        "Sep",
        "Okt",
        "Nov",
        "Des"
    ];
    date = new Date(date);
    return date
        ? `${date.getDate().toString().length === 1
            ? format[date.getDate()]
            : date.getDate()
        } ${shortMonth ? shortMonthData[date.getMonth()] : month[date.getMonth()]
        } ${date.getFullYear()}${time ? `, ${dateToTime(date)}` : ""}`
        : "";
};

export const dateToTime = (date) => {
    const format = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09"];
    date = new Date(date);
    return `${date.getHours().toString().length === 1
        ? format[date.getHours()]
        : date.getHours()
        }:${date.getMinutes().toString().length === 1
            ? format[date.getMinutes()]
            : date.getMinutes()
        }`;
};

export const filterDates = (event) => {
    let startDate = null;
    let endDate = null;
    if (event === 0) {
        startDate = moment().format('YYYY-MM-DD')
        endDate = moment().format('YYYY-MM-DD')
    } else if (event === 1) {
        startDate = moment().subtract(1, 'days').format('YYYY-MM-DD')
        endDate = moment().format('YYYY-MM-DD')
    } else if (event === 2) {
        startDate = moment().subtract(6, 'days').format('YYYY-MM-DD')
        endDate = moment().format('YYYY-MM-DD')
    } else if (event === 3) {
        startDate = moment().subtract(31, 'days').format('YYYY-MM-DD')
        endDate = moment().format('YYYY-MM-DD')
    }

    return {
        startDate: startDate,
        endDate: endDate
    }
}

export const filterQuickDates = (event) => {
    console.log(event)
    let startDate = null;
    let endDate = null;
    if (event === 0) {
        startDate = moment().subtract(7, 'days').format('YYYY-MM-DD')
        endDate = moment().format('YYYY-MM-DD')
    } else if (event === 1) {
        startDate = moment().subtract(30, 'days').format('YYYY-MM-DD')
        endDate = moment().format('YYYY-MM-DD')
    } else if (event === 2) {
        startDate = moment().subtract(90, 'days').format('YYYY-MM-DD')
        endDate = moment().format('YYYY-MM-DD')
    }

    return {
        startDate: startDate,
        endDate: endDate
    }
}

export const formatPrice = (price) => {
    if (!price) return "Rp. 0";
    const result = price.toLocaleString("id");
    return `Rp. ${result}`;
};

export const decimalCurrency = (val) => {
    return val?.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

export const MONTH_NAMES = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
];

export const camelToNormalCase = (val) => {
    const result = val.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);

    return finalResult;
}

export const formatInputFloat = (val, numberOfDecimal = 2) => {
    const regexNumberDecimal = new RegExp(
      `^(\\d+,?\\d{0,${numberOfDecimal}})\\d*$`
    );

    val = val.toString();
    val = val
      .replace(/[^0-9,]+/g, "")
      .replace(regexNumberDecimal, "$1")
      .replace(/,+/g, ",")
      .replace(/,(\d+),/g, ",$1");
    const formattedVal = `${parseFloat(val.split(",")[0]).toLocaleString("id")}${
      val.split(",")[1] !== undefined ? `,${val.split(",")[1]}` : ""
    }`;
    return val ? (!formattedVal.includes("NaN") ? formattedVal : val) : val;
  };

export const formatMoney = (val) => {
    if(val) {
        const parts = val.split(".");
        const v = parts[0].replace(/\D/g, ""),
        dec = parts[1]

        let n = new Intl.NumberFormat('en-EN').format(v);
        n = dec !== undefined ? n + "." + dec : n;
        return n;
    } else {
        return 0
    }
}
