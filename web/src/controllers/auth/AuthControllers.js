import { authRequest, accountRequest, loadMenuRequest } from '@/api/request/auth/AuthRequest'
import { setAuthentication, setPeriodMonth, setPeriodYear } from '@/utils/cookies'
import { store,checkErrorResponse } from "@/store/index.js"

export class AuthControllers {
		authorization(username, password, month, year) {
				let resp = null
				resp = authRequest(username, password)
						.then((response) => {
								this.setLoginDetail(response.data, month, year)
								store.dispatch("storeAuth", true);
								store.dispatch("storeBlock", false);
						}).catch((err) => {
								checkErrorResponse(err)
						}).finally(() => {
						})
				return resp
		}

		account() {
				let resp = null
				resp = accountRequest()
						.then((response) => {
								this.setSetup(response.data)
								store.dispatch("storeAuth", true);
								store.dispatch("storeBlock", false);
						}).catch((err) => {
							checkErrorResponse(err)
						}).finally(() => {
						})
				return resp
		}

		async loadMenu() {
				let resp = null
				resp = await loadMenuRequest()
						.then((response) => {
								return response.data?.data;
						}).catch((err) => {
								checkErrorResponse(err)
						}).finally(() => {
						})
				return resp
		}

		setLoginDetail(token, month, year) {
				setAuthentication(token, month, year);
				store.dispatch("storeToken", token);
				setPeriodMonth(month);
				store.dispatch("storePeriodMonth", month);
				setPeriodYear(year);
				store.dispatch("storePeriodYear", year);
		}

		setSetup(setup) {
			if(setup){
				store.dispatch("storeSetup", setup);
			}
		}
}
