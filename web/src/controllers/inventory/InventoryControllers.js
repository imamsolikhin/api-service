import {
  searchInventoryDefaultRequest,
  searchInventoryObjectRequest,
  lookupInventoryDefaultRequest,
  dataInventoryDefaultRequest,
  saveInventoryDefaultRequest,
  updateInventoryDefaultRequest,
  deleteInventoryDefaultRequest,
  authInventoryPrintRequest,
} from '@/api/request/inventory/InventoryRequest';
import { checkErrorResponse } from '@/store';

export class InventoryControllers {
  searchInventoryObject(module, params, data) {
    let resp = null;
    resp = searchInventoryObjectRequest(module, params, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  searchInventory(module, data) {
    let resp = null;
    resp = searchInventoryDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  lookupInventory(module, data) {
    let resp = null;
    resp = lookupInventoryDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  dataInventory(module, data) {
    let resp = null;
    resp = dataInventoryDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  saveInventory(module, data) {
    let resp = null;
    resp = saveInventoryDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  updateInventory(module, data) {
    let resp = null;
    resp = updateInventoryDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  deleteInventory(module, data) {
    let resp = null;
    resp = deleteInventoryDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  printInventory(module, data) {
    let resp = null;
    resp = authInventoryPrintRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }
}
