package com.portoloop.oauth.service;

import com.portoloop.oauth.dao.UserDao;
import com.portoloop.oauth.repo.UserJnBankAccountRepo;
import com.portoloop.oauth.repo.UserJnBranchRepo;
import com.portoloop.oauth.repo.UserJnCashAccountRepo;
import com.portoloop.oauth.repo.UserJnDivisionRepo;
import com.portoloop.oauth.repo.UserJnWarehouseRepo;
import com.portoloop.core.enums.CustomException;
import com.portoloop.oauth.repo.UserRepo;
import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.model.User;
import com.portoloop.core.model.UserJnBankAccount;
import com.portoloop.core.model.UserJnBranch;
import com.portoloop.core.model.UserJnCashAccount;
import com.portoloop.core.model.UserJnDivision;
import com.portoloop.core.model.UserJnWarehouse;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService extends BaseQuery {

  @Autowired
  private UserRepo repo;
  @Autowired
  private UserJnBranchRepo userBranchRepo;
  @Autowired
  private UserJnWarehouseRepo userWarehouseRepo;
  @Autowired
  private UserJnCashAccountRepo userCashAccountRepo;
  @Autowired
  private UserJnBankAccountRepo userBankAccountRepo;
  @Autowired
  private UserJnDivisionRepo userDivisionRepo;

  @Autowired
  private UserDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingBranch(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingBranch(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  public PaginatedResults pagingWarehouse(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingWarehouse(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  public PaginatedResults pagingDivision(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDivision(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  public PaginatedResults pagingCashAccount(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingCashAccount(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  public PaginatedResults pagingBankAccount(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingBankAccount(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public User save(User model) {
    try {
      if (model.getPassword().equals("inkombizz")) {
        User user = repo.findByCode(model.getCode());
        model.setPassword(user.getPassword());
      } else {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(model.getPassword());
        model.setPassword(encodedPassword);
      }
      repo.save(model);

      userBranchRepo.deleteByUserCode(model.getUsername());
      int i = 1;
      if (model.getListBranch().size() > 0) {
        for (UserJnBranch detail : model.getListBranch()) {
          detail.setBranchCode(detail.getCode());
          detail.setUserCode(model.getUsername());
          detail.setCode(model.getUsername() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
          userBranchRepo.save(detail);
          i++;
        }
      }

      userWarehouseRepo.deleteByUserCode(model.getUsername());
      i = 1;
      if (model.getListWarehouse().size() > 0) {
        for (UserJnWarehouse detail : model.getListWarehouse()) {
          detail.setWarehouseCode(detail.getCode());
          detail.setUserCode(model.getUsername());
          detail.setCode(model.getUsername() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
          userWarehouseRepo.save(detail);
          i++;
        }
      }

      userCashAccountRepo.deleteByUserCode(model.getUsername());
      i = 1;
      if (model.getListCashAccount().size() > 0) {
        for (UserJnCashAccount detail : model.getListCashAccount()) {
          detail.setCashAccountCode(detail.getCode());
          detail.setUserCode(model.getUsername());
          detail.setCode(model.getUsername() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
          userCashAccountRepo.save(detail);
          i++;
        }
      }

      userBankAccountRepo.deleteByUserCode(model.getUsername());
      i = 1;
      if (model.getListBankAccount().size() > 0) {
        for (UserJnBankAccount detail : model.getListBankAccount()) {
          detail.setBankAccountCode(detail.getCode());
          detail.setUserCode(model.getUsername());
          detail.setCode(model.getUsername() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
          userBankAccountRepo.save(detail);
          i++;
        }
      }
      
      userDivisionRepo.deleteByUserCode(model.getUsername());
      i = 1;
      if (model.getListDivision().size() > 0) {
        for (UserJnDivision detail : model.getListDivision()) {
          detail.setDivisionCode(detail.getCode());
          detail.setUserCode(model.getUsername());
          detail.setCode(model.getUsername() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
          userDivisionRepo.save(detail);
          i++;
        }
      }
      return model;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void delete(String code) {
    try {
      repo.deleteByCode(code);
      userBranchRepo.deleteByUserCode(code);
      userWarehouseRepo.deleteByUserCode(code);
      userCashAccountRepo.deleteByUserCode(code);
      userBankAccountRepo.deleteByUserCode(code);
      userDivisionRepo.deleteByUserCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
