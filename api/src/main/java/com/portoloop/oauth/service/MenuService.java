/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.oauth.service;

import com.portoloop.core.model.MenuDto;
import com.portoloop.oauth.dao.MenuDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author imamsolikhin
 */
@Service
public class MenuService {

  @Autowired
  private MenuDao menuDao;
  
  public List<MenuDto> loadMenu(String username) {
    return menuDao.menu(username);
  }
}
