package com.portoloop.oauth.service;

import com.portoloop.oauth.dao.RoleAuthorizationDao;
import com.portoloop.oauth.repo.RoleAuthorizationRepo;
import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.enums.CustomException;
import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.model.RoleAuthorization;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleAuthorizationService extends BaseQuery{

  @Autowired
  private RoleAuthorizationRepo repo;

  @Autowired
  
  private RoleAuthorizationDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void updateAssignAuthority(String code, boolean status) {
    try {
      RoleAuthorization model = repo.findByCode(code);
      model.setAssignAuthority(status);
      
      session = em.unwrap(org.hibernate.Session.class);
      session.merge(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void updateSaveAuthority(String code, boolean status) {
    try {
      RoleAuthorization model = repo.findByCode(code);
      model.setSaveAuthority(status);
      
      session = em.unwrap(org.hibernate.Session.class);
      session.merge(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void updateUpdateAuthority(String code, boolean status) {
    try {
      RoleAuthorization model = repo.findByCode(code);
      model.setUpdateAuthority(status);
      
      session = em.unwrap(org.hibernate.Session.class);
      session.merge(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void updateDeleteAuthority(String code, boolean status) {
    try {
      RoleAuthorization model = repo.findByCode(code);
      model.setDeleteAuthority(status);
      
      session = em.unwrap(org.hibernate.Session.class);
      session.merge(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void updatePrintAuthority(String code, boolean status) {
    try {
      RoleAuthorization model = repo.findByCode(code);
      model.setPrintAuthority(status);
      
      session = em.unwrap(org.hibernate.Session.class);
      session.merge(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
