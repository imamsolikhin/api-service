package com.portoloop.oauth.service;

import com.portoloop.oauth.dao.RoleAuthorizationDao;
import com.portoloop.oauth.dao.RoleDao;
import com.portoloop.core.enums.CustomException;
import com.portoloop.oauth.repo.RoleRepo;
import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.model.Role;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService extends BaseQuery{

  @Autowired
  private RoleRepo repo;

  @Autowired
  private RoleDao dao;

  @Autowired
  private RoleAuthorizationDao daoAuth;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  @Transactional
  public Role save(Role model) {
    try {
      repo.save(model);
      long data = daoAuth.checkExitingByHeader(model.getCode());
      if (data < 1) {
        String sql = ""
                + " INSERT INTO scr_role_authorization (`code`, headerCode, AuthorizationCode) "
                + " SELECT CONCAT('"+model.getCode()+"_',scr_menu.`code`), '"+model.getCode()+"', scr_menu.`code` "
                + " FROM scr_menu ";
        session = em.unwrap(org.hibernate.Session.class);
        session.createNativeQuery(sql).executeUpdate();
      }
      return model;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  @Transactional
  public void delete(String code) {
    try {
      repo.deleteByCode(code);
      
      String sql = " DELETE FROM scr_role_authorization WHERE headerCode = '"+code+"' ";
      session = em.unwrap(org.hibernate.Session.class);
      session.createNativeQuery(sql).executeUpdate();
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
