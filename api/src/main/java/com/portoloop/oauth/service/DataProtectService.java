package com.portoloop.oauth.service;

import com.portoloop.oauth.dao.DataProtectDao;
import com.portoloop.core.enums.CustomException;
import com.portoloop.oauth.repo.DataProtectRepo;
import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.model.DataProtect;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DataProtectService extends BaseQuery{

  @Autowired
  private DataProtectRepo repo;

  @Autowired
  private DataProtectDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  @Transactional
  public DataProtect save(DataProtect model) {
    try {
      repo.save(model);
      return model;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  @Transactional
  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
