package com.portoloop.oauth.repo;

import com.portoloop.core.model.UserJnDivision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface UserJnDivisionRepo extends JpaRepository<UserJnDivision, String> {

  @Transactional
  void deleteByCode(String code);

  @Transactional
  void deleteByUserCode(String code);
  
}
