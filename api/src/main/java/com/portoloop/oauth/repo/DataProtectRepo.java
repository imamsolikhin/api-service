package com.portoloop.oauth.repo;

import com.portoloop.core.model.DataProtect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface DataProtectRepo extends JpaRepository<DataProtect, String> {

  @Transactional
  void deleteByCode(String code);
}
