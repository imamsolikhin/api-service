package com.portoloop.oauth.repo;

import com.portoloop.core.model.UserJnWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface UserJnWarehouseRepo extends JpaRepository<UserJnWarehouse, String> {

  @Transactional
  void deleteByCode(String code);

  @Transactional
  void deleteByUserCode(String code);
  
}
