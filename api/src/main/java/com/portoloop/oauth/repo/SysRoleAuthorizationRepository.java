package com.portoloop.oauth.repo;

import com.portoloop.core.model.RoleAuthorization;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleAuthorizationRepository extends CrudRepository<RoleAuthorization, String> {
  
    RoleAuthorization findByHeaderCodeAndAuthorizationCode(String headeCode, String authCode);

}
