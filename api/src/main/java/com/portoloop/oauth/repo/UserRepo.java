package com.portoloop.oauth.repo;

import com.portoloop.core.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface UserRepo extends JpaRepository<User, String> {
  
  User findByCode(String code);
  
  @Transactional
  void deleteByCode(String code);
  
}
