package com.portoloop.oauth.repo;

import com.portoloop.core.model.UserJnBankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface UserJnBankAccountRepo extends JpaRepository<UserJnBankAccount, String> {

  @Transactional
  void deleteByCode(String code);

  @Transactional
  void deleteByUserCode(String code);
  
}
