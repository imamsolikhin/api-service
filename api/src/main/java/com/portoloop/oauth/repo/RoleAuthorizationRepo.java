package com.portoloop.oauth.repo;

import com.portoloop.core.model.RoleAuthorization;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Imam Solikhin
 */
public interface RoleAuthorizationRepo extends JpaRepository<RoleAuthorization, String> {

  RoleAuthorization findByCode(String code);
}
