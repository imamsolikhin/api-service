package com.portoloop.oauth.repo;

import com.portoloop.core.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface RoleRepo extends JpaRepository<Role, String> {

  @Transactional
  void deleteByCode(String code);
}
