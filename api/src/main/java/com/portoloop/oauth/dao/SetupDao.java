/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.oauth.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.model.Setup;
import org.springframework.stereotype.Repository;

/**
 *
 * @author imamsolikhin
 */
@Repository
public class SetupDao extends BaseQuery {

  public Setup data(String code, String branch) {
    try {
      String qry = " "
              + " SELECT "
              + "     model.code, "
              + "     model.companyName, "
              + "     model.companyAcronym, "
              + "     model.logoPath, "
              + "     model.webTitle, "
              + "     model.currencyCode, "
              + "     mst_currency.name currencyName, "
              + "     model.vatPercent, "
              + "     model.vatDivision, "
              + "     model.defaultPriceTypeCode, "
              + "     mst_price_type.name defaultPriceTypeName, "
              + "     model.arAging1, "
              + "     model.arAging2, "
              + "     model.arAging3, "
              + "     model.arAging4, "
              + "     model.apAging1, "
              + "     model.apAging2, "
              + "     model.apAging3, "
              + "     model.apAging4, "
              + "     users.*  "
              + " FROM sys_setup model "
              + "  LEFT JOIN mst_currency ON mst_currency.code = model.currencyCode "
              + "  LEFT JOIN mst_price_type ON mst_price_type.code = model.defaultPriceTypeCode "
              + "     LEFT JOIN (SELECT "
              + "         model.roleCode, "
              + "         scr_role.name roleName, "
              + "         model.fullName, "
              + "         model.username, "
              + "         model.emailAddress, "
              + "         model.employeeCode, "
              + "         mst_employee.name employeeName, "
              + "         model.defaultBranchCode, "
              + "         mst_branch.name defaultBranchName, "
              + "         model.defaultWarehouseCode, "
              + "         mst_warehouse.name defaultWarehouseName, "
              + "         model.defaultCashAccountCode, "
              + "         mst_cash_account.name defaultCashAccountName, "
              + "         model.defaultBankAccountCode, "
              + "         mst_bank_account.name defaultBankAccountName, "
              + "         model.defaultDivisionCode, "
              + "         mst_division.name defaultDivisionName, "
              + "         model.activeStatus, "
              + "         model.SuperUserStatus superUser   "
              + "     FROM "
              + "         scr_user model    "
              + "     LEFT JOIN "
              + "         scr_role  "
              + "             ON scr_role.code = model.roleCode    "
              + "     LEFT JOIN "
              + "         mst_employee  "
              + "             ON mst_employee.code = model.employeeCode    "
              + "     LEFT JOIN "
              + "         mst_branch  "
              + "             ON mst_branch.code = model.defaultBranchCode    "
              + "     LEFT JOIN "
              + "         mst_warehouse  "
              + "             ON mst_warehouse.code = model.defaultWarehouseCode    "
              + "     LEFT JOIN "
              + "         mst_cash_account  "
              + "             ON mst_cash_account.code = model.defaultCashAccountCode    "
              + "     LEFT JOIN "
              + "         mst_division  "
              + "             ON mst_division.code = model.defaultDivisionCode    "
              + "     LEFT JOIN "
              + "         mst_bank_account  "
              + "             ON mst_bank_account.code = model.defaultBankAccountCode) users ON users.username = '" + code + "' "
              + " WHERE "
              + "  model.code = '" + branch + "' ";
      return uniqueResult(qry, Setup.class);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
