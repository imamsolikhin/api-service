/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.oauth.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.model.Menu;
import com.portoloop.core.model.MenuDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author imamsolikhin
 */
@Repository
public class MenuDao extends BaseQuery {

  public List<MenuDto> menu(String username) {
    try {
      List<MenuDto> auditMenu = new ArrayList();
      List<MenuDto> listMenu = new ArrayList();

      List<Menu> Header = listResult("SELECT scr_menu.* FROM scr_menu INNER JOIN scr_role_authorization ON scr_role_authorization.AuthorizationCode = scr_menu.`code` INNER JOIN scr_role ON scr_role.`code` = scr_role_authorization.HeaderCode INNER JOIN scr_user ON scr_user.RoleCode = scr_role.`code` AND scr_user.`username` = '"+username+"' WHERE scr_menu.ParentCode IS NULL ORDER BY scr_menu.SortNo ASC;", Menu.class);
      if (!Header.isEmpty()) {
        for (int i = 0; i < Header.size(); i++) {
          if (Header.get(i).getMenuType().equals("GROUP")) {
            MenuDto m = new MenuDto();
            m.setCode(Header.get(i).getCode());
            m.setLabel(Header.get(i).getText());
            m.setIcon(Header.get(i).getIcon());
            listMenu.add(m);
          } else if (Header.get(i).getMenuType().equals("MODULE")) {
            MenuDto m = new MenuDto();
            m.setCode(Header.get(i).getCode());
            m.setLabel(Header.get(i).getText());
            m.setIcon(Header.get(i).getIcon());
            m.setTo(Header.get(i).getClasses());
            listMenu.add(m);
          }
        }
      }

      List<Menu> menus = listResult("SELECT scr_menu.* FROM scr_menu INNER JOIN scr_role_authorization ON scr_role_authorization.AuthorizationCode = scr_menu.`code` AND scr_role_authorization.AssignAuthority = 1 INNER JOIN scr_role ON scr_role.`code` = scr_role_authorization.HeaderCode INNER JOIN scr_user ON scr_user.RoleCode = scr_role.`code` AND scr_user.`username` = '"+username+"' WHERE scr_menu.ParentCode IS NOT NULL ORDER BY scr_menu.SortNo ASC;", Menu.class);
      if (!menus.isEmpty()) {
        for (int i = 0; i < menus.size(); i++) {
          if (menus.get(i).getMenuType().equals("SUB")) {
            for (int s = 0; s < listMenu.size(); s++) {
              if (listMenu.get(s).getCode().equals(menus.get(i).getParentCode())) {
                MenuDto m = new MenuDto();
                m.setCode(menus.get(i).getCode());
                m.setLabel(menus.get(i).getText());
                m.setIcon(menus.get(i).getIcon());
                m.setLink(menus.get(i).getClasses());
                listMenu.get(s).getItems().add(m);
              }
            }
          } else if (menus.get(i).getMenuType().equals("MODULE")) {
            for (int s = 0; s < listMenu.size(); s++) {
              if (listMenu.get(s).getCode().equals(menus.get(i).getParentCode())) {
                MenuDto m = new MenuDto();
                m.setCode(menus.get(i).getCode());
                m.setLabel(menus.get(i).getText());
                m.setIcon(menus.get(i).getIcon());
                m.setTo(menus.get(i).getClasses());
                listMenu.get(s).getItems().add(m);
              }
            }
          }

          if (menus.get(i).getMenuType().equals("MODULE")) {
            for (int s = 0; s < listMenu.size(); s++) {
              if (listMenu.get(s).getItems().size() > 0) {
                for (int n = 0; n < listMenu.get(s).getItems().size(); n++) {
                  if (listMenu.get(s).getItems().get(n).getCode().equals(menus.get(i).getParentCode())) {
                    MenuDto m = new MenuDto();
                    m.setCode(menus.get(i).getCode());
                    m.setLabel(menus.get(i).getText());
                    m.setIcon(menus.get(i).getIcon());
                    m.setTo(menus.get(i).getClasses());
                    listMenu.get(s).getItems().get(n).getItems().add(m);
                  }
                }
              }
            }
          }
        }
      }

      for (MenuDto m : listMenu) {
        if (m.getItems().size() > 0) {
          auditMenu.add(m);
        }else if(m.getTo() != null){
          auditMenu.add(m);
        }
      }
      return auditMenu;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
