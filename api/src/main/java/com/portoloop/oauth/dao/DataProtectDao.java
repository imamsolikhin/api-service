/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and sys_data_protection.open the template in the editor.
 */
package com.portoloop.oauth.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class DataProtectDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM sys_data_protection WHERE sys_data_protection.code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM sys_data_protection "
              + "  WHERE sys_data_protection.code LIKE '%" + obj.get("code") + "%' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  sys_data_protection.code,"
              + "  sys_data_protection.yearPeriod, "
              + "  sys_data_protection.monthPeriod "
              + " FROM sys_data_protection "
              + " WHERE "
              + "  sys_data_protection.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  sys_data_protection.code,"
              + "  sys_data_protection.yearPeriod, "
              + "  sys_data_protection.monthPeriod "
              + " FROM sys_data_protection "
              + " WHERE "
              + "  sys_data_protection.code LIKE '%" + obj.get("code") + "%' "
              + " ORDER BY sys_data_protection." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  sys_data_protection.code,"
              + "  sys_data_protection.yearPeriod, "
              + "  sys_data_protection.monthPeriod "
              + " FROM sys_data_protection "
              + " WHERE "
              + "  sys_data_protection.code LIKE '%" + obj.get("code") + "%' "
              + " ORDER BY sys_data_protection." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
