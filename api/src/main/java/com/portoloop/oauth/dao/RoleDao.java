/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and scr_role.open the template in the editor.
 */
package com.portoloop.oauth.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class RoleDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_role WHERE scr_role.code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_role "
              + "  LEFT JOIN mst_job_position ON mst_job_position.code = scr_role.JobPositionCode "
              + "  WHERE scr_role.code LIKE '%" + obj.get("code") + "%' "
              + "  AND scr_role.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("scr_role.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role.code,"
              + "  scr_role.name,"
              + "  scr_role.remark,"
              + "  scr_role.activeStatus,"
              + "  mst_job_position.code as jobPostitionCode,"
              + "  mst_job_position.name as jobPostitionName "
              + " FROM scr_role "
              + "  LEFT JOIN mst_job_position ON mst_job_position.code = scr_role.JobPositionCode "
              + " WHERE "
              + "  scr_role.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role.code,"
              + "  scr_role.name,"
              + "  scr_role.remark,"
              + "  scr_role.activeStatus,"
              + "  mst_job_position.code as jobPostitionCode,"
              + "  mst_job_position.name as jobPostitionName "
              + " FROM scr_role "
              + "  LEFT JOIN mst_job_position ON mst_job_position.code = scr_role.JobPositionCode "
              + " WHERE "
              + "  scr_role.code LIKE '%" + obj.get("code") + "%' "
              + "  AND scr_role.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("scr_role.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY scr_role." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role.code,"
              + "  scr_role.name,"
              + "  scr_role.remark,"
              + "  scr_role.activeStatus,"
              + "  mst_job_position.code as jobPostitionCode,"
              + "  mst_job_position.name as jobPostitionName "
              + " FROM scr_role "
              + "  LEFT JOIN mst_job_position ON mst_job_position.code = scr_role.JobPositionCode "
              + " WHERE "
              + "  scr_role.code LIKE '%" + obj.get("code") + "%' "
              + "  AND scr_role.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus(getTableName(module) + ".activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY scr_role." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
