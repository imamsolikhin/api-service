/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.oauth.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class UserDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_user WHERE scr_user.code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_user "
              + "  LEFT JOIN scr_role ON scr_role.code = scr_user.RoleCode "
              + "  LEFT JOIN mst_employee ON mst_employee.code = scr_user.EmployeeCode "
              + "  WHERE scr_user.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND scr_user.fullName LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  " + getStatus("scr_user.activeStatus", AppUtils.toString(obj, "activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_user.code,"
              + "  scr_user.fullName,"
              + "  scr_user.username,"
              + "  scr_user.password,"
              + "  scr_user.superUserStatus,"
              + "  scr_user.activeStatus,"
              + "  scr_user.defaultBranchCode, "
              + "  scr_user.defaultWarehouseCode, "
              + "  scr_user.defaultCashAccountCode, "
              + "  scr_user.defaultBankAccountCode, "
              + "  scr_user.defaultDivisionCode, "
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  mst_employee.code as employeeCode,"
              + "  mst_employee.name as employeeName "
              + " FROM scr_user "
              + "  LEFT JOIN scr_role ON scr_role.code = scr_user.RoleCode "
              + "  LEFT JOIN mst_employee ON mst_employee.code = scr_user.EmployeeCode "
              + " WHERE "
              + "  scr_user.code = '" + AppUtils.toString(obj, "code") + "' ";
      Map<String, Object> data = singleResult(qry);
      data.put("password", "inkombizz");
      return data;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_user.code,"
              + "  scr_user.fullName,"
              + "  scr_user.username,"
              + "  scr_user.superUserStatus,"
              + "  scr_user.activeStatus,"
              + "  scr_user.defaultBranchCode, "
              + "  scr_user.defaultWarehouseCode, "
              + "  scr_user.defaultCashAccountCode, "
              + "  scr_user.defaultBankAccountCode, "
              + "  scr_user.defaultDivisionCode, "
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  mst_employee.code as employeeCode,"
              + "  mst_employee.name as employeeName "
              + " FROM scr_user "
              + "  LEFT JOIN scr_role ON scr_role.code = scr_user.RoleCode "
              + "  LEFT JOIN mst_employee ON mst_employee.code = scr_user.EmployeeCode "
              + " WHERE "
              + "  scr_user.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND scr_user.fullName LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  " + getStatus("scr_user.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + " ORDER BY scr_user." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_user.code,"
              + "  scr_user.fullName,"
              + "  scr_user.username,"
              + "  scr_user.superUserStatus,"
              + "  scr_user.activeStatus,"
              + "  scr_user.defaultBranchCode, "
              + "  scr_user.defaultWarehouseCode, "
              + "  scr_user.defaultCashAccountCode, "
              + "  scr_user.defaultBankAccountCode, "
              + "  scr_user.defaultDivisionCode, "
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  mst_employee.code as employeeCode,"
              + "  mst_employee.name as employeeName "
              + " FROM scr_user "
              + "  LEFT JOIN scr_role ON scr_role.code = scr_user.RoleCode "
              + "  LEFT JOIN mst_employee ON mst_employee.code = scr_user.EmployeeCode "
              + " WHERE "
              + "  scr_user.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND scr_user.fullName LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  " + getStatus(getTableName(module) + ".activeStatus", obj.get("activeStatus").toString()) + " "
//              + "  " + getStatus(getTableName(module) + ".activeStatus", AppUtils.toStatus(AppUtils.toString(obj, "activeStatus").toString())) + " "
              + " ORDER BY scr_user." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults pagingBranch(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  mst_branch.code,"
              + "  mst_branch.name"
              + " FROM scr_user_branch "
              + "  LEFT JOIN mst_branch ON mst_branch.code = scr_user_branch.BranchCode "
              + " WHERE "
              + "  scr_user_branch.UserCode = '" + AppUtils.toString(obj, "code") + "' ";
//              + " ORDER BY scr_user_branch." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults pagingWarehouse(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  mst_warehouse.code,"
              + "  mst_warehouse.name"
              + " FROM scr_user_warehouse "
              + "  LEFT JOIN mst_warehouse ON mst_warehouse.code = scr_user_warehouse.WarehouseCode "
              + " WHERE "
              + "  scr_user_warehouse.UserCode = '" + AppUtils.toString(obj, "code") + "' ";
//              + " ORDER BY scr_user_warehouse." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults pagingCashAccount(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  mst_cash_account.code,"
              + "  mst_cash_account.name"
              + " FROM scr_user_cash_account "
              + "  LEFT JOIN mst_cash_account ON mst_cash_account.code = scr_user_cash_account.CashAccountCode "
              + " WHERE "
              + "  scr_user_cash_account.UserCode = '" + AppUtils.toString(obj, "code") + "' ";
//              + " ORDER BY scr_user_cash_account." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults pagingBankAccount(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  mst_bank_account.code,"
              + "  mst_bank_account.name"
              + " FROM scr_user_bank_account "
              + "  LEFT JOIN mst_bank_account ON mst_bank_account.code = scr_user_bank_account.BankAccountCode "
              + " WHERE "
              + "  scr_user_bank_account.UserCode = '" + AppUtils.toString(obj, "code") + "' ";
//              + " ORDER BY scr_user_bank_account." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults pagingDivision(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  mst_division.code,"
              + "  mst_division.name"
              + " FROM scr_user_division "
              + "  LEFT JOIN mst_division ON mst_division.code = scr_user_division.DivisionCode "
              + " WHERE "
              + "  scr_user_division.UserCode = '" + AppUtils.toString(obj, "code") + "' ";
//              + " ORDER BY scr_user_division." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
