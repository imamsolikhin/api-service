/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.oauth.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class RoleAuthorizationDao extends BaseQuery {

  public long checkExitingByHeader(String code) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_role_authorization WHERE scr_role_authorization.headerCode = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_role_authorization WHERE scr_role_authorization.code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_role_authorization "
              + "  WHERE scr_role_authorization.HeaderCode LIKE '%" + obj.get("code") + "%' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role_authorization.code,"
              + "  IF(scr_role_authorization.assignAuthority=1,'true','false') AS assignAuthority,"
              + "  IF(scr_role_authorization.saveAuthority=1,'true','false') AS saveAuthority,"
              + "  IF(scr_role_authorization.updateAuthority=1,'true','false') AS updateAuthority,"
              + "  IF(scr_role_authorization.deleteAuthority=1,'true','false') AS deleteAuthority,"
              + "  IF(scr_role_authorization.printAuthority=1,'true','false') AS printAuthority,"
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  scr_menu.code as authorizationCode,"
              + "  scr_menu.name as authorizationName, "
              + "  scr_menu.MenuType as menuType "
              + " FROM scr_role_authorization "
              + "  LEFT JOIN scr_role ON scr_role.code = scr_role_authorization.HeaderCode "
              + "  LEFT JOIN scr_menu ON scr_menu.code = scr_role_authorization.AuthorizationCode "
              + " WHERE "
              + "  scr_role_authorization.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role_authorization.code,"
              + "  IF(scr_role_authorization.assignAuthority=1,'true','false') AS assignAuthority,"
              + "  IF(scr_role_authorization.saveAuthority=1,'true','false') AS saveAuthority,"
              + "  IF(scr_role_authorization.updateAuthority=1,'true','false') AS updateAuthority,"
              + "  IF(scr_role_authorization.deleteAuthority=1,'true','false') AS deleteAuthority,"
              + "  IF(scr_role_authorization.printAuthority=1,'true','false') AS printAuthority,"
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  scr_menu.code as authorizationCode,"
              + "  scr_menu.name as authorizationName, "
              + "  scr_menu.MenuType as menuType "
              + " FROM scr_role_authorization "
              + "  LEFT JOIN scr_role ON scr_role.code = scr_role_authorization.HeaderCode "
              + "  LEFT JOIN scr_menu ON scr_menu.code = scr_role_authorization.AuthorizationCode "
              + " WHERE "
              + "  scr_role_authorization.HeaderCode LIKE '%" + obj.get("code") + "%' "
              + " ORDER BY scr_menu.SortNo ASC";
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role_authorization.code,"
              + "  IF(scr_role_authorization.assignAuthority=1,'true','false') AS assignAuthority,"
              + "  IF(scr_role_authorization.saveAuthority=1,'true','false') AS saveAuthority,"
              + "  IF(scr_role_authorization.updateAuthority=1,'true','false') AS updateAuthority,"
              + "  IF(scr_role_authorization.deleteAuthority=1,'true','false') AS deleteAuthority,"
              + "  IF(scr_role_authorization.printAuthority=1,'true','false') AS printAuthority,"
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  scr_menu.code as authorizationCode,"
              + "  scr_menu.name as authorizationName, "
              + "  scr_menu.MenuType as menuType "
              + " FROM scr_role_authorization "
              + "  LEFT JOIN scr_role ON scr_role.code = scr_role_authorization.HeaderCode "
              + "  LEFT JOIN scr_menu ON scr_menu.code = scr_role_authorization.AuthorizationCode "
              + " WHERE "
              + "  scr_role_authorization.HeaderCode LIKE '%" + obj.get("code") + "%' "
              + " ORDER BY scr_menu.SortNo ASC";
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
