package com.portoloop.oauth.controller;

import com.portoloop.core.base.BaseProgress;
import com.portoloop.core.base.ResponsBody;
import com.portoloop.core.enums.CustomException;
import com.portoloop.core.model.User;
import com.portoloop.oauth.service.UserService;
import com.portoloop.core.utils.AppUtils;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Authorization;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@Api(tags = "User")
@RequestMapping("/${config.pathOauth}/setting/user")
public class UserController extends BaseProgress {

  @Autowired
  private UserService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"activeStatus\":\"ALL\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), User.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/branch/search")
  public HttpEntity<Object> userBranch(@RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
          required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingBranch(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), User.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/warehouse/search")
  public HttpEntity<Object> userWarehouse(@RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
          required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingWarehouse(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), User.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/bank-account/search")
  public HttpEntity<Object> userBankAccount(@RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
          required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingBankAccount(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), User.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/cash-account/search")
  public HttpEntity<Object> userCashAccount(@RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
          required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingCashAccount(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), User.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/division/search")
  public HttpEntity<Object> userDivision(@RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
          required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingDivision(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), User.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, User.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody User model) {
    try {
      long data = service.checkData(model.getCode(), User.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody User model) {
    try {
      long data = service.checkData(model.getCode(), User.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, User.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

}
