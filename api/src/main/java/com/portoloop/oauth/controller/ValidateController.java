package com.portoloop.oauth.controller;

import com.portoloop.core.base.ResponsBody;
import com.portoloop.core.enums.CustomException;
import com.portoloop.core.model.UserDetails;
import com.portoloop.core.security.JwtSessionDataPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portoloop.core.security.JwtTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/${config.pathOauth}")
@Api(tags = "Validation")
public class ValidateController {

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private JwtSessionDataPoint jwtSessionDataPoint;

  @Authorization(value = "Authorization")
  @PostMapping(value = "/v1/validate")
  public HttpEntity<Object> validateTokenV1(HttpServletRequest req) {
    try {
      String token = req.getHeader("Authorization");
      if (token != null && token.startsWith("Bearer ")) {
        String jwtToken = token.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(jwtToken);
        
      UserDetails userDetails = new UserDetails(username, "", new ArrayList<>());
      userDetails.setSetup(jwtSessionDataPoint.data(username, "AGN"));
      }
      return new HttpEntity<>(new ResponsBody(null, "Token not valid", HttpStatus.FORBIDDEN));
    } catch (Exception e) {
      e.printStackTrace();
      throw new CustomException("Service maintenance", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
