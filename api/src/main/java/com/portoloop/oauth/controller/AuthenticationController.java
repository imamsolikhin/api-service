package com.portoloop.oauth.controller;

import com.portoloop.core.base.SignIn;
import com.portoloop.core.enums.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.portoloop.core.model.UserDetails;
import com.portoloop.core.security.JwtSessionDataPoint;

import com.portoloop.core.security.JwtTokenUtil;
import io.swagger.annotations.Api;
import java.util.ArrayList;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/${config.pathOauth}/")
@Api(tags = "Authentication")
public class AuthenticationController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private JwtSessionDataPoint jwtSessionDataPoint;

  @PostMapping(value = "/v1/authenticate")
  public HttpEntity<Object> authTokenV1(@RequestBody SignIn authenticationRequest) throws Exception {
    try {
      authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());


      UserDetails userDetails = new UserDetails(authenticationRequest.getUsername(), "", new ArrayList<>());
      userDetails.setSetup(jwtSessionDataPoint.data(authenticationRequest.getUsername(), "AGN"));
      final String token = jwtTokenUtil.generateToken(userDetails);

      return new HttpEntity<>(token);
    } catch (Exception e) {
      throw new CustomException("Invalid username or password", HttpStatus.FORBIDDEN);
    }
  }

  private void authenticate(String username, String password) throws Exception {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException e) {
      throw new CustomException("Username is disable", HttpStatus.FORBIDDEN);
    } catch (BadCredentialsException e) {
      throw new CustomException("Invalid username or password", HttpStatus.FORBIDDEN);
    } catch (Exception e) {
      throw new CustomException("Service maintenance", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
