/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.oauth.controller;

import com.portoloop.core.base.ResponsBody;
import com.portoloop.oauth.service.MenuService;
import com.portoloop.core.security.JwtTokenUtil;
import io.swagger.annotations.Api;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

/**
 *
 * @author imamsolikhin
 */
@RestController
@RequestMapping("/${config.pathOauth}")
@Api(tags = "Menu Auth")
public class MenuController {

  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private MenuService menuService;

  @GetMapping("/menus")
  public HttpEntity<Object> getAllMenu(HttpServletRequest req) {
    String token = req.getHeader("Authorization");
    if (token != null && token.startsWith("Bearer ")) {
      String jwtToken = token.substring(7);
      String username = jwtTokenUtil.getUsernameFromToken(jwtToken);
      return new HttpEntity<>(new ResponsBody(menuService.loadMenu(username), "Token success generade", HttpStatus.OK));
    }
    return new HttpEntity<>(new ResponsBody(null, "Token not valid", HttpStatus.FORBIDDEN));
  }
}
