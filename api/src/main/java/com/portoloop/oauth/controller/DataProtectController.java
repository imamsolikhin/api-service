package com.portoloop.oauth.controller;

import com.portoloop.core.base.BaseProgress;
import com.portoloop.core.base.ResponsBody;
import com.portoloop.core.enums.CustomException;
import com.portoloop.core.model.DataProtect;
import com.portoloop.oauth.service.DataProtectService;
import com.portoloop.core.utils.AppUtils;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Authorization;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Imam Solikhin
 */

@RestController
@Api(tags = "DataProtect")
@RequestMapping("/${config.pathOauth}/setting/data-protect")
public class DataProtectController extends BaseProgress {

  @Autowired
  private DataProtectService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"activeStatus\":\"ALL\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), DataProtect.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, DataProtect.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody DataProtect model) {
    try {
      long data = service.checkData(model.getCode(), DataProtect.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody DataProtect model) {
    try {
      long data = service.checkData(model.getCode(), DataProtect.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, DataProtect.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

}
