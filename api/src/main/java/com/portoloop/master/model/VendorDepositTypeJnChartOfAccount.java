package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_vendor_deposit_type_jn_chart_of_account")
public class VendorDepositTypeJnChartOfAccount  implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("VendorDepositTypeCode")
  @Column(name = "VendorDepositTypeCode", nullable = false)
  private String vendorDepositTypeCode;

  @SheetColumn("BranchCode")
  @Column(name = "BranchCode", nullable = false)
  private String branchCode;

  @SheetColumn("ChartOfAccountCode")
  @Column(name = "ChartOfAccountCode", nullable = false)
  private String chartOfAccountCode;

}
