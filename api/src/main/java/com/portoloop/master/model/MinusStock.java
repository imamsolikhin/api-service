package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_minus_stock")
public class MinusStock extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "WarehouseCode")
  @SheetColumn("WarehouseCode")
  private String warehouseCode = "";

  @Column(name = "RackCode")
  @SheetColumn("RackCode")
  private String rackCode = "";

  @Column(name = "ItemCode")
  @SheetColumn("ItemCode")
  private String itemCode = "";

  @Column(name = "MinusQuantity")
  @SheetColumn("MinusQuantity")
  private BigDecimal minusQuantity = new BigDecimal(0.00);

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
