package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_sales_person_jn_distribution_channel")
public class SalesPersonJnDistributionChannel extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("SalesPersonCode")
  @Column(name = "SalesPersonCode", nullable = false)
  private String salesPersonCode;

  @Column(name = "DistributionChannelCode")
  @SheetColumn("DistributionChannelCode")
  private String distributionChannelCode = "";

}
