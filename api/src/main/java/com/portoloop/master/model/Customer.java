package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_customer")
public class Customer extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "BusinessEntityCode")
  @SheetColumn("BusinessEntityCode")
  private String businessEntityCode = "";

  @Column(name = "CustomerCategoryCode")
  @SheetColumn("CustomerCategoryCode")
  private String customerCategoryCode = "";

  @Column(name = "Address")
  @SheetColumn("Address")
  private String address = "";

  @Column(name = "CityCode")
  @SheetColumn("CityCode")
  private String cityCode = "";

  @Column(name = "ZipCode")
  @SheetColumn("ZipCode")
  private String zipCode = "";

  @Column(name = "Phone1")
  @SheetColumn("Phone1")
  private String phone1 = "";

  @Column(name = "Phone2")
  @SheetColumn("Phone2")
  private String phone2 = "";
  
  @Column(name = "EmailAddress")
  @SheetColumn("EmailAddress")
  private String emailAddress = "";

  @Column(name = "PaymentTermCode")
  @SheetColumn("PaymentTermCode")
  private String paymentTermCode = "";

  @Column(name = "DistributionChannelCode")
  @SheetColumn("DistributionChannelCode")
  private String distributionChannelCode = "";

  @Column(name = "ARAccountCode")
  @SheetColumn("ARAccountCode")
  private String arAccountCode = "";

  @Column(name = "CreditLimit")
  @SheetColumn("CreditLimit")
  private BigDecimal creditLimit = new BigDecimal(0.00);
  
  @Column(name = "WAPUStatus")
  @SheetColumn("WAPUStatus")
  private boolean wapuStatus = false;

  @Column(name = "DefaultDiscountTypeCode")
  @SheetColumn("DefaultDiscountTypeCode")
  private String defaultDiscountTypeCode = "";
 
  @Column(name = "PriceTypeCode")
  @SheetColumn("PriceTypeCode")
  private String priceTypeCode = "";

  @Column(name = "DefaultContactPersonCode")
  @SheetColumn("DefaultContactPersonCode")
  private String defaultContactPersonCode = "";

  @Column(name = "DefaultShipToCode")
  @SheetColumn("DefaultShipToCode")
  private String defaultShipToCode = "";

  @Column(name = "DefaultBillToCode")
  @SheetColumn("DefaultBillToCode")
  private String defaultBillToCode = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
