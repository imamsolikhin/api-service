package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_employee")
public class Employee extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @Column(name = "NIP")
  @SheetColumn("NIP")
  private String nip = "";

  @Column(name = "Name")
  @SheetColumn("Name")
  private String name = "";

  @Column(name = "JobPositionCode")
  @SheetColumn("JobPositionCode")
  private String jobPositionCode = "";

  @Column(name = "Address")
  @SheetColumn("Address")
  private String address = "";

  @Column(name = "ZipCode")
  @SheetColumn("ZipCode")
  private String zipCode = "";

  @Column(name = "CityCode")
  @SheetColumn("CityCode")
  private String cityCode = "";

  @Column(name = "Phone")
  @SheetColumn("Phone")
  private String phone = "";

  @Column(name = "MobileNo")
  @SheetColumn("MobileNo")
  private String mobileNo = "";

  @Column(name = "EmailAddress")
  @SheetColumn("EmailAddress")
  private String emailAddress = "";

  @Column(name = "Gender")
  @SheetColumn("Gender")
  private String gender = "";

  @Column(name = "MaritalStatus")
  @SheetColumn("MaritalStatus")
  private String maritalStatus = "";

  @Column(name = "ReligionCode")
  @SheetColumn("ReligionCode")
  private String religionCode = "";

  @Column(name = "EducationCode")
  @SheetColumn("EducationCode")
  private String educationCode = "";

  @Column(name = "KTPNo")
  @SheetColumn("KTPNo")
  private String ktpNo = "";

  @Column(name = "NPWP")
  @SheetColumn("NPWP")
  private String npwp = "";

  @Column(name = "NPWPName")
  @SheetColumn("NPWPName")
  private String npwpName = "";

  @Column(name = "NPWPAddress")
  @SheetColumn("NPWPAddress")
  private String npwpAddress = "";

  @Column(name = "NPWPCityCode")
  @SheetColumn("NPWPCityCode")
  private String npwpCityCode = "";

  @Column(name = "NPWPZipCode")
  @SheetColumn("NPWPZipCode")
  private String npwpZipCode = "";

  @Column(name = "ACNo")
  @SheetColumn("ACNo")
  private String acNo = "";

  @Column(name = "ACName")
  @SheetColumn("ACName")
  private String acName = "";

  @Column(name = "BankCode")
  @SheetColumn("BankCode")
  private String bankCode = "";

  @Column(name = "JoinDate")
  @SheetColumn("JoinDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date joinDate = new Date();

  @Column(name = "ResignDate")
  @SheetColumn("ResignDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date resignDate = new Date();

  @Column(name = "BirthDate")
  @SheetColumn("BirthDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date birthDate = new Date();

  @Column(name = "BirthPlace")
  @SheetColumn("BirthPlace")
  private String birthPlace = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
