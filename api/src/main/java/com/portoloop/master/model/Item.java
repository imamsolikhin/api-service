package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_item")
public class Item extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "ItemSubCategoryCode")
  @SheetColumn("ItemSubCategoryCode")
  private String itemSubCategoryCode = "";

  @Column(name = "UnitOfMeasureCode")
  @SheetColumn("UnitOfMeasureCode")
  private String unitOfMeasureCode = "";

  @Column(name = "ColorCode")
  @SheetColumn("ColorCode")
  private String colorCode = "";

  @Column(name = "ItemClassificationCode")
  @SheetColumn("ItemClassificationCode")
  private String itemClassificationCode = "";

  @Column(name = "ItemBrandCode")
  @SheetColumn("ItemBrandCode")
  private String itemBrandCode = "";

  @Column(name = "InventoryType")
  @SheetColumn("InventoryType")
  private String inventoryType = "INVENTORY";

  @Column(name = "DefaultItemCatalogCode")
  @SheetColumn("DefaultItemCatalogCode")
  private String defaultItemCatalogCode = "";

  @Column(name = "MinStock")
  @SheetColumn("MinStock")
  private BigDecimal minStock = new BigDecimal(0.00);

  @Column(name = "MaxStock")
  @SheetColumn("MaxStock")
  private BigDecimal maxStock = new BigDecimal(0.00);

  @Column(name = "COGSIDR")
  @SheetColumn("COGSIDR")
  private BigDecimal cogsIDR = new BigDecimal(0.00);

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("SalesOrderStatus")
  @Column(name = "SalesOrderStatus", columnDefinition = "TINYINT(1)")
  private boolean salesOrderStatus = true;
  
  @SheetColumn("AssemblyStatus")
  @Column(name = "AssemblyStatus", columnDefinition = "TINYINT(1)")
  private boolean assemblyStatus = true;
  
  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;
  
  @javax.persistence.Transient
  private List<ItemJnCatalogNumber> listItemJnCatalogNumberDetail;

}
