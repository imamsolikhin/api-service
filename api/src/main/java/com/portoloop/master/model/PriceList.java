package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;


@Sheet
@Entity
@Data
@Table(name = "mst_price_list")
public class PriceList extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @Column(name = "PriceTypeCode")
  @SheetColumn("PriceTypeCode")
  private String priceTypeCode = "";

  @Column(name = "EffectiveDate")
  @SheetColumn("EffectiveDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date effectiveDate = new Date();

  @Column(name = "FilePath1")
  @SheetColumn("FilePath1")
  private String filePath1 = "";

  @Column(name = "ApprovalReasonCode")
  @SheetColumn("ApprovalReasonCode")
  private String approvalReasonCode = "";

  @Column(name = "ApprovalRemark")
  @SheetColumn("ApprovalRemark")
  private String approvalRemark = "";

  @Column(name = "ApprovalStatus")
  @SheetColumn("ApprovalStatus")
  private String approvalStatus = "PENDING";

  @Column(name = "RefNo")
  @SheetColumn("RefNo")
  private String refNo = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;
  
  @javax.persistence.Transient
  private List<PriceListItem> listPriceListItem;

}
