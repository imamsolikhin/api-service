package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_vendor")
public class Vendor extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "BusinessEntityCode")
  @SheetColumn("BusinessEntityCode")
  private String businessEntityCode = "";

  @Column(name = "Address")
  @SheetColumn("Address")
  private String address = "";

  @Column(name = "CityCode")
  @SheetColumn("CityCode")
  private String cityCode = "";

  @Column(name = "ZipCode")
  @SheetColumn("ZipCode")
  private String zipCode = "";

  @Column(name = "Phone1")
  @SheetColumn("Phone1")
  private String phone1 = "";

  @Column(name = "Phone2")
  @SheetColumn("Phone2")
  private String phone2 = "";

  @Column(name = "Fax")
  @SheetColumn("Fax")
  private String fax = "";

  @Column(name = "EmailAddress")
  @SheetColumn("EmailAddress")
  private String emailAddress = "";

  @Column(name = "VendorCategoryCode")
  @SheetColumn("VendorCategoryCode")
  private String vendorCategoryCode = "";

  @Column(name = "APChartOfAccountCode")
  @SheetColumn("APChartOfAccountCode")
  private String apChartOfAccountCode = "";

  @Column(name = "NPWP")
  @SheetColumn("NPWP")
  private String npwp = "";

  @Column(name = "NPWPName")
  @SheetColumn("NPWPName")
  private String npwpName = "";

  @Column(name = "NPWPAddress")
  @SheetColumn("NPWPAddress")
  private String npwpAddress = "";

  @Column(name = "NPWPCityCode")
  @SheetColumn("NPWPCityCode")
  private String npwpCityCode = "";

  @Column(name = "NPWPZipCode")
  @SheetColumn("NPWPZipCode")
  private String npwpZipCode = "";

  @Column(name = "PaymentTermCode")
  @SheetColumn("PaymentTermCode")
  private String paymentTermCode = "";

  @Column(name = "DefaultContactPersonCode")
  @SheetColumn("DefaultContactPersonCode")
  private String defaultContactPersonCode = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
