package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_inventory_adj_reason_jn_branch")
public class InventoryAdjReasonJnBranch  implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("ReasonCode")
  @Column(name = "ReasonCode", nullable = false)
  private String reasonCode;

  @Column(name = "BranchCode")
  @SheetColumn("BranchCode")
  private String branchCode = "";

  @SheetColumn("ChartOfAccountCode")
  @Column(name = "ChartOfAccountCode")
  private String chartOfAccountCode = "";

}
