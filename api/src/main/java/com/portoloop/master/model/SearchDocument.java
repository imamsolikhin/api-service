package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_search_document")
public class SearchDocument extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("BBMStatus")
  @Column(name = "BBMStatus", columnDefinition = "TINYINT(1)")
  private boolean bbmStatus = false;

  @SheetColumn("BBKStatus")
  @Column(name = "BBKStatus", columnDefinition = "TINYINT(1)")
  private boolean bbkStatus = false;

  @SheetColumn("BKMStatus")
  @Column(name = "BKMStatus", columnDefinition = "TINYINT(1)")
  private boolean bkmStatus = false;

  @SheetColumn("BKKStatus")
  @Column(name = "BKKStatus", columnDefinition = "TINYINT(1)")
  private boolean bksStatus = false;

  @SheetColumn("GJMStatus")
  @Column(name = "GJMStatus", columnDefinition = "TINYINT(1)")
  private boolean gjmStatus = false;

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
