package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_bank_account")
public class BankAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code = "";

  @Column(name = "Name")
  @SheetColumn("Name")
  private String name = "";

  @Column(name = "ACNo")
  @SheetColumn("ACNo")
  private String acNo = "";

  @Column(name = "ACName")
  @SheetColumn("ACName")
  private String acName = "";

  @Column(name = "BankCode")
  @SheetColumn("BankCode")
  private String bankCode;

  @Column(name = "BankBranch")
  @SheetColumn("BankBranch")
  private String bankBranch = "";

  @Column(name = "BBMVoucherNo")
  @SheetColumn("BBMVoucherNo")
  private String bbmVoucherNo = "";

  @Column(name = "BBKVoucherNo")
  @SheetColumn("BBKVoucherNo")
  private String bbkVoucherNo = "";

  @Column(name = "ChartOfAccountCode")
  @SheetColumn("ChartOfAccountCode")
  private String chartOfAccountCode;

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
