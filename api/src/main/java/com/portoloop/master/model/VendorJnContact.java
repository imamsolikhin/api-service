package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_vendor_jn_contact")
public class VendorJnContact extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "VendorCode")
  @SheetColumn("VendorCode")
  private String vendorCode = "";

  @Column(name = "Phone")
  @SheetColumn("Phone")
  private String phone = "";

  @Column(name = "MobileNo")
  @SheetColumn("MobileNo")
  private String mobileNo = "";

  @Column(name = "BirthDate")
  @SheetColumn("BirthDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date birthDate = new Date();

  @Column(name = "JobPositionCode")
  @SheetColumn("JobPositionCode")
  private String jobPositionCode = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
