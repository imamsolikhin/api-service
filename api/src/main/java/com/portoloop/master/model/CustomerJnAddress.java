package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_customer_jn_address")
public class CustomerJnAddress extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @Column(name = "CustomerCode")
  @SheetColumn("CustomerCode")
  private String customerCode = "";

  @Column(name = "Name")
  @SheetColumn("Name")
  private String name = "";

  @Column(name = "Address")
  @SheetColumn("Address")
  private String address = "";

  @Column(name = "NIK")
  @SheetColumn("NIK")
  private String nik = "";

  @Column(name = "CityCode")
  @SheetColumn("CityCode")
  private String cityCode = "";

  @Column(name = "ZipCode")
  @SheetColumn("ZipCode")
  private String zipCode = "";

  @Column(name = "Phone1")
  @SheetColumn("Phone1")
  private String phone1 = "";

  @Column(name = "Phone2")
  @SheetColumn("Phone2")
  private String phone2 = "";

  @Column(name = "Fax")
  @SheetColumn("Fax")
  private String fax = "";

  @Column(name = "EmailAddress")
  @SheetColumn("EmailAddress")
  private String emailAddress = "";

  @Column(name = "ContactPerson")
  @SheetColumn("ContactPerson")
  private String contactPerson = "";

  @Column(name = "ShipToStatus")
  @SheetColumn("ShipToStatus")
  private boolean shipToStatus = false;

  @Column(name = "billToStatus")
  @SheetColumn("billToStatus")
  private boolean billToStatus = false;

  @Column(name = "NPWPStatus")
  @SheetColumn("NPWPStatus")
  private boolean npwpStatus = false;

  @Column(name = "NPWP")
  @SheetColumn("NPWP")
  private String npwp = "";

  @Column(name = "NPWPName")
  @SheetColumn("NPWPName")
  private String npwpName = "";

  @Column(name = "NPWPAddress")
  @SheetColumn("NPWPAddress")
  private String npwpAddress = "";

  @Column(name = "NPWPCityCode")
  @SheetColumn("NPWPCityCode")
  private String npwpCityCode = "";

  @Column(name = "NPWPZipCode")
  @SheetColumn("NPWPZipCode")
  private String npwpZipCode = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
