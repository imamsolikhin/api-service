package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_warehouse")
public class Warehouse extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("BranchCode")
  @Column(name = "BranchCode", nullable = false)
  private String branchCode;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "Address")
  @SheetColumn("Address")
  private String Address = "";

  @Column(name = "Phone1")
  @SheetColumn("Phone1")
  private String Phone1 = "";

  @Column(name = "Phone2")
  @SheetColumn("Phone2")
  private String Phone2 = "";

  @Column(name = "ContactPerson")
  @SheetColumn("ContactPerson")
  private String ContactPerson = "";

  @Column(name = "Fax")
  @SheetColumn("Fax")
  private String Fax = "";

  @Column(name = "EmailAddress")
  @SheetColumn("EmailAddress")
  private String EmailAddress = "";

  @Column(name = "CityCode")
  @SheetColumn("CityCode")
  private String CityCode = "";

  @Column(name = "DOCK_IN_Code")
  @SheetColumn("DOCK_IN_Code")
  private String DOCK_IN_Code = "";

  @Column(name = "DOCK_DLN_Code")
  @SheetColumn("DOCK_DLN_Code")
  private String DOCK_DLN_Code = "";

  @Column(name = "ZipCode")
  @SheetColumn("ZipCode")
  private String ZipCode = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
