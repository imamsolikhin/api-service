package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_bill_of_material_detail")
public class BillOfMaterialDetail extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("HeaderCode")
  @Column(name = "HeaderCode", nullable = false)
  private String headerCode;

  @SheetColumn("ItemCode")
  @Column(name = "ItemCode", nullable = false)
  private String itemCode;

  @SheetColumn("Quantity")
  @Column(name = "Quantity", nullable = false)
  private BigDecimal quantity = new BigDecimal(0.00);

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";
  
  
  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
