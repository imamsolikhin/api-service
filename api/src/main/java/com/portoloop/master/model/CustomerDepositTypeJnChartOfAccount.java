package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_customer_deposit_type_jn_chart_of_account")
public class CustomerDepositTypeJnChartOfAccount implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("CustomerDepositTypeCode")
  @Column(name = "CustomerDepositTypeCode", nullable = false)
  private String customerDepositTypeCode;

  @Column(name = "BranchCode")
  @SheetColumn("BranchCode")
  private String branchCode = "";

  @Column(name = "ChartOfAccountCode")
  @SheetColumn("ChartOfAccountCode")
  private String chartOfAccountCode = "";
}
