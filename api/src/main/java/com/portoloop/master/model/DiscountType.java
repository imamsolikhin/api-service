package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_discount_type")
public class DiscountType implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "DiscountPercent01")
  @SheetColumn("DiscountPercent01")
  private BigDecimal discountPercent01 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent02")
  @SheetColumn("DiscountPercent02")
  private BigDecimal discountPercent02 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent03")
  @SheetColumn("DiscountPercent03")
  private BigDecimal discountPercent03 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent04")
  @SheetColumn("DiscountPercent04")
  private BigDecimal discountPercent04 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent05")
  @SheetColumn("DiscountPercent05")
  private BigDecimal discountPercent05 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent06")
  @SheetColumn("DiscountPercent06")
  private BigDecimal discountPercent06 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent07")
  @SheetColumn("DiscountPercent07")
  private BigDecimal discountPercent07 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent08")
  @SheetColumn("DiscountPercent08")
  private BigDecimal discountPercent08 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent09")
  @SheetColumn("DiscountPercent09")
  private BigDecimal discountPercent09 = new BigDecimal(0.00);

  @Column(name = "DiscountPercent10")
  @SheetColumn("DiscountPercent10")
  private BigDecimal discountPercent10 = new BigDecimal(0.00);

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;
  
  @SheetColumn("Remark")
  @Column(name = "Remark")
  private String remark;

}
