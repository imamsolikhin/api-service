package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_document_type_chart_of_account")
public class DocumentTypeChartOfAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("DocumentTypeCode")
  @Column(name = "DocumentTypeCode", nullable = false)
  private String documentTypeCode;

  @SheetColumn("CurrencyCode")
  @Column(name = "CurrencyCode", nullable = false)
  private String currencyCode;

  @SheetColumn("AccountCode")
  @Column(name = "AccountCode", nullable = false)
  private String accountCode;

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";


}
