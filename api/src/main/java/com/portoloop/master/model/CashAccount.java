package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_cash_account")
public class CashAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;
  
  @Column(name = "Name")
  @SheetColumn("Name")
  private String name;
  
  @Column(name = "ChartOfAccountCode")
  @SheetColumn("ChartOfAccountCode")
  private String chartOfAccountCode;
  
  @Column(name = "BKMVoucherNo")
  @SheetColumn("BKMVoucherNo")
  private String bkmVoucherNo;
  
  @Column(name = "BKKVoucherNo")
  @SheetColumn("BKKVoucherNo")
  private String bkkVoucherNo;
  
  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark;

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
