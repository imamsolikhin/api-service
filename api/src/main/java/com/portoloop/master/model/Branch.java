package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_branch")
public class Branch extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("Name")
  @Column(name = "Name", nullable = false)
  private String name;

  @Column(name = "Address")
  @SheetColumn("Address")
  private String address = "";

  @Column(name = "CityCode")
  @SheetColumn("CityCode")
  private String cityCode = "";

  @Column(name = "Phone1")
  @SheetColumn("Phone1")
  private String phone1 = "";

  @Column(name = "Phone2")
  @SheetColumn("Phone2")
  private String phone2 = "";

  @Column(name = "ZipCode")
  @SheetColumn("ZipCode")
  private String zipCode = "";

  @Column(name = "EmailAddress")
  @SheetColumn("EmailAddress")
  private String emailAddress = "";

  @Column(name = "ContactPerson")
  @SheetColumn("ContactPerson")
  private String contactPerson = "";

  @Column(name = "JournalCode")
  @SheetColumn("JournalCode")
  private String journalCode = "";
  
  @Column(name = "DefaultPurchaseBillToCode")
  @SheetColumn("DefaultPurchaseBillToCode")
  private String defaultPurchaseBillToCode = "";
  
  @Column(name = "DefaultPurchaseShipToCode")
  @SheetColumn("DefaultPurchaseShipToCode")
  private String defaultPurchaseShipToCode = "";

  @Column(name = "InvoiceNote")
  @SheetColumn("InvoiceNote")
  private String invoiceNote = "";

  @Column(name = "FinanceForexGainLossChartOfAccountCode")
  @SheetColumn("FinanceForexGainLossChartOfAccountCode")
  private String financeForexGainLossChartOfAccountCode = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
