package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_item_jn_current_stock_daily")
public class ItemJnCurrentStockDaily extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("TransactionDate")
  @Column(name = "TransactionDate", nullable = false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transactionDate = new Date();
  
  @SheetColumn("WarehouseCode")
  @Column(name = "WarehouseCode", nullable = false)
  private String warehouseCode;

  @SheetColumn("ItemCode")
  @Column(name = "ItemCode", nullable = false)
  private String itemCode;

  @SheetColumn("RackCode")
  @Column(name = "RackCode", nullable = false)
  private String rackCode;

  @Column(name = "ActualStock")
  @SheetColumn("ActualStock")
  private BigDecimal actualStock = new BigDecimal(0.00);
  
  @Column(name = "COGSIDR")
  @SheetColumn("COGSIDR")
  private BigDecimal cogsIDR = new BigDecimal(0.00);

}
