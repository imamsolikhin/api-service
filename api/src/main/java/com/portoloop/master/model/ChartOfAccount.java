package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_chart_of_account")
public class ChartOfAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @Column(name = "Name")
  @SheetColumn("Name")
  private String name = "";

  @Column(name = "AccountType")
  @SheetColumn("AccountType")
  private String accountType = "H";


  @Column(name = "PYQStatus")
  @SheetColumn("PYQStatus")
  private boolean pyqStatus = false;

  @Column(name = "BBMStatus")
  @SheetColumn("BBMStatus")
  private boolean bbmStatus = false;

  @Column(name = "BBKStatus")
  @SheetColumn("BBKStatus")
  private boolean bkkStatus = false;

  @Column(name = "BKMStatus")
  @SheetColumn("BKMStatus")
  private boolean bkmStatus = false;

  @Column(name = "GJMStatus")
  @SheetColumn("GJMStatus")
  private boolean gjmStatus = false;

  @Column(name = "ADJStatus")
  @SheetColumn("ADJStatus")
  private boolean adjStatus = false;
  
  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;
}
