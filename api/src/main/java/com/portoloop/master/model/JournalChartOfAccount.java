package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_journal_chart_of_account")
public class JournalChartOfAccount extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("JournalCode")
  @Column(name = "JournalCode", nullable = false)
  private String journalCode;

  @SheetColumn("CurrencyCode")
  @Column(name = "CurrencyCode", nullable = false)
  private String currencyCode;

  @SheetColumn("JournalPostingSetupCode")
  @Column(name = "JournalPostingSetupCode", nullable = false)
  private String journalPostingSetupCode;

  @SheetColumn("AccountCode")
  @Column(name = "AccountCode", nullable = false)
  private String accountCode;

  @SheetColumn("JournalPostingType")
  @Column(name = "JournalPostingType")
  private String journalPostingType = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";


}
