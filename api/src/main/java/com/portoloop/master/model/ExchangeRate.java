package com.portoloop.master.model;

import com.portoloop.core.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_exchange_rate")
public class ExchangeRate extends BaseModel implements Serializable {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code;

  @SheetColumn("CurrencyCode")
  @Column(name = "CurrencyCode", nullable = false)
  private String currencyCode;

  @Column(name = "TransactionDate")
  @SheetColumn("TransactionDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transactionDate = new Date();
  
  @Column(name = "ExchangeRate")
  @SheetColumn("ExchangeRate")
  private BigDecimal exchangeRate = new BigDecimal(0.00);

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
