package com.portoloop.master.repo;

import com.portoloop.master.model.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ProvinceRepo extends JpaRepository<Province, String> {

  @Transactional
  void deleteByCode(String code);

}
