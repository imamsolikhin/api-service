package com.portoloop.master.repo;

import com.portoloop.master.model.VendorDepositTypeJnChartOfAccount;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface VendorDepositTypeJnChartOfAccountRepo extends JpaRepository<VendorDepositTypeJnChartOfAccount, String> {

    @Query(value ="SELECT cdjnca.* FROM mst_vendor_deposit_type_jn_chart_of_account cdjnca "
          + "WHERE cdjnca.code = :code ",nativeQuery = true)
  VendorDepositTypeJnChartOfAccount findByCode(@Param("code") String code);
  
  @Query("SELECT m FROM VendorDepositTypeJnChartOfAccount m WHERE m.vendorDepositTypeCode = :vendorDepositTypeCode ")
  List<VendorDepositTypeJnChartOfAccount> findDetailByCode(@Param("vendorDepositTypeCode") String code);
  
  @Transactional
  void deleteByCode(String code);

}
