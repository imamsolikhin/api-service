package com.portoloop.master.repo;

import com.portoloop.master.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface DepartmentRepo extends JpaRepository<Department, String> {

  @Transactional
  void deleteByCode(String code);

}
