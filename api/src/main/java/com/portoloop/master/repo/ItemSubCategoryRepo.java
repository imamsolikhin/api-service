package com.portoloop.master.repo;

import com.portoloop.master.model.ItemSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemSubCategoryRepo extends JpaRepository<ItemSubCategory, String> {

  @Transactional
  void deleteByCode(String code);

}
