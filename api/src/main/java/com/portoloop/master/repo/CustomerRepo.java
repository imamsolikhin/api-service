package com.portoloop.master.repo;

import com.portoloop.master.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CustomerRepo extends JpaRepository<Customer, String> {

  @Transactional
  void deleteByCode(String code);

}
