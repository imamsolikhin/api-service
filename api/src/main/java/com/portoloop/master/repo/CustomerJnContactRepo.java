package com.portoloop.master.repo;

import com.portoloop.master.model.CustomerJnContact;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CustomerJnContactRepo extends JpaRepository<CustomerJnContact, String> {
    
    @Query(value = "SELECT c.* FROM mst_customer_jn_contact c "
          + "INNER JOIN mst_job_position jp ON jp.Code = c.JobPositionCode "
            + "WHERE c.code = :code ", nativeQuery = true)
    CustomerJnContact findByCode(@Param("code") String code);
  
  @Query("SELECT max(c) FROM CustomerJnContact c WHERE c.code LIKE :prefixCode% ")
  List<CustomerJnContact> findMaxCode(@Param("prefixCode") String prefixCode);

  @Transactional
  void deleteByCode(String code);

}
