package com.portoloop.master.repo;

import com.portoloop.master.model.CustomerJnAddress;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CustomerJnAddressRepo extends JpaRepository<CustomerJnAddress, String> {

    
    @Query("SELECT max(c) FROM CustomerJnAddress c WHERE c.code LIKE :prefixCode% ")
    List<CustomerJnAddress> findMaxCode(@Param("prefixCode") String prefixCode);

    @Transactional
    void deleteByCode(String code);

}
