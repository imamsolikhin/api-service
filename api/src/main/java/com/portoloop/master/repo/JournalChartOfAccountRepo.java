package com.portoloop.master.repo;

import com.portoloop.master.model.JournalChartOfAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface JournalChartOfAccountRepo extends JpaRepository<JournalChartOfAccount, String> {

  @Transactional
  void deleteByCode(String code);

}
