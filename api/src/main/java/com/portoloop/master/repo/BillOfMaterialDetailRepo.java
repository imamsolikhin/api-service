/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.repo;

import com.portoloop.master.model.BillOfMaterialDetail;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lukassh
 */
public interface BillOfMaterialDetailRepo extends JpaRepository<BillOfMaterialDetail, String> {

    @Query(value = "SELECT bomd.* FROM mst_bill_of_material_detail bomd "
            + "WHERE bomd.code = :code ", nativeQuery = true)
    BillOfMaterialDetail findByCode(@Param("code") String code);

    @Query("SELECT bomd FROM BillOfMaterialDetail bomd WHERE bomd.itemCode = :code ")
    List<BillOfMaterialDetail> findDetailByCode(@Param("code") String code);

    @Transactional
    void deleteByHeaderCode(String code);
    
    @Transactional
    void deleteByCode(String code);
    
}
