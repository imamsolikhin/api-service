package com.portoloop.master.repo;

import com.portoloop.master.model.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface DocumentTypeRepo extends JpaRepository<DocumentType, String> {

  @Transactional
  void deleteByCode(String code);

}
