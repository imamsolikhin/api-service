package com.portoloop.master.repo;

import com.portoloop.master.model.Island;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface IslandRepo extends JpaRepository<Island, String> {

  @Transactional
  void deleteByCode(String code);

}
