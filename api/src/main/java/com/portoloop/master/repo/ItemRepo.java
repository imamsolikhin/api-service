package com.portoloop.master.repo;

import com.portoloop.master.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemRepo extends JpaRepository<Item, String> {

    @Query(value = "SELECT item.* FROM mst_item item "
            + "WHERE item.code = :code ", nativeQuery = true)
    Item findByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);

}
