package com.portoloop.master.repo;

import com.portoloop.master.model.ItemBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemBrandRepo extends JpaRepository<ItemBrand, String> {

  @Transactional
  void deleteByCode(String code);

}
