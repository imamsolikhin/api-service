package com.portoloop.master.repo;

import com.portoloop.master.model.BillOfMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BillOfMaterialRepo extends JpaRepository<BillOfMaterial, String> {

    @Query(value = "SELECT bom.* FROM mst_bill_of_material bom "
          + "INNER JOIN mst_item b ON b.Code = bom.ItemCode "
            + "WHERE bom.code = :code ", nativeQuery = true)
    BillOfMaterial findByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);

}
