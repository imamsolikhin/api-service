package com.portoloop.master.repo;

import com.portoloop.master.model.Color;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ColorRepo extends JpaRepository<Color, String> {

  @Transactional
  void deleteByCode(String code);

}
