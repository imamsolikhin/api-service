package com.portoloop.master.repo;

import com.portoloop.master.model.VendorJnContact;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface VendorJnContactRepo extends JpaRepository<VendorJnContact, String> {
    
    
  @Query("SELECT max(c) FROM VendorJnContact c WHERE c.code LIKE :prefixCode% ")
  List<VendorJnContact> findMaxCode(@Param("prefixCode") String prefixCode);

  @Transactional
  void deleteByCode(String code);

}
