package com.portoloop.master.repo;

import com.portoloop.master.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ProjectRepo extends JpaRepository<Project, String> {

  @Transactional
  void deleteByCode(String code);

}
