package com.portoloop.master.repo;

import com.portoloop.master.model.InventoryAdjReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface InventoryAdjReasonRepo extends JpaRepository<InventoryAdjReason, String> {

  @Transactional
  void deleteByCode(String code);

}
