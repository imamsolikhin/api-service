package com.portoloop.master.repo;

import com.portoloop.master.model.VendorCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface VendorCategoryRepo extends JpaRepository<VendorCategory, String> {

  @Transactional
  void deleteByCode(String code);

}
