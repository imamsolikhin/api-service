package com.portoloop.master.repo;

import com.portoloop.master.model.CustomerDepositTypeJnChartOfAccount;
import com.portoloop.master.model.ItemJnCatalogNumber;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemJnCatalogNumberRepo extends JpaRepository<ItemJnCatalogNumber, String> {

    @Query(value = "SELECT ican.* FROM mst_item_jn_catalog_number ican "
            + "WHERE ican.code = :code ", nativeQuery = true)
    ItemJnCatalogNumber findByCode(@Param("code") String code);

    @Query("SELECT m FROM ItemJnCatalogNumber m WHERE m.itemCode = :code ")
    List<ItemJnCatalogNumber> findDetailByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);

}
