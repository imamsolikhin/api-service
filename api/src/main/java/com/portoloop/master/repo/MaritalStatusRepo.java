package com.portoloop.master.repo;

import com.portoloop.master.model.MaritalStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface MaritalStatusRepo extends JpaRepository<MaritalStatus, String> {

  @Transactional
  void deleteByCode(String code);

}
