package com.portoloop.master.repo;

import com.portoloop.master.model.ItemJnCurrentStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemJnCurrentStockRepo extends JpaRepository<ItemJnCurrentStock, String> {

  @Transactional
  void deleteByCode(String code);

}
