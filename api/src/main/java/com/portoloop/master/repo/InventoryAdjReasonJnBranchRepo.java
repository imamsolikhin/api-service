package com.portoloop.master.repo;

import com.portoloop.master.model.InventoryAdjReasonJnBranch;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface InventoryAdjReasonJnBranchRepo extends JpaRepository<InventoryAdjReasonJnBranch, String> {

  @Query(value = "SELECT sode.* FROM mst_inventory_adj_reason_jn_branch sode "
            + "WHERE sode.code = :code ", nativeQuery = true)
    InventoryAdjReasonJnBranch findByCode(@Param("code") String code);
    
    @Query("SELECT m FROM InventoryAdjReasonJnBranch m WHERE m.reasonCode = :code ")
    List<InventoryAdjReasonJnBranch> findDetailByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);

}
