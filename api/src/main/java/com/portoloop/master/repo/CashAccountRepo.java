package com.portoloop.master.repo;

import com.portoloop.master.model.CashAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CashAccountRepo extends JpaRepository<CashAccount, String> {

  @Transactional
  void deleteByCode(String code);

}
