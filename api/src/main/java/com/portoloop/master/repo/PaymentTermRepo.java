package com.portoloop.master.repo;

import com.portoloop.master.model.PaymentTerm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface PaymentTermRepo extends JpaRepository<PaymentTerm, String> {

  @Transactional
  void deleteByCode(String code);

}
