package com.portoloop.master.repo;

import com.portoloop.master.model.CustomerDepositTypeJnChartOfAccount;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CustomerDepositTypeJnChartOfAccountRepo extends JpaRepository<CustomerDepositTypeJnChartOfAccount, String> {
    
    
      
  @Query(value ="SELECT cdjnca.* FROM mst_customer_deposit_type_jn_chart_of_account cdjnca "
          + "WHERE cdjnca.code = :code ",nativeQuery = true)
  CustomerDepositTypeJnChartOfAccount findByCode(@Param("code") String code);
  
  @Query("SELECT m FROM CustomerDepositTypeJnChartOfAccount m WHERE m.customerDepositTypeCode = :customerDepositTypeCode ")
  List<CustomerDepositTypeJnChartOfAccount> findDetailByCode(@Param("customerDepositTypeCode") String code);
  
  @Transactional
  void deleteByCode(String code);

}
