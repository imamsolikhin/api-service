package com.portoloop.master.repo;

import com.portoloop.master.model.ItemDivision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemDivisionRepo extends JpaRepository<ItemDivision, String> {

  @Transactional
  void deleteByCode(String code);

}
