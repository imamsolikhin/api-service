package com.portoloop.master.repo;

import com.portoloop.master.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BranchRepo extends JpaRepository<Branch, String> {

  @Transactional
  void deleteByCode(String code);

}
