package com.portoloop.master.repo;

import com.portoloop.master.model.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ExchangeRateRepo extends JpaRepository<ExchangeRate, String> {

  @Transactional
  void deleteByCode(String code);

}
