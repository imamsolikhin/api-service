package com.portoloop.master.repo;

import com.portoloop.master.model.BillOfMaterialDetail;
import com.portoloop.master.model.PriceListItem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface PriceListItemRepo extends JpaRepository<PriceListItem, String> {

    @Query(value = "SELECT pls.* FROM mst_price_list_item pls "
            + "WHERE pls.code = :code ", nativeQuery = true)
    BillOfMaterialDetail findByCode(@Param("code") String code);

    @Query("SELECT pls FROM PriceListItem pls WHERE pls.itemCode = :code ")
    List<PriceListItem> findDetailByCode(@Param("code") String code);

    @Transactional
    List<PriceListItem> findByHeaderCode(String code);
    
    @Transactional
    void deleteByHeaderCode(String code);

    @Transactional
    void deleteByCode(String code);

}
