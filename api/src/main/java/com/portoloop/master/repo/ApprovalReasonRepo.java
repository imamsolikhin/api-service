package com.portoloop.master.repo;

import com.portoloop.master.model.ApprovalReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ApprovalReasonRepo extends JpaRepository<ApprovalReason, String> {

  @Transactional
  void deleteByCode(String code);

}
