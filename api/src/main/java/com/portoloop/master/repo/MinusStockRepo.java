package com.portoloop.master.repo;

import com.portoloop.master.model.MinusStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface MinusStockRepo extends JpaRepository<MinusStock, String> {

  @Transactional
  void deleteByCode(String code);

}
