package com.portoloop.master.repo;

import com.portoloop.master.model.CustomerCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CustomerCategoryRepo extends JpaRepository<CustomerCategory, String> {

  @Transactional
  void deleteByCode(String code);

}
