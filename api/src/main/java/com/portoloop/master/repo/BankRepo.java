package com.portoloop.master.repo;

import com.portoloop.master.model.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BankRepo extends JpaRepository<Bank, String> {

  @Transactional
  void deleteByCode(String code);

}
