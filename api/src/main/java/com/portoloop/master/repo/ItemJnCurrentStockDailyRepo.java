package com.portoloop.master.repo;

import com.portoloop.master.model.ItemJnCurrentStockDaily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemJnCurrentStockDailyRepo extends JpaRepository<ItemJnCurrentStockDaily, String> {

  @Transactional
  void deleteByCode(String code);

}
