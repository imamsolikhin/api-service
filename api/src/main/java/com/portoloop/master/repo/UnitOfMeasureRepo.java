package com.portoloop.master.repo;

import com.portoloop.master.model.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface UnitOfMeasureRepo extends JpaRepository<UnitOfMeasure, String> {

  @Transactional
  void deleteByCode(String code);

}
