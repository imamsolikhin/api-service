package com.portoloop.master.repo;

import com.portoloop.master.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BankAccountRepo extends JpaRepository<BankAccount, String> {

  @Transactional
  void deleteByCode(String code);

}
