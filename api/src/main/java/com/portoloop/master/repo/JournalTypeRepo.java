package com.portoloop.master.repo;

import com.portoloop.master.model.JournalType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface JournalTypeRepo extends JpaRepository<JournalType, String> {

  @Transactional
  void deleteByCode(String code);

}
