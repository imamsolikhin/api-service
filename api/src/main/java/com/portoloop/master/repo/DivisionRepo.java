package com.portoloop.master.repo;

import com.portoloop.master.model.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface DivisionRepo extends JpaRepository<Division, String> {

  @Transactional
  void deleteByCode(String code);

}
