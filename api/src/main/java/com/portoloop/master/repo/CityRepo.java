package com.portoloop.master.repo;

import com.portoloop.master.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CityRepo extends JpaRepository<City, String> {

  @Transactional
  void deleteByCode(String code);

}
