package com.portoloop.master.repo;

import com.portoloop.master.model.SalesPersonJnDistributionChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface SalesPersonJnDistributionChannelRepo extends JpaRepository<SalesPersonJnDistributionChannel, String> {

  @Transactional
  void deleteByCode(String code);

}
