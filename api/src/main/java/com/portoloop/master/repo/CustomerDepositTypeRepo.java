package com.portoloop.master.repo;

import com.portoloop.master.model.CustomerDepositType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CustomerDepositTypeRepo extends JpaRepository<CustomerDepositType, String> {

    
  @Query(value ="SELECT spl.* FROM mst_customer_deposit_type spl "
          + "WHERE spl.code = :code ",nativeQuery = true)
  CustomerDepositType findByCode(@Param("code") String code);
  
  @Transactional
  void deleteByCode(String code);

}
