package com.portoloop.master.repo;

import com.portoloop.master.model.VendorDepositType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface VendorDepositTypeRepo extends JpaRepository<VendorDepositType, String> {

    @Query(value = "SELECT spl.* FROM mst_vendor_deposit_type spl "
            + "WHERE spl.code = :code ", nativeQuery = true)
    VendorDepositType findByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);

}
