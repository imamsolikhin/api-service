package com.portoloop.master.repo;

import com.portoloop.master.model.Religion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ReligionRepo extends JpaRepository<Religion, String> {

  @Transactional
  void deleteByCode(String code);

}
