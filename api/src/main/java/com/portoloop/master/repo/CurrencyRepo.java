package com.portoloop.master.repo;

import com.portoloop.master.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CurrencyRepo extends JpaRepository<Currency, String> {

  
  @Query(value ="SELECT cry.* FROM mst_currency cry "
          + "WHERE cry.code = :code ",nativeQuery = true)
  Currency findByCode(@Param("code") String code);  
    
  @Transactional
  void deleteByCode(String code);

}
