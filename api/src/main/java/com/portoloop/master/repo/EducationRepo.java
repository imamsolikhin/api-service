package com.portoloop.master.repo;

import com.portoloop.master.model.Education;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface EducationRepo extends JpaRepository<Education, String> {

  @Transactional
  void deleteByCode(String code);

}
