package com.portoloop.master.repo;

import com.portoloop.master.model.SalesPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface SalesPersonRepo extends JpaRepository<SalesPerson, String> {

  @Transactional
  void deleteByCode(String code);

}
