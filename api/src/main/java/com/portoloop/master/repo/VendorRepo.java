package com.portoloop.master.repo;

import com.portoloop.master.model.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface VendorRepo extends JpaRepository<Vendor, String> {

  @Transactional
  void deleteByCode(String code);

}
