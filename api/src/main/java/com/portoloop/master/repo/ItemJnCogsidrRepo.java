package com.portoloop.master.repo;

import com.portoloop.master.model.ItemJnCogsidr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemJnCogsidrRepo extends JpaRepository<ItemJnCogsidr, String> {

  @Transactional
  void deleteByCode(String code);

}
