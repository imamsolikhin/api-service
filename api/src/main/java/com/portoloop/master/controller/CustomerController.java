package com.portoloop.master.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portoloop.core.base.BaseProgress;
import com.portoloop.core.enums.EnumDataType;
import com.portoloop.core.base.ResponsBody;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.model.Customer;
import com.poiji.exception.PoijiExcelType;
import com.portoloop.master.service.CustomerService;
import com.portoloop.core.utils.IOExcel;
import com.poiji.bind.Poiji;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Authorization;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@Api(tags = "Customer")
@RequestMapping("/${config.pathMaster}/customer")
public class CustomerController extends BaseProgress {

  @Autowired
  private CustomerService service;
  private ModelMapper modelMapper;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "','name':'" + name + "','activeStatus':'" + activeStatus + "'}");
      return new HttpEntity<>(service.paging(obj, page, size, sort, Customer.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, Customer.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
  
  @Authorization(value = "Authorization")
  @GetMapping(value = "/look-up/search")
  public HttpEntity<Object> listLookUp(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "','name':'" + name + "','activeStatus':'" + activeStatus + "'}");
      return new HttpEntity<>(service.lookUp(obj, page, size, sort, Customer.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
  
  // api for sales order
  @Authorization(value = "Authorization")
  @GetMapping(value = "/so-look-up/search")
  public HttpEntity<Object> listLookUpForSo(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "','name':'" + name + "','activeStatus':'" + activeStatus + "'}");
      return new HttpEntity<>(service.lookUpForSo(obj, page, size, sort, Customer.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
  
  @Authorization(value = "Authorization")
  @GetMapping(value = "so-look-up/data")
  public HttpEntity<Object> dataForSoLookUp(@RequestParam String code) {
    try {
      Object model = service.dataForSoLookUp(code, Customer.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
  // api for sales order
  
  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody Customer model) {
    try {
      long data = service.checkData(model.getCode(), Customer.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
  
  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody Customer model) {
    try {
      long data = service.checkData(model.getCode(), Customer.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, Customer.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/imports")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<Customer> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, Customer.class);
      for (Customer m : model) {
        service.save(m);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/exports")
  public void exports(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "", required = false) String title,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort,
          HttpServletResponse response) throws ParseException {

    JSONObject obj = new JSONObject("{'code':'" + code + "','name':'" + name + "','activeStatus':'" + activeStatus + "'}");
    List<Map<String, Object>> list = service.list(obj, page, size, sort, Customer.class);

    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, title);
    IOExcel.expCellValues("Master Customer", EnumDataType.ENUM_DataType.STRING, 0, IOExcel.cols + 1, true);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Name", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Remark", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (int i = 0; i < list.size(); i++) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("name").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("remark").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("activeStatus").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      row++;
    }
    try {
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition", "attachment;filename=master-" + title + ".xlsx");
      response.setStatus(HttpServletResponse.SC_OK);
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }
}
