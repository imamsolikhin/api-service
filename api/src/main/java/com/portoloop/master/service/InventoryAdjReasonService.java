package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.InventoryAdjReasonDao;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.model.InventoryAdjReason;
import com.portoloop.master.model.InventoryAdjReasonJnBranch;
import com.portoloop.master.repo.InventoryAdjReasonJnBranchRepo;
import com.portoloop.master.repo.InventoryAdjReasonRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class InventoryAdjReasonService {

    @Autowired
    private InventoryAdjReasonRepo repo;

    @Autowired
    private InventoryAdjReasonJnBranchRepo dRepo;

    @Autowired
    private MasterDefaultDao dao;

    @Autowired
    private InventoryAdjReasonDao inventoryAdjReasonDao;
    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return dao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults detail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return inventoryAdjReasonDao.pagingDetail(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public InventoryAdjReason save(InventoryAdjReason model) {
        try {
            int i = 1;
            for (InventoryAdjReasonJnBranch inventoryAdjReasonJnBranch : model.getListInventoryAdjReasonJnBranch()) {
                inventoryAdjReasonJnBranch.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                inventoryAdjReasonJnBranch.setReasonCode(model.getCode());
                dRepo.save(inventoryAdjReasonJnBranch);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
    public InventoryAdjReason update(InventoryAdjReason model) {
        try {

            dRepo.deleteByCode(model.getCode());

            //Insert New Detail
            
            int i = 1;
            for (InventoryAdjReasonJnBranch inventoryAdjReasonJnBranch : model.getListInventoryAdjReasonJnBranch()) {
                inventoryAdjReasonJnBranch.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                inventoryAdjReasonJnBranch.setReasonCode(model.getCode());
                dRepo.save(inventoryAdjReasonJnBranch);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public void delete(String code) {
        try {
            repo.deleteByCode(code);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
