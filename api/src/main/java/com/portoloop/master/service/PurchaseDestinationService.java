package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.PurchaseDestinationDao;
import com.portoloop.master.model.PurchaseDestination;
import com.portoloop.master.repo.PurchaseDestinationRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class PurchaseDestinationService {

  @Autowired
  private PurchaseDestinationRepo repo;

  @Autowired
  private PurchaseDestinationDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public Object dataBillTo(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.dataBillTo(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public Object dataShipTo(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.dataShipTo(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PurchaseDestination save(PurchaseDestination model) {
    try {
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
