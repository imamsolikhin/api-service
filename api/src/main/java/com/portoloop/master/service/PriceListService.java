package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.dao.PriceListDao;
import com.portoloop.master.model.PriceList;
import com.portoloop.master.model.PriceListItem;
import com.portoloop.master.repo.PriceListItemRepo;
import com.portoloop.master.repo.PriceListRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class PriceListService {

    @Autowired
    private PriceListRepo repo;

    @Autowired
    private PriceListItemRepo priceListItemRepo;

    @Autowired
    private MasterDefaultDao dao;

    @Autowired
    private PriceListDao priceListDao;

    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PriceList data(String code) {
        try {
            return repo.findByCode(code);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return priceListDao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return priceListDao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return priceListDao.pagingDetail(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PriceList save(PriceList model) {
        try {
            int i = 1;
            for (PriceListItem priceListItem : model.getListPriceListItem()) {
                priceListItem.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                priceListItem.setHeaderCode(model.getCode());
                priceListItemRepo.save(priceListItem);
                i++;
            }
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PriceList update(PriceList model) {
        try {

            priceListItemRepo.deleteByCode(model.getCode());

            //Insert New Detail
            int i = 1;
            for (PriceListItem priceListItem : model.getListPriceListItem()) {
                priceListItem.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                priceListItem.setHeaderCode(model.getCode());
                priceListItemRepo.save(priceListItem);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PriceList updateAppovalStatus(String code, String approvalStatus) throws Exception {
        try {

            PriceList priceList = repo.findByCode(code);
            
            if (priceList.equals(null)) {
                throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
            }
            
            String approval = priceList.getApprovalStatus();

            if (approval.equals("PENDING")) {
                priceListDao.updateAppovalStatus(code, approvalStatus);
            } else if (approval.equals("APPROVED")) {
                throw new CustomException("This Transaction has been Approved!", HttpStatus.EXPECTATION_FAILED);
            } else if (approval.equals("REJECTED")) {
                throw new CustomException("This Transaction has been Rejected!", HttpStatus.EXPECTATION_FAILED);
            }


            return priceList;
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public void delete(String code) {
        try {
            priceListItemRepo.deleteByHeaderCode(code);
            repo.deleteByCode(code);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
