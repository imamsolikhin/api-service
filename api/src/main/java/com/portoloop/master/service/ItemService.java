package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.ItemDao;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.model.Item;
import com.portoloop.master.model.ItemJnCatalogNumber;
import com.portoloop.master.repo.ItemJnCatalogNumberRepo;
import com.portoloop.master.repo.ItemRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class ItemService {

    @Autowired
    private ItemRepo repo;

    @Autowired
    private ItemJnCatalogNumberRepo itemCatalogNumberRepo;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private MasterDefaultDao dao;

    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return dao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
    public Object getDataById(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return itemDao.getDataById(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults dataCatalogNumberDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return itemDao.dataCatalogNumberDetail(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return itemDao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
    public PaginatedResults lookUpSO(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return itemDao.lookUpSO(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Item save(Item model) {
        try {
            
            int i = 1;
            for (ItemJnCatalogNumber itemJnCatalogNumber : model.getListItemJnCatalogNumberDetail()) {
                itemJnCatalogNumber.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                itemJnCatalogNumber.setItemCode(model.getCode());
                itemCatalogNumberRepo.save(itemJnCatalogNumber);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
    public Item update(Item model) {
        try {
            
            Item data = repo.findByCode(model.getCode());
            if (data == null) {
                throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
            }
            
            //Delete Old Detail
            List<ItemJnCatalogNumber> listCustomerDepositTypeJnChartOfAccountDetail = itemCatalogNumberRepo.findDetailByCode(model.getCode());
            if (listCustomerDepositTypeJnChartOfAccountDetail == null) {
                throw new CustomException("Sales Price List Detail data doesn't exist", HttpStatus.NOT_FOUND);
            }

            for (ItemJnCatalogNumber itemJnCatalogNumber : listCustomerDepositTypeJnChartOfAccountDetail) {

                if (itemCatalogNumberRepo.findByCode(itemJnCatalogNumber.getCode()) == null) {
                    throw new CustomException("Data " + itemJnCatalogNumber.getCode() + "  Not Found!", HttpStatus.CONFLICT);
                }
                itemCatalogNumberRepo.deleteByCode(itemJnCatalogNumber.getCode());
            }

            //Insert New Detail
            int i = 1;
            for (ItemJnCatalogNumber itemJnCatalogNumber : model.getListItemJnCatalogNumberDetail()) {
                itemJnCatalogNumber.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                itemJnCatalogNumber.setItemCode(model.getCode());
                itemCatalogNumberRepo.save(itemJnCatalogNumber);
                i++;
            }
            
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public void delete(String code) {
        try {
            repo.deleteByCode(code);

            List<ItemJnCatalogNumber> listItemJnCatalogNumberDetail = itemCatalogNumberRepo.findDetailByCode(code);
            if (listItemJnCatalogNumberDetail == null) {
                throw new CustomException("Sales Price List Detail data doesn't exist", HttpStatus.NOT_FOUND);
            }

            for (ItemJnCatalogNumber itemJnCatalogNumber : listItemJnCatalogNumberDetail) {

                if (itemCatalogNumberRepo.findByCode(itemJnCatalogNumber.getCode()) == null) {
                    throw new CustomException("Data " + itemJnCatalogNumber.getCode() + "  Not Found!", HttpStatus.CONFLICT);
                }
                itemCatalogNumberRepo.deleteByCode(itemJnCatalogNumber.getCode());
            }

        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
