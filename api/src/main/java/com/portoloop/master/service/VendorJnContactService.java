package com.portoloop.master.service;

import static com.portoloop.core.base.Session.username;
import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.VendorJnContactDao;
import com.portoloop.master.model.VendorJnContact;
import com.portoloop.master.repo.VendorJnContactRepo;
import com.portoloop.core.utils.AutoNumber;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class VendorJnContactService {

    @Autowired
    private VendorJnContactRepo repo;

    @Autowired
    private VendorJnContactDao dao;

    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return dao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private String createCode(VendorJnContact model) {
        try {

            String prefix = model.getVendorCode() + "-";
            List<VendorJnContact> list = repo.findMaxCode(prefix);

            String oldID = "";
            if (list != null) {
                if (list.size() > 0) {
                    if (list.get(0) != null) {
                        oldID = list.get(0).getCode();
                    }
                }
            }
            return AutoNumber.generate(prefix, oldID, AutoNumber.TRANSACTION_LENGTH_3);
        } catch (HibernateException e) {
            throw e;
        }
    }

    public VendorJnContact save(VendorJnContact model) {
        try {
            model.setCode(createCode(model));
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public void delete(String code) {
        try {
            repo.deleteByCode(code);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
