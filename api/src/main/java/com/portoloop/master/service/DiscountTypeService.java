package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.DiscountTypeDao;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.model.DiscountType;
import com.portoloop.master.repo.DiscountTypeRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class DiscountTypeService {

  @Autowired
  private DiscountTypeRepo repo;

  @Autowired
  private MasterDefaultDao dao;
  
  @Autowired
  private DiscountTypeDao discountTypeDao;
  
  @Autowired
  private DiscountTypeDao dtDao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dtDao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public Object calcData(String code, String prmAmount, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "','prmAmount':'" + prmAmount + "'}");
      return dtDao.calcData(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return discountTypeDao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dtDao.lookUp(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public DiscountType save(DiscountType model) {
    try {
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
