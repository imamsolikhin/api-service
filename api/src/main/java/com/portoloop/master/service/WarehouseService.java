package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.dao.WarehouseDao;
import com.portoloop.master.model.Warehouse;
import com.portoloop.master.repo.RackRepo;
import com.portoloop.master.repo.WarehouseRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class WarehouseService {

    @Autowired
    private WarehouseRepo repo;

    @Autowired
    private RackRepo rackRepo;

    @Autowired
    private MasterDefaultDao dao;

    @Autowired
    private WarehouseDao wDao;

    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return wDao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    //FOR DLN
    public Object dataForDLN(JSONObject obj, Class<?> module) {
        try {
            return wDao.dataForDLN(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults pagingForDLN(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return wDao.pagingForDLN(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    //FOR DLN

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return wDao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults lookup(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return wDao.lookup(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Warehouse save(Warehouse model) {
        try {
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public void delete(String code) {
        try {
            repo.deleteByCode(code);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
