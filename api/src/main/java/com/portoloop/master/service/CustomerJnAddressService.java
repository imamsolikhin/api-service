package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.EnumTransactionType;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.CustomerAddressDao;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.model.CustomerJnAddress;
import com.portoloop.master.repo.CustomerJnAddressRepo;
import com.portoloop.core.utils.AutoNumber;
import com.portoloop.core.utils.DateUtils;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class CustomerJnAddressService {

    @Autowired
    private CustomerJnAddressRepo repo;

    @Autowired
    private MasterDefaultDao dao;

    @Autowired
    private CustomerAddressDao myDao;

    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return myDao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return myDao.lookUp(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object dataBillTo(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return myDao.dataBillTo(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object dataShipTo(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return myDao.dataShipTo(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return myDao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults pagingByCustomerCode(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return myDao.pagingByCustomerCode(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private String createCode(CustomerJnAddress model) {
        try {
            String prefix = model.getCustomerCode() +"-";
            List<CustomerJnAddress> list = repo.findMaxCode(prefix);

            String oldID = "";
            if (list != null) {
                if (list.size() > 0) {
                    if (list.get(0) != null) {
                        oldID = list.get(0).getCode();
                    }
                }
            }
            return AutoNumber.generate(prefix, oldID, AutoNumber.TRANSACTION_LENGTH_3);
        } catch (HibernateException e) {
            throw e;
        }
    }

    public CustomerJnAddress save(CustomerJnAddress model) {
        try {
            model.setCode(createCode(model));
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public void delete(String code) {
        try {
            repo.deleteByCode(code);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
