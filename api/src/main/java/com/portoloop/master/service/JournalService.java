package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.JournalDao;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.model.Journal;
import com.portoloop.master.model.JournalChartOfAccount;
import com.portoloop.master.repo.JournalChartOfAccountRepo;
import com.portoloop.master.repo.JournalRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class JournalService {

  @Autowired
  private JournalRepo repo;
  
  @Autowired
  private JournalChartOfAccountRepo journalChartOfAccountRepo;

  @Autowired
  private MasterDefaultDao dao;
  
  @Autowired
  private JournalDao journalDao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return journalDao.pagingDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public Journal save(Journal model) {
        try {

            //Insert New Detail
            int i = 1;
            for (JournalChartOfAccount  journalChartOfAccount : model.getListJournalChartOfAccount()) {
                journalChartOfAccount.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                journalChartOfAccount.setJournalCode(model.getCode());
                journalChartOfAccountRepo.save(journalChartOfAccount);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
  
  public Journal update(Journal model) {
        try {

            journalChartOfAccountRepo.deleteByCode(model.getCode());

            //Insert New Detail
            int i = 1;
            for (JournalChartOfAccount  journalChartOfAccount : model.getListJournalChartOfAccount()) {
                journalChartOfAccount.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                journalChartOfAccount.setJournalCode(model.getCode());
                journalChartOfAccountRepo.save(journalChartOfAccount);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
