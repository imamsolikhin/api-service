package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.dao.VendorDepositTypeDao;
import com.portoloop.master.model.VendorDepositType;
import com.portoloop.master.model.VendorDepositTypeJnChartOfAccount;
import com.portoloop.master.repo.VendorDepositTypeJnChartOfAccountRepo;
import com.portoloop.master.repo.VendorDepositTypeRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class VendorDepositTypeService {

  @Autowired
  private VendorDepositTypeRepo repo;
  
  @Autowired
    private VendorDepositTypeJnChartOfAccountRepo vendorDepositTypeJnChartOfAccountRepo;

  @Autowired
  private MasterDefaultDao dao;
  
  @Autowired
  private VendorDepositTypeDao vendorDepositTypeDao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object dataPerBranch(String code, String branchCode, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "','branchCode':'" + branchCode + "'}");
      return vendorDepositTypeDao.dataPerBranch(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return vendorDepositTypeDao.lookUp(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return vendorDepositTypeDao.pagingDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public VendorDepositType save(VendorDepositType model) {
        try {

            int i = 1;
            for (VendorDepositTypeJnChartOfAccount vendorDepositTypeJnChartOfAccount : model.getListVendorDepositTypeJnChartOfAccountDetail()) {
                vendorDepositTypeJnChartOfAccount.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                vendorDepositTypeJnChartOfAccount.setVendorDepositTypeCode(model.getCode());
                vendorDepositTypeJnChartOfAccountRepo.save(vendorDepositTypeJnChartOfAccount);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public VendorDepositType update(VendorDepositType model) {
        try {
            
            VendorDepositType data = repo.findByCode(model.getCode());
            if (data == null) {
                throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
            }
            
            //Delete Old Detail
            List<VendorDepositTypeJnChartOfAccount> listVendorDepositTypeJnChartOfAccountDetail = vendorDepositTypeJnChartOfAccountRepo.findDetailByCode(model.getCode());
            if (listVendorDepositTypeJnChartOfAccountDetail == null) {
                throw new CustomException("Sales Price List Detail data doesn't exist", HttpStatus.NOT_FOUND);
            }

            for (VendorDepositTypeJnChartOfAccount vendorDepositTypeJnChartOfAccount : listVendorDepositTypeJnChartOfAccountDetail) {

                if (vendorDepositTypeJnChartOfAccountRepo.findByCode(vendorDepositTypeJnChartOfAccount.getCode()) == null) {
                    throw new CustomException("Data " + vendorDepositTypeJnChartOfAccount.getCode() + "  Not Found!", HttpStatus.CONFLICT);
                }
                vendorDepositTypeJnChartOfAccountRepo.deleteByCode(vendorDepositTypeJnChartOfAccount.getCode());
            }

            //Insert New Detail
            int i = 1;
            for (VendorDepositTypeJnChartOfAccount vendorDepositTypeJnChartOfAccount : model.getListVendorDepositTypeJnChartOfAccountDetail()) {
                vendorDepositTypeJnChartOfAccount.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                vendorDepositTypeJnChartOfAccount.setVendorDepositTypeCode(model.getCode());
                vendorDepositTypeJnChartOfAccountRepo.save(vendorDepositTypeJnChartOfAccount);
                i++;
            }
            
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
