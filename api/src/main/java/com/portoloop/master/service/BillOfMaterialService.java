package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.BillOfMaterialDao;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.model.BillOfMaterial;
import com.portoloop.master.model.BillOfMaterialDetail;
import com.portoloop.master.repo.BillOfMaterialDetailRepo;
import com.portoloop.master.repo.BillOfMaterialRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class BillOfMaterialService {

    @Autowired
    private BillOfMaterialRepo repo;

    @Autowired
    private BillOfMaterialDao dao;

    @Autowired
    private MasterDefaultDao masterDefaultDao;

    @Autowired
    private BillOfMaterialDetailRepo billOfMaterialDetailRepo;

    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return dao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return masterDefaultDao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return masterDefaultDao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults dataBOMDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.pagingDetail(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public BillOfMaterial save(BillOfMaterial model) {
        try {
            int i = 1;
            for (BillOfMaterialDetail billOfMaterialDetail : model.getListBillOfMaterialDetail()) {
                billOfMaterialDetail.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                billOfMaterialDetail.setHeaderCode(model.getCode());
                billOfMaterialDetailRepo.save(billOfMaterialDetail);
                i++;
            }
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public BillOfMaterial update(BillOfMaterial model) {
        try {

            billOfMaterialDetailRepo.deleteByCode(model.getCode());

            //Insert New Detail
            int i = 1;
            for (BillOfMaterialDetail billOfMaterialDetail : model.getListBillOfMaterialDetail()) {
                billOfMaterialDetail.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                billOfMaterialDetail.setHeaderCode(model.getCode());
                billOfMaterialDetailRepo.save(billOfMaterialDetail);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public void delete(String code) {
        try {

            billOfMaterialDetailRepo.deleteByHeaderCode(code);
            repo.deleteByCode(code);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    // FOR ASSEMBLY
    public PaginatedResults pagingAssembly(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.pagingAssembly(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
    public PaginatedResults pagingBomDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.pagingBomDetail(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    // FOR ASSEMBLY

}
