package com.portoloop.master.service;

import com.portoloop.core.base.PaginatedResults;
import com.portoloop.core.enums.CustomException;
import com.portoloop.master.dao.CustomerDepositTypeDao;
import com.portoloop.master.dao.MasterDefaultDao;
import com.portoloop.master.model.CustomerDepositType;
import com.portoloop.master.model.CustomerDepositTypeJnChartOfAccount;
import com.portoloop.master.repo.CustomerDepositTypeJnChartOfAccountRepo;
import com.portoloop.master.repo.CustomerDepositTypeRepo;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class CustomerDepositTypeService {

    @Autowired
    private CustomerDepositTypeRepo repo;

    @Autowired
    private CustomerDepositTypeJnChartOfAccountRepo cusDeJncusDeJncoaRepo;

    @Autowired
    private MasterDefaultDao dao;

    @Autowired
    private CustomerDepositTypeDao customerDepositDao;

    public long checkData(String code, Class<?> module) {
        try {
            return dao.checkExiting(code, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public Object data(String code, Class<?> module) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code + "'}");
            return dao.data(obj, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.list(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return dao.paging(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
      try {
        return customerDepositDao.lookUp(obj, page, size, sort, module);
      } catch (Exception e) {
        throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
      }
    }

    public Object dataPerBranch(String code, String branchCode, Class<?> module) {
      try {
        JSONObject obj = new JSONObject("{'code':'" + code + "','branchCode':'" + branchCode + "'}");
        return customerDepositDao.dataPerBranch(obj, module);
      } catch (Exception e) {
        throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
      }
    }

    public PaginatedResults coa(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            return customerDepositDao.coa(obj, page, size, sort, module);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public CustomerDepositType save(CustomerDepositType model) {
        try {

            int i = 1;
            for (CustomerDepositTypeJnChartOfAccount customerDepositTypeJnChartOfAccount : model.getListCustomerDepositTypeJnChartOfAccountDetail()) {
                customerDepositTypeJnChartOfAccount.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                customerDepositTypeJnChartOfAccount.setCustomerDepositTypeCode(model.getCode());
                cusDeJncusDeJncoaRepo.save(customerDepositTypeJnChartOfAccount);
                i++;
            }

            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public CustomerDepositType update(CustomerDepositType model) {
        try {
            
            CustomerDepositType data = repo.findByCode(model.getCode());
            if (data == null) {
                throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
            }
            
            //Delete Old Detail
            List<CustomerDepositTypeJnChartOfAccount> listCustomerDepositTypeJnChartOfAccountDetail = cusDeJncusDeJncoaRepo.findDetailByCode(model.getCode());
            if (listCustomerDepositTypeJnChartOfAccountDetail == null) {
                throw new CustomException("Sales Price List Detail data doesn't exist", HttpStatus.NOT_FOUND);
            }

            for (CustomerDepositTypeJnChartOfAccount customerDepositTypeJnChartOfAccount : listCustomerDepositTypeJnChartOfAccountDetail) {

                if (cusDeJncusDeJncoaRepo.findByCode(customerDepositTypeJnChartOfAccount.getCode()) == null) {
                    throw new CustomException("Data " + customerDepositTypeJnChartOfAccount.getCode() + "  Not Found!", HttpStatus.CONFLICT);
                }
                cusDeJncusDeJncoaRepo.deleteByCode(customerDepositTypeJnChartOfAccount.getCode());
            }

            //Insert New Detail
            int i = 1;
            for (CustomerDepositTypeJnChartOfAccount customerDepositTypeJnChartOfAccount : model.getListCustomerDepositTypeJnChartOfAccountDetail()) {
                customerDepositTypeJnChartOfAccount.setCode(model.getCode() + "-" + org.apache.commons.lang.StringUtils.leftPad(Integer.toString(i), 3, "0"));
                customerDepositTypeJnChartOfAccount.setCustomerDepositTypeCode(model.getCode());
                cusDeJncusDeJncoaRepo.save(customerDepositTypeJnChartOfAccount);
                i++;
            }
            
            return repo.save(model);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
    public void delete(String code) {
        try {
            
            repo.deleteByCode(code);
            
            List<CustomerDepositTypeJnChartOfAccount> listCustomerDepositTypeJnChartOfAccountDetail = cusDeJncusDeJncoaRepo.findDetailByCode(code);
            if (listCustomerDepositTypeJnChartOfAccountDetail == null) {
                throw new CustomException("Sales Price List Detail data doesn't exist", HttpStatus.NOT_FOUND);
            }

            for (CustomerDepositTypeJnChartOfAccount customerDepositTypeJnChartOfAccount : listCustomerDepositTypeJnChartOfAccountDetail) {

                if (cusDeJncusDeJncoaRepo.findByCode(customerDepositTypeJnChartOfAccount.getCode()) == null) {
                    throw new CustomException("Data " + customerDepositTypeJnChartOfAccount.getCode() + "  Not Found!", HttpStatus.CONFLICT);
                }
                cusDeJncusDeJncoaRepo.deleteByCode(customerDepositTypeJnChartOfAccount.getCode());
            }

        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    
}
