/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class ItemDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_item WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object getDataById(JSONObject obj, Class<?> module) {
        try {
            String qry = " SELECT"
                    + "	mst_item.code, "
                    + "	mst_item.name, "
                    + "	mst_item.itemSubCategoryCode, "
                    + "	mst_item_sub_category.Name AS itemSubCategoryName, "
                    + "	mst_item.unitOfMeasureCode, "
                    + "	mst_unit_of_measure.Name AS unitOfMeasureName, "
                    + "	mst_item.colorCode, "
                    + "	mst_color.Name AS colorName, "
                    + "	mst_item.itemClassificationCode, "
                    + "	mst_item_classification.Name AS itemClassificationName, "
                    + "	mst_item_classification.assemblyStatus, "
                    + "	mst_item_classification.salesOrderStatus, "
                    + "	mst_item_sub_category.itemCategoryCode, "
                    + "	mst_item_category.Name AS itemCategoryName, "
                    + "	mst_item_category.itemDivisionCode, "
                    + "	mst_item_division.Name AS itemDivisionName, "
                    + "	mst_item.itemBrandCode, "
                    + "	mst_item_brand.Name AS itemBrandName, "
                    + "	mst_item.inventoryType, "                    
                    + "	mst_item.defaultItemCatalogCode, "
                    + "	mst_item.minStock, "
                    + "	mst_item.maxStock, "
                    + "	mst_item.remark, "
                    + "	mst_item.activeStatus "
                    + " FROM mst_item "
                    + "LEFT JOIN mst_unit_of_measure  ON mst_item.UnitOfMeasureCode = mst_unit_of_measure.Code "
                    + "INNER JOIN mst_item_sub_category ON mst_item_sub_category.Code = mst_item.ItemSubCategoryCode "
                    + "INNER JOIN mst_item_category ON mst_item_category.Code = mst_item_sub_category.ItemCategoryCode "
                    + "INNER JOIN mst_item_division ON mst_item_division.Code = mst_item_category.ItemDivisionCode "
                    + "INNER JOIN mst_item_classification ON mst_item_classification.Code = mst_item.ItemClassificationCode "
                    + "INNER JOIN mst_item_brand ON mst_item_brand.Code = mst_item.ItemBrandCode "
                    + "INNER JOIN mst_color ON mst_color.Code = mst_item.ColorCode "
                    + " WHERE "
                    + "mst_item.Code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  COUNT(*) "
                    + "  FROM mst_item "
                    + "LEFT JOIN mst_unit_of_measure  ON mst_item.UnitOfMeasureCode = mst_unit_of_measure.Code "
                    + "INNER JOIN mst_item_sub_category ON mst_item_sub_category.Code = mst_item.ItemSubCategoryCode "
                    + "INNER JOIN mst_item_category ON mst_item_category.Code = mst_item_sub_category.ItemCategoryCode "
                    + "INNER JOIN mst_item_division ON mst_item_division.Code = mst_item_category.ItemDivisionCode "
                    + "INNER JOIN mst_item_brand ON mst_item_brand.Code = mst_item.ItemBrandCode "
                    + "INNER JOIN mst_color ON mst_color.Code = mst_item.ColorCode "
                    + "WHERE mst_item.code LIKE '%" + obj.get("code") + "%' "
                    + "  " + getStatus("mst_item.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "	mst_item.code, "
                    + "	mst_item.name, "
                    + "	mst_item.itemSubCategoryCode, "
                    + "	mst_item_sub_category.Name AS itemSubCategoryName, "
                    + "	mst_item.unitOfMeasureCode, "
                    + "	mst_unit_of_measure.Name AS unitOfMeasureName, "
                    + "	mst_item.colorCode, "
                    + "	mst_color.Name AS colorName, "
                    + "	mst_item.itemClassificationCode, "
                    + "	mst_item_sub_category.itemCategoryCode, "
                    + "	mst_item_category.Name AS itemCategoryName, "
                    + "	mst_item_category.itemDivisionCode, "
                    + "	mst_item_division.Name AS itemDivisionName, "
                    + "	mst_item.itemBrandCode, "
                    + "	mst_item_brand.Name AS itemBrandName, "
                    + "	mst_item.inventoryType, "
                    + "	mst_item.remark, "
                    + "	mst_item.activeStatus, "
                    + " IFNULL(onHandStok.ActualStock,0) AS onHandStock,   "
                    + " mst_item.MinStock AS minStock,  "
                    + " mst_item.MaxStock AS maxStock  "
                    + "FROM mst_item "
                    + "LEFT JOIN mst_unit_of_measure  ON mst_item.UnitOfMeasureCode = mst_unit_of_measure.Code "
                    + "INNER JOIN mst_item_sub_category ON mst_item_sub_category.Code = mst_item.ItemSubCategoryCode "
                    + "INNER JOIN mst_item_category ON mst_item_category.Code = mst_item_sub_category.ItemCategoryCode "
                    + "INNER JOIN mst_item_division ON mst_item_division.Code = mst_item_category.ItemDivisionCode "
                    + "INNER JOIN mst_item_brand ON mst_item_brand.Code = mst_item.ItemBrandCode "
                    + "INNER JOIN mst_color ON mst_color.Code = mst_item.ColorCode "
                    + "  LEFT JOIN( "
                    + " 	SELECT "
                    + " 		mst_item_jn_current_stock.ItemCode, "
                    + " 		SUM(mst_item_jn_current_stock.ActualStock) AS actualStock "
                    + " 	FROM "
                    + " 		mst_item_jn_current_stock "
//                    + " 	WHERE "
//                    + " 		mst_item_jn_current_stock.RackCode LIKE '%-DI%' "
                    + " 	GROUP BY mst_item_jn_current_stock.ItemCode "
                    + "  ) AS onHandStok ON onHandStok.ItemCode = mst_item.code  "
                    + " WHERE "
                    + "  mst_item.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_item.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_item.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY mst_item." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countCatalogNumberDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  COUNT(*) "
                    + "  FROM mst_item_jn_catalog_number "
                    + " LEFT JOIN mst_item ON mst_item.Code = mst_item_jn_catalog_number.ItemCode "
                    + "  WHERE mst_item.code = '" + obj.get("code") + "' "
                    + "  " + getStatus("mst_item.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults dataCatalogNumberDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + " itmJnCt.code AS catalogCode, "
                    + " itmJnCt.itemCode, "
                    + " itmJnCt.catalogNo, "
                    + " itmJnCt.remark, "
                    + " itmJnCt.activeStatus "
                    + " FROM mst_item_jn_catalog_number itmJnCt "
                    + " LEFT JOIN mst_item ON itmJnCt.ItemCode = mst_item.code "
                    + " WHERE itmJnCt.itemCode = '" + obj.get("code") + "' "
                    + " ORDER BY itmJnCt." + sort[0] + " " + sort[1];
            long countData = this.countCatalogNumberDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public PaginatedResults lookUpSO(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(item.code) FROM( SELECT";
            String list = "SELECT item.* FROM( SELECT " ;        
            String select = " "
                    + "	item.code, "
                    + "	item.name, "
                    + "	item.salesOrderStatus, "
                    + "	item.itemSubCategoryCode, "
                    + "	sc.Name itemSubCategoryName, "
                    + "	item.unitOfMeasureCode, "
                    + "	uom.Name unitOfMeasureName, "
                    + "	item.colorCode, "
                    + "	clr.Name colorName, "
                    + "	item.itemClassificationCode, "
                    + "	sc.itemCategoryCode, "
                    + "	ic.Name itemCategoryName, "
                    + "	ic.itemDivisionCode, "
                    + "	idv.Name itemDivisionName, "
                    + "	item.itemBrandCode, "
                    + "	item.Name itemBrandName, "
                    + "	item.inventoryType, "
                    + "	item.remark, "
                    + "	item.activeStatus, "
                    + " IFNULL(onHandStok.ActualStock,0) onHandStock,   "
                    + " item.MinStock minStock,  "
                    + " item.MaxStock maxStock  ";
            String qry = " "
                    + "FROM mst_item item "
                    + "LEFT JOIN mst_unit_of_measure uom ON item.UnitOfMeasureCode = uom.Code "
                    + "INNER JOIN mst_item_sub_category sc ON sc.Code = item.ItemSubCategoryCode "
                    + "INNER JOIN mst_item_category ic ON ic.Code = sc.ItemCategoryCode "
                    + "INNER JOIN mst_item_division idv ON idv.Code = ic.ItemDivisionCode "
                    + "INNER JOIN mst_item_brand ib ON ib.Code = item.ItemBrandCode "
                    + "INNER JOIN mst_color clr ON clr.Code = item.ColorCode "
                    + "  LEFT JOIN( "
                    + " 	SELECT "
                    + " 		mst_item_jn_current_stock.ItemCode, "
                    + " 		SUM(mst_item_jn_current_stock.ActualStock) AS actualStock "
                    + " 	FROM "
                    + " 		mst_item_jn_current_stock "
                    + " 	GROUP BY mst_item_jn_current_stock.ItemCode "
                    + "  ) onHandStok ON onHandStok.ItemCode = item.Code  "
                    + ")item "
                    + " WHERE item.Code LIKE '%" + obj.get("code") + "%' "
                    + "  AND item.Name LIKE '%" + obj.get("name") + "%' "
                    + "  AND item.salesOrderStatus = True "
                    + "ORDER BY item." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
           return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
