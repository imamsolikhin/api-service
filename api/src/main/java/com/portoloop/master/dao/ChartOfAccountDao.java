/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class ChartOfAccountDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_chart_of_account WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "   COUNT(*) "
                    + "  FROM mst_chart_of_account "
                    + "    INNER JOIN mst_currency ON mst_chart_of_account.currencyCode = mst_currency.code "
                    + " WHERE "
                    + "  mst_chart_of_account.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_chart_of_account.name LIKE '%" + obj.get("name") + "%' "
                    + "  AND IFNULL(mst_currency.code,'') LIKE '%" + obj.get("currencyCode") + "%' "
                    + "  AND IFNULL(mst_currency.name,'') LIKE '%" + obj.get("currencyName") + "%' "
                    + "  " + getStatus("mst_chart_of_account.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    mst_chart_of_account.code, "
                    + "    mst_chart_of_account.name, "
                    + "    mst_chart_of_account.accountType, "
                    + "    mst_chart_of_account.currencyCode, "
                    + "    IFNULL(mst_currency.name,'') AS currencyName, "
                    + "    mst_chart_of_account.pyqStatus, "
                    + "    mst_chart_of_account.iinStatus, "
                    + "    mst_chart_of_account.iotStatus, "
                    + "    mst_chart_of_account.bbmStatus, "
                    + "    mst_chart_of_account.bkkStatus, "
                    + "    mst_chart_of_account.bkmStatus, "
                    + "    mst_chart_of_account.gjmStatus, "
                    + "    mst_chart_of_account.adjStatus, "
                    + "    mst_chart_of_account.cavStatus, "
                    + "    mst_chart_of_account.budgetStatus, "
                    + "    mst_chart_of_account.activeStatus, "
                    + "    mst_chart_of_account.remark "
                    + "  FROM  "
                    + "    mst_chart_of_account "
                    + "    LEFT JOIN mst_currency ON mst_chart_of_account.currencyCode = mst_currency.code "
                    + " WHERE "
                    + "  mst_chart_of_account.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    mst_chart_of_account.code, "
                    + "    mst_chart_of_account.name, "
                    + "    mst_chart_of_account.accountType, "
                    + "    mst_chart_of_account.currencyCode, "
                    + "    IFNULL(mst_currency.name,'') AS currencyName, "
                    + "    mst_chart_of_account.pyqStatus, "
                    + "    mst_chart_of_account.iinStatus, "
                    + "    mst_chart_of_account.iotStatus, "
                    + "    mst_chart_of_account.bbmStatus, "
                    + "    mst_chart_of_account.bkkStatus, "
                    + "    mst_chart_of_account.bkmStatus, "
                    + "    mst_chart_of_account.gjmStatus, "
                    + "    mst_chart_of_account.adjStatus, "
                    + "    mst_chart_of_account.cavStatus, "
                    + "    mst_chart_of_account.budgetStatus, "
                    + "    mst_chart_of_account.remark "
                    + "  FROM  "
                    + "    mst_chart_of_account "
                    + "    INNER JOIN mst_currency ON mst_chart_of_account.currencyCode = mst_currency.code "
                    + " WHERE "
                    + "  mst_chart_of_account.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_chart_of_account.name LIKE '%" + obj.get("name") + "%' "
                    + "  AND IFNULL(mst_bank.code,'') LIKE '%" + obj.get("bankCode") + "%' "
                    + "  AND IFNULL(mst_bank.name,'') LIKE '%" + obj.get("bankName") + "%' "
                    + "  " + getStatus("mst_chart_of_account.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY mst_chart_of_account." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    mst_chart_of_account.code, "
                    + "    mst_chart_of_account.name, "
                    + "    mst_chart_of_account.accountType, "
                    + "    mst_chart_of_account.currencyCode, "
                    + "    IFNULL(mst_currency.name,'') AS currencyName, "
                    + "    mst_chart_of_account.pyqStatus, "
                    + "    mst_chart_of_account.iinStatus, "
                    + "    mst_chart_of_account.iotStatus, "
                    + "    mst_chart_of_account.bbmStatus, "
                    + "    mst_chart_of_account.bkkStatus, "
                    + "    mst_chart_of_account.bkmStatus, "
                    + "    mst_chart_of_account.gjmStatus, "
                    + "    mst_chart_of_account.adjStatus, "
                    + "    mst_chart_of_account.cavStatus, "
                    + "    mst_chart_of_account.budgetStatus, "
                    + "    mst_chart_of_account.remark "
                    + "  FROM  "
                    + "    mst_chart_of_account "
                    + "    LEFT JOIN mst_currency ON mst_chart_of_account.currencyCode = mst_currency.code "
                    + " WHERE "
                    + "  mst_chart_of_account.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_chart_of_account.name LIKE '%" + obj.get("name") + "%' "
                    + "  AND IFNULL(mst_currency.code,'') LIKE '%" + obj.get("currencyCode") + "%' "
                    + "  AND IFNULL(mst_currency.name,'') LIKE '%" + obj.get("currencyName") + "%' "
                    + "  " + getStatus("mst_chart_of_account.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY mst_chart_of_account." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(coa.code) FROM( SELECT";
            String list = "SELECT coa.* FROM( SELECT ";
            String select = " "
                    + "    coa.code, "
                    + "    coa.name, "
                    + "    coa.accountType, "
                    + "    coa.currencyCode, "
                    + "    IFNULL(curr.name,'') AS currencyName, "
                    + "    coa.pyqStatus, "
                    + "    coa.iinStatus, "
                    + "    coa.iotStatus, "
                    + "    coa.bbmStatus, "
                    + "    coa.bkkStatus, "
                    + "    coa.bkmStatus, "
                    + "    coa.gjmStatus, "
                    + "    coa.adjStatus, "
                    + "    coa.cavStatus, "
                    + "    coa.budgetStatus, "
                    + "    coa.remark ";
            String qry = " "
                    + "FROM mst_chart_of_account coa "
                    + "    LEFT JOIN mst_currency curr ON coa.currencyCode = curr.code "
                    + ")coa "
                    + "WHERE coa.Code LIKE '%" + obj.get("code") + "%' "
                    + "  AND coa.Name LIKE '%" + obj.get("name") + "%' "
                    + "  AND coa.accountType = 'S' "
                    + "ORDER BY coa." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
