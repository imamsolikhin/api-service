/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class DocumentTypeDao extends BaseQuery {

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + " mst_document_type_chart_of_account.currencyCode,\n"
                    + "	mst_document_type_chart_of_account.AccountCode AS chartOfAccountCode,\n"
                    + "	mst_chart_of_account.Name AS chartOfAccountName ";
            String qry = " "
                    + "FROM "
                    + "  mst_document_type_chart_of_account "
                    + "  INNER JOIN mst_chart_of_account ON mst_document_type_chart_of_account.AccountCode = mst_chart_of_account.Code "
                    + "WHERE mst_document_type_chart_of_account.DocumentTypeCode='" + obj.get("documentTypeCode") + "' "
                    + "ORDER BY mst_document_type_chart_of_account." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
