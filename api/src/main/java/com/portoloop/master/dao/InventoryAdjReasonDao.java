/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Irfaan
 */
@Repository
public class InventoryAdjReasonDao extends BaseQuery {

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "	mst_inventory_adj_reason_jn_branch.BranchCode AS branchCode, "
                    + "	mst_branch.Name AS branchName, "
                    + "	mst_inventory_adj_reason_jn_branch.ChartOfAccountCode AS chartOfAccountCode, "
                    + "	mst_chart_of_account.Name AS chartOfAccountName ";
            String qry = " "
                    + "FROM mst_inventory_adj_reason_jn_branch "
                    + "INNER JOIN mst_branch ON mst_inventory_adj_reason_jn_branch.BranchCode = mst_branch.Code "
                    + "INNER JOIN mst_chart_of_account ON mst_inventory_adj_reason_jn_branch.ChartOfAccountCode = mst_chart_of_account.Code "
                    + "WHERE mst_inventory_adj_reason_jn_branch.ReasonCode='" + obj.get("headerCode") + "' "
                    + "ORDER BY mst_inventory_adj_reason_jn_branch." + sort[0] + " " + sort[1];
              long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
