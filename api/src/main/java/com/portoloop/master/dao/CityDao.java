/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class CityDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_city WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_city "
              + "  	INNER JOIN mst_province ON mst_city.ProvinceCode = mst_province.`Code` "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + "  WHERE mst_city.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_city.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_city.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }
  
  public long countSearch(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT * FROM( "
              + " SELECT "
              + "   COUNT(*) "
                +  "FROM mst_city " 
                +  "INNER JOIN mst_province ON mst_city.`ProvinceCode` = mst_province.`Code` " 
                +  "INNER JOIN mst_island ON mst_province.`IslandCode` = mst_island.`Code` " 
                +  "INNER JOIN mst_country ON mst_island.`CountryCode` = mst_country.`Code` "
                +  ")searchQry "
              + "  WHERE searchQry.code LIKE '%" + obj.get("code") + "%' "
              + "  AND searchQry.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("searchQry.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_city.code, "
              + "  	 mst_city.name, "
              + "  	 mst_province.code as provinceCode, "
              + "  	 mst_province.name as provinceName, "
              + "  	 mst_island.code as islandCode, "
              + "  	 mst_island.name as islandName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_city.remark, "
              + "  	 mst_city.activeStatus "
              + "  FROM mst_city "
              + "  	INNER JOIN mst_province ON mst_city.ProvinceCode = mst_province.`Code` "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_city.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_city.code, "
              + "  	 mst_city.name, "
              + "  	 mst_province.code as provinceCode, "
              + "  	 mst_province.name as provinceName, "
              + "  	 mst_island.code as islandCode, "
              + "  	 mst_island.name as islandName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_city.remark, "
              + "  	 mst_city.activeStatus "
              + "  FROM mst_city "
              + "  	INNER JOIN mst_province ON mst_city.ProvinceCode = mst_province.`Code` "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_city.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_city.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_city.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_city." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " " 
              + "  SELECT "
              + "  	 mst_city.code, "
              + "  	 mst_city.name, "
              + "  	 mst_province.code as provinceCode, "
              + "  	 mst_province.name as provinceName, "
              + "  	 mst_island.code as islandCode, "
              + "  	 mst_island.name as islandName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_city.remark, "
              + "  	 mst_city.activeStatus "
              + "  FROM mst_city "
              + "  	INNER JOIN mst_province ON mst_city.ProvinceCode = mst_province.`Code` "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_city.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_city.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_city.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_city." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public PaginatedResults pagingSearch(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " " 
              + "  SELECT * FROM(  " 
              + "  SELECT  " 
                  +  "	mst_city.`code`, " 
                  +  "	mst_city.`name`, " 
                  +  "	mst_city.`provinceCode`, " 
                  +  "	IFNULL(mst_province.`Name`, '') AS provinceName, " 
                  +  "	mst_province.`islandCode`, "
                  +  "	IFNULL(mst_island.`Name`, '') AS islandName, " 
                  +  "	mst_island.`countryCode`, " 
                  +  "	IFNULL(mst_country.`Name`, '') AS countryName " 
                  +  "FROM mst_city " 
                  +  "INNER JOIN mst_province ON mst_city.`ProvinceCode` = mst_province.`Code` " 
                  +  "INNER JOIN mst_island ON mst_province.`IslandCode` = mst_island.`Code` " 
                  +  "INNER JOIN mst_country ON mst_island.`CountryCode` = mst_country.`Code` "
                  +  ")searchQry "
              + " WHERE "
              + "  searchQry.code LIKE '%" + obj.get("code") + "%' "
              + "  AND searchQry.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("searchQry.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY searchQry." + sort[0] + " " + sort[1];
      long countData = this.countSearch(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
