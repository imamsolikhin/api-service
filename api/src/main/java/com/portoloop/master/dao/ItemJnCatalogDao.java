/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class ItemJnCatalogDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_item_jn_catalog_number WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  COUNT(*) "
              + "  FROM mst_item_jn_catalog_number "
              + "  INNER JOIN mst_item ON mst_item_jn_catalog_number.itemCode = mst_item.code "
              + "  WHERE mst_item_jn_catalog_number.code LIKE '%" + obj.get("code") + "%' "
              + "  " + getStatus("mst_item_jn_catalog_number.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }
  
  public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT code, itemCode, catalogNo, remark, activeStatus "
              + " FROM " + getTableName(module) + " "
              + " WHERE "
              + " code LIKE '%" + obj.get("code") + "%' "
              + " ORDER BY " + getTableName(module) + "." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + " catnum.code, catnum.itemCode, catnum.catalogNo, catnum.remark, catnum.activeStatus "
              + " FROM mst_item_jn_catalog_number catnum "
              + " INNER JOIN mst_item ON mst_item.Code = catnum.ItemCode "
              + " WHERE "
              + "  catnum.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item_jn_catalog_number.code, "
              + "    mst_item_jn_catalog_number.name, "
              + "    mst_item_jn_catalog_number.acNo, "
              + "    mst_item_jn_catalog_number.acName, "
              + "    mst_item_jn_catalog_number.bankCode, "
              + "    IFNULL(mst_bank.name,'') as bankName, "
              + "    mst_item_jn_catalog_number.bankBranch, "
              + "    mst_item_jn_catalog_number.bbmVoucherNo, "
              + "    mst_item_jn_catalog_number.bbkVoucherNo, "
              + "    mst_item_jn_catalog_number.chartOfAccountCode, "
              + "    IFNULL(mst_chart_of_account.name,'') as chartOfAccountName, "
              + "    mst_item_jn_catalog_number.activeStatus, "
              + "    mst_item_jn_catalog_number.remark "
              + " FROM mst_item_jn_catalog_number "
              + " INNER JOIN mst_bank ON mst_item_jn_catalog_number.BankCode = mst_bank.code "
              + " INNER JOIN mst_chart_of_account ON mst_item_jn_catalog_number.BankCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_item_jn_catalog_number.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_item_jn_catalog_number.name LIKE '%" + obj.get("name") + "%' "
              + "  AND IFNULL(mst_bank.code,'') LIKE '%" + obj.get("bankCode") + "%' "
              + "  AND IFNULL(mst_bank.name,'') LIKE '%" + obj.get("bankName") + "%' "
              + "  " + getStatus("mst_item_jn_catalog_number.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_item_jn_catalog_number." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
