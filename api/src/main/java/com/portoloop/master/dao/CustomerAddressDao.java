/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class CustomerAddressDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_customer_jn_address WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "  mst_customer_jn_address.code, "
                    + "  mst_customer_jn_address.CustomerCode AS customerCode, "
                    + "  mst_customer_jn_address.Name AS name, "
                    + "  mst_customer_jn_address.Address AS address, "
                    + "  mst_customer_jn_address.NIK AS nik, "
                    + "  mst_customer_jn_address.Phone1 AS phone1 ";
            String qry = " "
                    + "FROM "
                    + "  mst_customer_jn_address "
                    + "WHERE " 
                    + "  mst_customer_jn_address.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_customer_jn_address.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_customer_jn_address.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_customer_jn_address." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public PaginatedResults pagingByCustomerCode(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "  mst_customer_jn_address.code, "
                    + "  mst_customer_jn_address.CustomerCode AS customerCode, "
                    + "  mst_customer_jn_address.Name AS name, "
                    + "  mst_customer_jn_address.Address AS address, "
                    + "  mst_customer_jn_address.NIK AS nik, "
                    + "  mst_customer_jn_address.Phone1 AS phone1 ";
            String qry = " "
                    + "FROM "
                    + "  mst_customer_jn_address "
                    + "WHERE mst_customer_jn_address.CustomerCode ='" + obj.get("customerCode") + "' "
                    + "AND mst_customer_jn_address.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_customer_jn_address.Name LIKE '%" + obj.get("name") + "%' "
                    + "ORDER BY mst_customer_jn_address." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  COUNT(mst_customer_jn_address.code) "
                    + "  FROM mst_customer_jn_address "
                    + "  INNER JOIN mst_customer ON mst_customer_jn_address.CustomerCode = mst_customer.Code "
                    + "  INNER JOIN mst_city ON mst_customer_jn_address.CityCode = mst_city.Code "
                    + "  WHERE mst_bank_account.Code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_bank_account.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_customer_jn_address.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long countLookup(JSONObject obj, Class<?> module) {
        try {
            String qry = "SELECT COUNT(mst_customer_jn_address.code) "
                    + "FROM mst_customer_jn_address "
                    + "INNER JOIN mst_customer ON mst_customer_jn_address.CustomerCode = mst_customer.Code "
                    + "INNER JOIN mst_city ON mst_customer_jn_address.CityCode = mst_city.Code "
                    + "WHERE mst_customer_jn_address.ActiveStatus=1 "
                    + "     AND mst_customer_jn_address.Code LIKE '%" + obj.get("code") + "%' "
                    + "     AND mst_customer_jn_address.Name LIKE '%" + obj.get("name") + "%' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = "SELECT "
                    + "     mst_customer_jn_address.code,  "
                    + "     mst_customer_jn_address.name,  "
                    + "     mst_customer_jn_address.address,  "
                    + "     mst_city.code as cityCode,  "
                    + "     mst_city.name as cityName,  "
                    + "     mst_customer_jn_address.phone1,  "
                    + "     mst_customer_jn_address.phone2,  "
                    + "     mst_customer_jn_address.npwpStatus, "
                    + "     mst_customer_jn_address.fax,  "
                    + "     mst_customer_jn_address.emailAddress,  "
                    + "     mst_customer_jn_address.zipCode,  "
                    + "     mst_customer_jn_address.contactPerson,  "
                    + "     mst_customer_jn_address.remark "
                    + "FROM mst_customer_jn_address "
                    + "INNER JOIN mst_customer ON mst_customer_jn_address.CustomerCode = mst_customer.Code "
                    + "INNER JOIN mst_city ON mst_customer_jn_address.CityCode = mst_city.Code "
                    + "WHERE mst_customer_jn_address.ActiveStatus=1 "
                    + "     AND mst_customer_jn_address.Code LIKE '%" + obj.get("code") + "%' "
                    + "     AND mst_customer_jn_address.Name LIKE '%" + obj.get("name") + "%' "
                    + "ORDER BY mst_customer_jn_address." + sort[0] + " " + sort[1];
            long countData = this.countLookup(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "      mst_customer_jn_address.code, "
                        + "  mst_customer_jn_address.name, "
                        + "  mst_customer_jn_address.CustomerCode AS customerCode, "
                        + "  mst_customer_jn_address.address, "
                        + "  mst_city.code AS cityCode, "
                        + "  mst_city.name AS cityName, "
                        + "  mst_country.Code AS countryCode, "
                        + "  mst_country.Name AS countryName, "
                        + "  mst_customer_jn_address.phone1, "
                        + "  mst_customer_jn_address.phone2, "
                        + "  mst_customer_jn_address.emailAddress, "
                        + "  mst_customer_jn_address.billToStatus, "
                        + "  mst_customer_jn_address.ShipToStatus, "
                        + "  mst_customer_jn_address.NPWPStatus AS npwpStatus, "
                        + "  mst_customer_jn_address.NPWP AS npwp, "
                        + "  mst_customer_jn_address.NPWPName AS npwpName, "
                        + "  mst_customer_jn_address.NPWPAddress AS npwpAddress, "
                        + "  mst_customer_jn_address.NPWPCityCode AS npwpCityCode, "
                        + "  npwp_city.Name AS npwpCityName, "
                        + "  npwp_province.Code AS npwpProvinceCode, "
                        + "  npwp_province.Name AS npwpProvinceName, "
                        + "  npwp_country.Code AS npwpCountryCode, "
                        + "  npwp_country.Name AS npwpCountryName, "
                        + "  mst_customer_jn_address.NPWPZipCode AS npwpZipCode, "
                        + "  mst_customer_jn_address.contactPerson "
                    + "  FROM mst_customer_jn_address "
                    + "  INNER JOIN mst_city AS npwp_city "
                    + "    ON mst_customer_jn_address.CityCode = npwp_city.Code "
                    + "  INNER JOIN mst_province AS npwp_province "
                    + "    ON npwp_city.`ProvinceCode` = npwp_province.`Code` "
                    + "  INNER JOIN mst_island AS npwp_island "
                    + "    ON npwp_province.`IslandCode` = npwp_island.`Code` "
                    + "  INNER JOIN mst_country AS npwp_country "
                    + "    ON npwp_island.`CountryCode` = npwp_country.`Code` "
                    + "  INNER JOIN mst_city "
                    + "    ON mst_customer_jn_address.CityCode = mst_city.Code "
                    + "  INNER JOIN mst_province "
                    + "    ON mst_city.`ProvinceCode` = mst_province.`Code` "
                    + "  INNER JOIN mst_island "
                    + "    ON mst_province.`IslandCode` = mst_island.`Code` "
                    + "  INNER JOIN mst_country "
                    + "    ON mst_island.`CountryCode` = mst_country.`Code` "
                    + " WHERE mst_customer_jn_address.ActiveStatus=1 "
                    + "  AND mst_customer_jn_address.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object dataBillTo(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "  	 mst_customer_jn_address.code, "
                    + "  	 mst_customer_jn_address.name, "
                    + "  	 mst_customer_jn_address.address, "
                    + "  	 mst_city.code as cityCode, "
                    + "  	 mst_city.name as cityName, "
                    + "  	 mst_customer_jn_address.phone1, "
                    + "  	 mst_customer_jn_address.phone2, "
                    + "        mst_customer_jn_address.npwpStatus,"
                    + "  	 mst_customer_jn_address.fax, "
                    + "  	 mst_customer_jn_address.emailAddress, "
                    + "  	 mst_customer_jn_address.zipCode, "
                    + "  	 mst_customer_jn_address.contactPerson, "
                    + "  	 mst_customer_jn_address.remark, "
                    + "  	 mst_customer_jn_address.activeStatus "
                    + "  FROM mst_customer_jn_address "
                    + "  	INNER JOIN mst_city ON mst_customer_jn_address.CityCode = mst_city.Code "
                    + "  	INNER JOIN mst_customer ON mst_customer_jn_address.Code = mst_customer.DefaultBillToCode "
                    + " WHERE "
                    + "  mst_customer.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object dataShipTo(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "  	 mst_customer_jn_address.code, "
                    + "  	 mst_customer_jn_address.name, "
                    + "  	 mst_customer_jn_address.address, "
                    + "  	 mst_city.code as cityCode, "
                    + "  	 mst_city.name as cityName, "
                    + "  	 mst_customer_jn_address.phone1, "
                    + "  	 mst_customer_jn_address.phone2, "
                    + "  	 mst_customer_jn_address.fax, "
                    + "        mst_customer_jn_address.npwpStatus,"
                    + "  	 mst_customer_jn_address.emailAddress, "
                    + "  	 mst_customer_jn_address.zipCode, "
                    + "  	 mst_customer_jn_address.contactPerson, "
                    + "  	 mst_customer_jn_address.remark, "
                    + "  	 mst_customer_jn_address.activeStatus "
                    + "  FROM mst_customer_jn_address "
                    + "  	INNER JOIN mst_city ON mst_customer_jn_address.CityCode = mst_city.Code "
                    + "  	INNER JOIN mst_customer ON mst_customer_jn_address.Code = mst_customer.DefaultShipToCode "
                    + " WHERE "
                    + "  mst_customer.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
