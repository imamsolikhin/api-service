/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class DiscountTypeDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM  mst_discount_type WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "   COUNT(mst_discount_type.Code) "
                    + "  FROM  mst_discount_type "
                    + "  WHERE  mst_discount_type.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND  mst_discount_type.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus(" mst_discount_type.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "     mst_discount_type.code, "
                    + "     mst_discount_type.name, "
                    + "     IFNULL(mst_discount_type.discountPercent01,0) as discountPercent01, "
                    + "     IFNULL(mst_discount_type.discountPercent02,0) as discountPercent02, "
                    + "     IFNULL(mst_discount_type.discountPercent03,0) as discountPercent03, "
                    + "     IFNULL(mst_discount_type.discountPercent04,0) as discountPercent04, "
                    + "     IFNULL(mst_discount_type.discountPercent05,0) as discountPercent05, "
                    + "     IFNULL(mst_discount_type.discountPercent06,0) as discountPercent06, "
                    + "     IFNULL(mst_discount_type.discountPercent07,0) as discountPercent07, "
                    + "     IFNULL(mst_discount_type.discountPercent08,0) as discountPercent08, "
                    + "     IFNULL(mst_discount_type.discountPercent09,0) as discountPercent09, "
                    + "     IFNULL(mst_discount_type.discountPercent10,0) as discountPercent10, "
                    + "     mst_discount_type.remark, "
                    + "     mst_discount_type.activeStatus "
                    + " FROM  mst_discount_type "
                    + " WHERE "
                    + "   mst_discount_type.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object calcData(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "     dt.code, "
                    + "     dt.name, "
                    + "     fn_gradual_discount_amount( "
                    + "     IFNULL('" + obj.get("prmAmount") + "',0), "
                    + "     IFNULL(dt.discountPercent01,0), "
                    + "     IFNULL(dt.discountPercent02,0), "
                    + "     IFNULL(dt.discountPercent03,0), "
                    + "     IFNULL(dt.discountPercent04,0), "
                    + "     IFNULL(dt.discountPercent05,0), "
                    + "     IFNULL(dt.discountPercent06,0), "
                    + "     IFNULL(dt.discountPercent07,0), "
                    + "     IFNULL(dt.discountPercent08,0), "
                    + "     IFNULL(dt.discountPercent09,0), "
                    + "     IFNULL(dt.discountPercent10,0),11 "
                    + "     ) discountAmountCalc, "
                    + "     dt.remark, "
                    + "     dt.activeStatus "
                    + " FROM  mst_discount_type dt "
                    + " WHERE "
                    + "   dt.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "     mst_discount_type.code, "
                    + "     mst_discount_type.name, "
                    + "     IFNULL(mst_discount_type.discountPercent01,0) as discountPercent01, "
                    + "     mst_discount_type.remark, "
                    + "     mst_discount_type.activeStatus "
                    + " FROM  mst_discount_type "
                    + " WHERE "
                    + "   mst_discount_type.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND  mst_discount_type.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus(" mst_discount_type.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY  mst_discount_type." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "     mst_discount_type.code, "
                    + "     mst_discount_type.name, "
                    + "     IFNULL(mst_discount_type.discountPercent01,0) as discountPercent01, "
                    + "     IFNULL(mst_discount_type.discountPercent02,0) as discountPercent02, "
                    + "     IFNULL(mst_discount_type.discountPercent03,0) as discountPercent03, "
                    + "     IFNULL(mst_discount_type.discountPercent04,0) as discountPercent04, "
                    + "     IFNULL(mst_discount_type.discountPercent05,0) as discountPercent05, "
                    + "     IFNULL(mst_discount_type.discountPercent06,0) as discountPercent06, "
                    + "     IFNULL(mst_discount_type.discountPercent07,0) as discountPercent07, "
                    + "     IFNULL(mst_discount_type.discountPercent08,0) as discountPercent08, "
                    + "     IFNULL(mst_discount_type.discountPercent09,0) as discountPercent09, "
                    + "     IFNULL(mst_discount_type.discountPercent10,0) as discountPercent10, "
                    + "     mst_discount_type.remark, "
                    + "     mst_discount_type.activeStatus "
                    + " FROM  mst_discount_type "
                    + " WHERE "
                    + "   mst_discount_type.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND  mst_discount_type.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus(" mst_discount_type.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY  mst_discount_type." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "mst_discount_type.code, "
                    + "mst_discount_type.name, "
                    + "mst_discount_type.discountPercent01, "
                    + "mst_discount_type.discountPercent02, "
                    + "mst_discount_type.discountPercent03, "
                    + "mst_discount_type.discountPercent04, "
                    + "mst_discount_type.discountPercent05, "
                    + "mst_discount_type.discountPercent06, "
                    + "mst_discount_type.discountPercent07, "
                    + "mst_discount_type.discountPercent08, "
                    + "mst_discount_type.discountPercent09, "
                    + "mst_discount_type.discountPercent10, "
                    + "mst_discount_type.remark, "
                    + "mst_discount_type.activeStatus ";
            String qry = " "
                    + "FROM "
                    + "  mst_discount_type "
                    + "WHERE mst_discount_type.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_discount_type.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_discount_type.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_discount_type." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
