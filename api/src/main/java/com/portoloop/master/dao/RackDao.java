/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class RackDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_rack WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT * FROM( "
                    + " SELECT "
                    + "   COUNT(*) "
                    + "  FROM mst_rack "
                    + "   INNER JOIN mst_warehouse ON mst_rack.`WarehouseCode` = mst_warehouse.`Code` "
                    + "   )racker "
                    + "  WHERE racker.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND racker.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("racker.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT * FROM( "
                    + " SELECT  "
                    + "    mst_rack.code, "
                    + "    mst_rack.name, "
                    + "    mst_rack.rackCategory, "
                    + "    mst_rack.WarehouseCode, "
                    + "    IFNULL(mst_warehouse.name,'') as warehouseName, "
                    + "    mst_rack.activeStatus "
                    + " FROM mst_rack "
                    + " INNER JOIN mst_warehouse ON mst_rack.`WarehouseCode` = mst_warehouse.`Code` "
                    + " )racker "
                    + " WHERE "
                    + "  racker.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT * FROM( "
                    + " SELECT  "
                    + "    mst_rack.code, "
                    + "    mst_rack.name, "
                    + "    mst_rack.rackCategory, "
                    + "    mst_rack.warehouseCode, "
                    + "    IFNULL(mst_warehouse.name,'') as warehouseName, "
                    + "    mst_rack.activeStatus "
                    + " FROM mst_rack "
                    + " INNER JOIN mst_warehouse ON mst_rack.`WarehouseCode` = mst_warehouse.`Code` "
                    + " )racker "
                    + " WHERE "
                    + "  racker.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND racker.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("racker.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY racker." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "  mst_rack.code, "
                    + "  mst_rack.name, "
                    + "  mst_rack.warehouseCode, "
                    + "  mst_rack.activeStatus, "
                    + "  mst_warehouse.Name AS warehouseName ";
            String qry = " "
                    + "FROM "
                    + "  mst_rack "
                    + "  INNER JOIN mst_warehouse "
                    + "    ON mst_rack.WarehouseCode = mst_warehouse.Code "
                    + "WHERE mst_rack.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_rack.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_rack.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_rack." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
