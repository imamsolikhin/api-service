/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class EmpolyeeDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_employee WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "mst_employee.Code AS code, "
                    + "  mst_employee.Name AS name, "
                    + "  mst_employee.Address AS address, "
                    + "  mst_employee.CityCode AS cityCode, "
                    + "  mst_city.Name AS cityName, "
                    + "  mst_country.Code AS countryCode, "
                    + "  mst_country.Name AS countryName, "
                    + "  mst_employee.ZipCode AS zipCode, "
                    + "  mst_employee.activeStatus, "
                    + "  mst_employee.EmailAddress AS emailAddress  ";
            String qry = " "
                    + "FROM "
                    + "  mst_employee "
                    + "  LEFT JOIN "
                    + "        mst_city      "
                    + "            ON mst_employee.CityCode = mst_city.Code    "
                    + "    LEFT JOIN "
                    + "        mst_province      "
                    + "            ON mst_city.`ProvinceCode` = mst_province.`Code`    "
                    + "    LEFT JOIN "
                    + "        mst_island      "
                    + "            ON mst_province.`IslandCode` = mst_island.`Code`    "
                    + "    LEFT JOIN "
                    + "        mst_country      "
                    + "            ON mst_island.`CountryCode` = mst_country.`Code`  "
                    + "WHERE mst_employee.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_employee.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_employee.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_employee." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
