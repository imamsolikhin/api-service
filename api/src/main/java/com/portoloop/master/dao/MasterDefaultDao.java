/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import com.portoloop.master.model.Bank;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.print.DocFlavor;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class MasterDefaultDao extends BaseQuery {

    private ModelMapper modelMapper;

    @Transactional
    public <T> T save(JSONObject obj, Class<T> module) throws IOException {
        session = em.unwrap(org.hibernate.Session.class);
        byte[] jsonData = obj.toString().getBytes();
        ObjectMapper mapper = new ObjectMapper();
        Bank c = mapper.readValue(jsonData, Bank.class);
//    modelMapper.map(obj, module);

        session.persist(c);
        return (T) module;
    }

    @Transactional
    public <T> T update(JSONObject obj, Class<T> module) {
        session = em.unwrap(org.hibernate.Session.class);
        modelMapper.map(obj, module);

        session.merge(module);
        return (T) module;
    }

    @Transactional
    public <T> T delete(JSONObject obj, Class<T> module) {
        session = em.unwrap(org.hibernate.Session.class);
        modelMapper.map(obj, module);

        session.remove(module);
        return (T) module;
    }

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM " + getTableName(module) + " WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM " + getTableName(module) + " "
                    + "  WHERE code LIKE '%" + obj.get("code") + "%' "
                    + "  AND name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT code,name,remark,activeStatus "
                    + " FROM " + getTableName(module) + " "
                    + " WHERE "
                    + "  code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object dataRateByDate(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "SELECT  "
                    + "rate.exchangeRate "
                    + "FROM mst_exchange_rate rate "
                    + "WHERE rate.CurrencyCode= '" + obj.get("currencyCode") + "' "
                    + "AND DATE(rate.TransactionDate) = DATE('" + obj.get("grnDate") + "') ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT code,name,remark,activeStatus "
                    + " FROM " + getTableName(module) + " "
                    + " WHERE "
                    + "  code LIKE '%" + obj.get("code") + "%' "
                    + "  AND name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY " + getTableName(module) + "." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT code,name,remark,activeStatus "
                    + " FROM " + getTableName(module) + " "
                    + " WHERE "
                    + "  code LIKE '%" + obj.get("code") + "%' "
                    + "  AND name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY " + getTableName(module) + "." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
