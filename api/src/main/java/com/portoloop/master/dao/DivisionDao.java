/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class DivisionDao extends BaseQuery {
    
    public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_division WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_division "
              + "   INNER JOIN mst_department ON mst_division.`DepartmentCode` = mst_department.`Code` "
              + "  WHERE mst_division.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_division.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_division.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_division.code, "
              + "    mst_division.name, "
              + "    mst_division.departmentCode, "
              + "    IFNULL(mst_department.name,'') as departmentName, "
              + "    mst_division.activeStatus, "
              + "    mst_division.remark "
              + " FROM mst_division "
              + " INNER JOIN mst_department ON mst_division.`DepartmentCode` = mst_department.`Code` "
              + " WHERE "
              + "  mst_division.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_division.code, "
              + "    mst_division.name, "
              + "    mst_division.departmentCode, "
              + "    IFNULL(mst_department.name,'') as departmentName, "
              + "    mst_division.activeStatus, "
              + "    mst_division.remark "
              + " FROM mst_division "
              + " INNER JOIN mst_department ON mst_division.`DepartmentCode` = mst_department.`Code` "
              + " WHERE "
              + "  mst_division.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_division.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_division.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_division." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_division.code, "
              + "    mst_division.name, "
              + "    mst_division.departmentCode, "
              + "    IFNULL(mst_department.name,'') as departmentName, "
              + "    mst_division.activeStatus, "
              + "    mst_division.remark "
              + " FROM mst_division "
              + " INNER JOIN mst_department ON mst_division.`DepartmentCode` = mst_department.`Code` "
              + " WHERE "
              + "  mst_division.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_division.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_division.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_division." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
    
}
