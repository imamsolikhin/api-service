/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class VendorDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_vendor WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "   COUNT(*) "
                    + "  FROM mst_vendor "
                    + "   INNER JOIN mst_city ON mst_vendor.CityCode = mst_city.code "
                    + "   INNER JOIN mst_province ON mst_city.provinceCode = mst_province.Code "
                    + "   INNER JOIN mst_island ON mst_province.islandCode = mst_island.Code "
                    + "   INNER JOIN mst_country ON mst_island.countryCode = mst_country.Code "
                    + "   INNER JOIN mst_payment_term term ON term.Code = mst_vendor.paymentTermCode "
                    + "   LEFT JOIN mst_chart_of_account ON mst_vendor.apChartOfAccountCode = mst_chart_of_account.Code "
                    + "  WHERE mst_vendor.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_vendor.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_vendor.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  mst_vendor.code, "
                    + "  mst_vendor.name, "
                    + "  mst_vendor.address, "
                    + "  mst_vendor.phone1, "
                    + "  mst_vendor.phone2, "
                    + "  mst_vendor.cityCode, "
                    + "  IFNULL(mst_city.name, '') AS cityName, "
                    + "  mst_city.provinceCode, "
                    + "  IFNULL(mst_province.name, '') AS provinceName, "
                    + "  mst_province.islandCode, "
                    + "  IFNULL(mst_island.name, '') AS islandName, "
                    + "  mst_island.countryCode, "
                    + "  IFNULL(mst_country.name, '') AS countryName, "
                    + "  mst_vendor.apChartOfAccountCode, "
                    + "  IFNULL(mst_chart_of_account.Name, '') AS apChartOfAccountName, "
                    + "  mst_vendor.zipCode, "
                    + "  mst_vendor.fax, "
                    + "  mst_vendor.emailAddress, "
                    + "  mst_vendor.VendorCategoryCode AS vendorCategoryCode, "
                    + "  mst_vendor_category.Name AS vendorCategoryName, "
                    + "  mst_vendor.paymentTermCode, "
                    + "  mst_payment_term.Name AS paymentTermName, "
                    + "  mst_payment_term.Days AS paymentTermDays, "
                    + "  mst_vendor.businessEntityCode, "
                    + "  mst_business_entity.Name AS businessEntityName, "
                    + "  mst_vendor.defaultContactPersonCode, "
                    + "  mst_vendor.NPWP AS npwp, "
                    + "  mst_vendor.NPWPName AS npwpName, "
                    + "  mst_vendor.NPWPAddress AS npwpAddress, "
                    + "  mst_vendor.NPWPZipCode AS npwpZipCode, "
                    + "  mst_vendor.NPWPCityCode AS npwpCityCode, "
                    + "  npwp_city.Name AS npwpCityName, "
                    + "  npwp_province.code AS npwpProvinceCode, "
                    + "  npwp_province.Name AS npwpProvinceName, "
                    + "  npwp_island.Code AS npwpIslandCode, "
                    + "  npwp_island.Name AS npwpIslandName, "
                    + "  npwp_country.Code AS npwpCountryCode, "
                    + "  npwp_country.Name AS npwpCountryName, "
                    + "  mst_vendor.activeStatus, "
                    + "  mst_vendor.remark "
                    + "FROM "
                    + "  mst_vendor "
                    + "  LEFT JOIN mst_business_entity "
                    + "    ON mst_vendor.BusinessEntityCode = mst_business_entity.Code "
                    + "  LEFT JOIN mst_payment_term "
                    + "    ON mst_vendor.PaymentTermCode = mst_payment_term.Code "
                    + "  LEFT JOIN mst_vendor_category "
                    + "    ON mst_vendor.VendorCategoryCode = mst_vendor_category.Code "
                    + "  INNER JOIN mst_city npwp_city "
                    + "    ON mst_vendor.CityCode = npwp_city.code "
                    + "  INNER JOIN mst_province npwp_province "
                    + "    ON npwp_city.provinceCode = npwp_province.code "
                    + "  INNER JOIN mst_island npwp_island "
                    + "    ON npwp_province.islandCode = npwp_island.code "
                    + "  INNER JOIN mst_country npwp_country "
                    + "    ON npwp_island.countryCode = npwp_country.code "
                    + "  INNER JOIN mst_city "
                    + "    ON mst_vendor.CityCode = mst_city.code "
                    + "  INNER JOIN mst_province "
                    + "    ON mst_city.provinceCode = mst_province.code "
                    + "  INNER JOIN mst_island "
                    + "    ON mst_province.islandCode = mst_island.code "
                    + "  INNER JOIN mst_country "
                    + "    ON mst_island.countryCode = mst_country.code "
                    + "  LEFT JOIN mst_chart_of_account "
                    + "    ON mst_vendor.apChartOfAccountCode = mst_chart_of_account.Code "
                    + " WHERE "
                    + "  mst_vendor.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "    mst_vendor.code, "
                    + "    mst_vendor.name, "
                    + "    mst_vendor.address, "
                    + "    mst_vendor.phone1, "
                    + "    mst_vendor.phone2, "
                    + "    mst_vendor.cityCode, "
                    + "    IFNULL(mst_city.name,'') as cityName, "
                    + "    mst_city.provinceCode, "
                    + "    IFNULL(mst_province.name,'') as provinceName, "
                    + "    mst_province.islandCode, "
                    + "    IFNULL(mst_island.name,'') as islandName, "
                    + "    mst_island.countryCode, "
                    + "    IFNULL(mst_country.name,'') AS countryName,  "
                    + "    mst_vendor.apChartOfAccountCode, "
                    + "    IFNULL(mst_chart_of_account.Name,'') AS apChartOfAccountName, "
                    + "    mst_vendor.zipCode, "
                    + "    mst_vendor.emailAddress, "
                    + "    mst_vendor.defaultContactPersonCode, "
                    + "    mst_vendor.activeStatus, "
                    + "    mst_vendor.remark "
                    + " FROM mst_vendor "
                    + "   INNER JOIN mst_city ON mst_vendor.CityCode = mst_city.code "
                    + "   INNER JOIN mst_province ON mst_city.provinceCode = mst_province.Code "
                    + "   INNER JOIN mst_island ON mst_province.islandCode = mst_island.Code "
                    + "   INNER JOIN mst_country ON mst_island.countryCode = mst_country.Code "
                    + "   LEFT JOIN mst_chart_of_account ON mst_vendor.apChartOfAccountCode = mst_chart_of_account.Code "
                    + " WHERE "
                    + "  mst_vendor.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_vendor.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_vendor.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY mst_vendor." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "    mst_vendor.code, "
                    + "    mst_vendor.name, "
                    + "    mst_vendor.address, "
                    + "    mst_vendor.phone1, "
                    + "    mst_vendor.phone2, "
                    + "    mst_vendor.cityCode, "
                    + "    IFNULL(mst_city.name,'') as cityName, "
                    + "    mst_vendor.apChartOfAccountCode, "
                    + "    IFNULL(mst_chart_of_account.Name,'') AS apChartOfAccountName, "
                    + "    mst_island.countryCode, "
                    + "    IFNULL(mst_country.Name,'') AS countryName,  "
                    + "    mst_vendor.zipCode, "
                    + "    mst_vendor.emailAddress, "
                    + "    mst_vendor.defaultContactPersonCode, "
                    + "    mst_vendor.paymentTermCode, "
                    + "    term.Name paymentTermName, "
                    + "    term.Days paymentTermDays, "
                    + "    mst_vendor.activeStatus, "
                    + "    mst_vendor.remark "
                    + " FROM mst_vendor "
                    + "   INNER JOIN mst_city ON mst_vendor.CityCode = mst_city.code "
                    + "   INNER JOIN mst_province ON mst_city.provinceCode = mst_province.Code "
                    + "   INNER JOIN mst_island ON mst_province.islandCode = mst_island.Code "
                    + "   INNER JOIN mst_country ON mst_island.countryCode = mst_country.Code "
                    + "   INNER JOIN mst_payment_term term ON term.Code = mst_vendor.paymentTermCode "
                    + "   LEFT JOIN mst_chart_of_account ON mst_vendor.apChartOfAccountCode = mst_chart_of_account.Code "
                    + " WHERE "
                    + "  mst_vendor.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_vendor.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_vendor.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY mst_vendor." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
