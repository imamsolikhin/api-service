/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class ExchangeRateDao extends BaseQuery {

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "  DATE_FORMAT(mst_exchange_rate.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "  mst_exchange_rate.exchangeRate ";
            String qry = " "
                    + "FROM "
                    + "  mst_exchange_rate "
                    + "WHERE mst_exchange_rate.CurrencyCode='" + obj.get("currencyCode") + "' "
                    + "AND mst_exchange_rate.TransactionDate BETWEEN '" + obj.get("startDate") + "' AND '" + obj.get("lastDate") + "' "
                    + "ORDER BY mst_exchange_rate." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
