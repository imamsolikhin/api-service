/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class CustomerJnContactDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_customer_jn_contact WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "    mst_customer_jn_contact.code, "
                    + "        mst_customer_jn_contact.CustomerCode AS customerCode, "
                    + "        mst_customer_jn_contact.Name AS name, "
                    + "        mst_customer_jn_contact.Phone AS phone, "
                    + "        mst_customer_jn_contact.MobileNo AS mobileNo, "
                    + "        mst_customer_jn_contact.BirthDate AS birthDate, "
                    + "        mst_customer_jn_contact.JobPositionCode AS jobPositionCode, "
                    + "        mst_job_position.Name AS jobPositionName, "
                    + "        mst_customer_jn_contact.activeStatus   "
                    + " FROM mst_customer_jn_contact "
                    + "  INNER JOIN mst_job_position ON mst_customer_jn_contact.`JobPositionCode` = mst_job_position.code  "
                    + " WHERE "
                    + "  mst_customer_jn_contact.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "    mst_customer_jn_contact.code, "
                    + "    mst_customer_jn_contact.CustomerCode, "
                    + "    mst_customer_jn_contact.Name, "
                    + "    mst_customer_jn_contact.Phone, "
                    + "    mst_customer_jn_contact.MobileNo, "
                    + "    mst_customer_jn_contact.BirthDate, "
                    + "    mst_customer_jn_contact.JobPositionCode, "
                    + "    mst_job_position.Name AS jobPositionName, "
                    + "    mst_customer_jn_contact.activeStatus "
                    + " FROM mst_customer_jn_contact "
                    + "  INNER JOIN mst_job_position ON mst_customer_jn_contact.`JobPositionCode` = mst_job_position.code  "
                    + " WHERE "
                    + "  mst_customer_jn_contact.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_customer_jn_contact.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_customer_jn_contact.activeStatus", obj.get("activeStatus").toString()) + " "
                    + " ORDER BY mst_customer_jn_contact." + sort[0] + " " + sort[1];
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "mst_customer_jn_contact.code, "
                    + "  mst_customer_jn_contact.CustomerCode AS customerCode, "
                    + "  mst_customer_jn_contact.Name AS name, "
                    + "  mst_customer_jn_contact.Phone AS phone, "
                    + "  mst_customer_jn_contact.MobileNo AS mobileNo, "
                    + "  mst_customer_jn_contact.BirthDate AS birthDate, "
                    + "  mst_customer_jn_contact.JobPositionCode AS jobPositionCode, "
                    + "  mst_job_position.Name AS jobPositionName , "
                    + "  mst_customer_jn_contact.activeStatus AS activeStatus ";
            String qry = " "
                    + "FROM "
                    + "  mst_customer_jn_contact "
                    + "  INNER JOIN mst_job_position "
                    + "    ON mst_customer_jn_contact.`JobPositionCode` = mst_job_position.code "
                    + "WHERE " 
                    + "  mst_customer_jn_contact.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_customer_jn_contact.name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_customer_jn_contact.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_customer_jn_contact." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingByCustomerCode(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "mst_customer_jn_contact.code, "
                    + "  mst_customer_jn_contact.CustomerCode AS customerCode, "
                    + "  mst_customer_jn_contact.Name AS name, "
                    + "  mst_customer_jn_contact.Phone AS phone, "
                    + "  mst_customer_jn_contact.MobileNo AS mobileNo, "
                    + "  mst_customer_jn_contact.BirthDate AS birthDate, "
                    + "  mst_customer_jn_contact.JobPositionCode AS jobPositionCode, "
                    + "  mst_job_position.Name AS jobPositionName , "
                    + "  mst_customer_jn_contact.activeStatus AS activeStatus ";
            String qry = " "
                    + "FROM "
                    + "  mst_customer_jn_contact "
                    + "  INNER JOIN mst_job_position "
                    + "    ON mst_customer_jn_contact.`JobPositionCode` = mst_job_position.code "
                    + "WHERE mst_customer_jn_contact.CustomerCode ='" + obj.get("customerCode") + "' "
                    + "ORDER BY mst_customer_jn_contact." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
