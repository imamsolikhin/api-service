/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.hibernate.Session;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author irfan
 */
@Repository
public class PriceListDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_price_list WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    
    @Transactional
    public void updateAppovalStatus(String code, String approvalStatus) {

        try {
            Session session = em.unwrap(Session.class);
            String qry = " "
                    + "UPDATE mst_price_list "
                    + "SET mst_price_list.ApprovalStatus ='"+approvalStatus+"' "
                    + "WHERE mst_price_list.Code = '" + code + "' ";

            session.createNativeQuery(qry).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  mst_price_list.code, "
                    + "  DATE_FORMAT(mst_price_list.effectiveDate, '%d-%m-%Y') effectiveDate, "
                    + "  mst_price_list.refNo, "
                    + "  mst_price_list.remark "
                    + "FROM "
                    + "  mst_price_list "
                    + " WHERE "
                    + "     mst_price_list.Code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "mst_price_list.code, "
                    + "  mst_price_list.priceTypeCode, "
                    + "  mst_price_list.effectiveDate, "
                    + "  mst_price_list.refNo, "
                    + "  mst_price_list.remark ";
            String qry = " "
                    + "FROM "
                    + "  mst_price_list "
                    + "WHERE mst_price_list.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_price_list.EffectiveDate BETWEEN '" + obj.get("effectiveStartDate") + "' AND '" + obj.get("effectiveEndDate") + "'"
                    + "  " + getStatus("mst_price_list.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "AND mst_price_list.ApprovalStatus='"+obj.get("approvalStatus")+"' "
                    + "ORDER BY mst_price_list." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "	  mst_price_list_item.itemCode, "
                    + "  mst_item.name AS itemName, "
                    + "  mst_item.itemSubCategoryCode, "
                    + "  mst_item_division.code AS itemDivisionCode, "
                    + "  mst_item.unitOfMeasureCode, "
                    + "  mst_price_list_item.priceList ";
            String qry = " "
                    + "FROM mst_price_list_item "
                    + "  INNER JOIN mst_item "
                    + "    ON mst_price_list_item.ItemCode = mst_item.Code "
                    + "  INNER JOIN mst_item_sub_category "
                    + "    ON mst_item.ItemSubCategoryCode = mst_item_sub_category.Code "
                    + "  INNER JOIN mst_item_category "
                    + "    ON mst_item_category.Code = mst_item_sub_category.ItemCategoryCode "
                    + "  INNER JOIN mst_item_division "
                    + "    ON mst_item_division.Code = mst_item_category.ItemDivisionCode  "
                    + "WHERE mst_price_list_item.HeaderCode='" + obj.get("headerCode") + "' "
                    + "ORDER BY mst_price_list_item." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
