/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class VendorDepositTypeDao extends BaseQuery {

    public Object dataPerBranch(JSONObject obj, Class<?> module) {
        try {
            String qry = "SELECT   "
                    + "	mst_vendor_deposit_type.code,  "
                    + "	mst_vendor_deposit_type.name,  "
                    + "	IFNULL(mst_chart_of_account.Code,'') AS chartOfAccountCode,  "
                    + "	IFNULL(mst_chart_of_account.Name,'') AS chartOfAccountName "
                    + "FROM mst_vendor_deposit_type  "
                    + "LEFT JOIN mst_vendor_deposit_type_jn_chart_of_account ON mst_vendor_deposit_type_jn_chart_of_account.VendorDepositTypeCode = mst_vendor_deposit_type.Code  "
                    + "LEFT JOIN mst_chart_of_account ON mst_chart_of_account.Code = mst_vendor_deposit_type_jn_chart_of_account.ChartOfAccountCode  "
                    + "WHERE mst_vendor_deposit_type.Code = '" + obj.get("code") + "'  "
                    + "AND mst_vendor_deposit_type_jn_chart_of_account.BranchCode = '" + obj.get("branchCode") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT   "
                    + "	mst_vendor_deposit_type.code,  "
                    + "	mst_vendor_deposit_type.name,  "
                    + "	IFNULL(mst_chart_of_account.Code,'') AS chartOfAccountCode,  "
                    + "	IFNULL(mst_chart_of_account.Name,'') AS chartOfAccountName ";
            String qry = " "
                    + "FROM mst_vendor_deposit_type  "
                    + "LEFT JOIN mst_vendor_deposit_type_jn_chart_of_account ON mst_vendor_deposit_type_jn_chart_of_account.VendorDepositTypeCode = mst_vendor_deposit_type.Code  "
                    + "LEFT JOIN mst_chart_of_account ON mst_chart_of_account.Code = mst_vendor_deposit_type_jn_chart_of_account.ChartOfAccountCode  "
                    + "WHERE mst_vendor_deposit_type.Code LIKE '%" + obj.get("code") + "%'  "
                    + "AND mst_vendor_deposit_type.Name LIKE '%" + obj.get("name") + "%'  "
                    + "AND mst_vendor_deposit_type_jn_chart_of_account.BranchCode = '" + obj.get("branchCode") + "' "
                    + "ORDER BY mst_vendor_deposit_type_jn_chart_of_account." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "  mst_vendor_deposit_type_jn_chart_of_account.branchCode, "
                    + "  mst_branch.Name AS branchName, "
                    + "  mst_vendor_deposit_type_jn_chart_of_account.chartOfAccountCode, "
                    + "  mst_chart_of_account.Name AS chartOfAccountName ";
            String qry = " "
                    + "FROM "
                    + "  mst_vendor_deposit_type_jn_chart_of_account "
                    + "  INNER JOIN mst_branch "
                    + "    ON mst_vendor_deposit_type_jn_chart_of_account.BranchCode = mst_branch.code "
                    + "  INNER JOIN mst_chart_of_account "
                    + "    ON mst_vendor_deposit_type_jn_chart_of_account.ChartOfAccountCode = mst_chart_of_account.Code "
                    + "WHERE mst_vendor_deposit_type_jn_chart_of_account.VendorDepositTypeCode='" + obj.get("vendorDepositTypeCode") + "' "
                    + "ORDER BY mst_vendor_deposit_type_jn_chart_of_account." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
