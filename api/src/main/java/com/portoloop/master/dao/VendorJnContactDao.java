/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class VendorJnContactDao extends BaseQuery{
    
    public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_vendor_jn_contact WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_vendor_jn_contact "
              + "  INNER JOIN mst_job_position ON mst_vendor_jn_contact.`JobPositionCode` = mst_job_position.code  "
//              + "  INNER JOIN mst_vendor ON mst_vendor_jn_contact.`VendorCode` = mst_vendor.code  "
              + "  WHERE mst_vendor_jn_contact.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_vendor_jn_contact.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_vendor_jn_contact.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_vendor_jn_contact.code, "
              + "    mst_vendor_jn_contact.vendorCode, "
              + "    mst_vendor_jn_contact.name, "
              + "    mst_vendor_jn_contact.phone, "
              + "    mst_vendor_jn_contact.mobileNo, "
              + "    mst_vendor_jn_contact.birthDate, "
              + "    mst_vendor_jn_contact.jobPositionCode, "
              + "    mst_job_position.Name AS jobPositionName, "
              + "    mst_vendor_jn_contact.activeStatus "
              + " FROM mst_vendor_jn_contact "
              + "  INNER JOIN mst_job_position ON mst_vendor_jn_contact.`JobPositionCode` = mst_job_position.code  "
//              + "  INNER JOIN mst_vendor ON mst_vendor_jn_contact.`VendorCode` = mst_vendor.code  "
              + " WHERE "
              + "  mst_vendor_jn_contact.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_vendor_jn_contact.code, "
              + "    mst_vendor_jn_contact.VendorCode, "
              + "    mst_vendor_jn_contact.Name, "
              + "    mst_vendor_jn_contact.Phone, "
              + "    mst_vendor_jn_contact.MobileNo, "
              + "    mst_vendor_jn_contact.BirthDate, "
              + "    mst_vendor_jn_contact.JobPositionCode, "
              + "    mst_job_position.Name AS jobPositionName, "
              + "    mst_vendor_jn_contact.activeStatus "
              + " FROM mst_vendor_jn_contact "
              + "  INNER JOIN mst_job_position ON mst_vendor_jn_contact.`JobPositionCode` = mst_job_position.code  "
//              + "  INNER JOIN mst_vendor ON mst_vendor_jn_contact.`VendorCode` = mst_vendor.code  "
              + " WHERE "
              + "  mst_vendor_jn_contact.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_vendor_jn_contact.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_vendor_jn_contact.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_vendor_jn_contact." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_vendor_jn_contact.code, "
              + "    mst_vendor_jn_contact.vendorCode, "
              + "    mst_vendor_jn_contact.name, "
              + "    mst_vendor_jn_contact.phone, "
              + "    mst_vendor_jn_contact.mobileNo, "
              + "    mst_vendor_jn_contact.birthDate, "
              + "    mst_vendor_jn_contact.jobPositionCode, "
              + "    mst_job_position.Name AS jobPositionName, "
              + "    mst_vendor_jn_contact.activeStatus "
              + " FROM mst_vendor_jn_contact "
              + "  INNER JOIN mst_job_position ON mst_vendor_jn_contact.`JobPositionCode` = mst_job_position.code  "
//              + "  INNER JOIN mst_vendor ON mst_vendor_jn_contact.`VendorCode` = mst_vendor.code  "
              + " WHERE "
              + "  mst_vendor_jn_contact.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_vendor_jn_contact.name LIKE '%" + obj.get("name") + "%' "
              + "  AND mst_vendor_jn_contact.VendorCode LIKE '%" + obj.get("vendorCode") + "%' "
              + "  " + getStatus("mst_vendor_jn_contact.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_vendor_jn_contact." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
    
}
