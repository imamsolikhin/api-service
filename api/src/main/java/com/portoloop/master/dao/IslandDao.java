/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class IslandDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_island WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_island "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + "  WHERE mst_city.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_island.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_city.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_island.code, "
              + "  	 mst_island.name, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_island.remark, "
              + "  	 mst_island.activeStatus "
              + "  FROM mst_island "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_island.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_island.code, "
              + "  	 mst_island.name, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_island.remark, "
              + "  	 mst_island.activeStatus "
              + "  FROM mst_island "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_island.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_island.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_island.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_island." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " " 
              + "  SELECT "
              + "  	 mst_island.code, "
              + "  	 mst_island.name, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_island.remark, "
              + "  	 mst_island.activeStatus "
              + "  FROM mst_island "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_island.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_island.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_island.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_island." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
