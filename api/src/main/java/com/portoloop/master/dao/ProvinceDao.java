/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class ProvinceDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_province WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_province "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + "  WHERE mst_city.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_province.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_province.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_province.code, "
              + "  	 mst_province.name, "
              + "  	 mst_island.code as islandCode, "
              + "  	 mst_island.name as islandName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_province.remark, "
              + "  	 mst_province.activeStatus "
              + "  FROM mst_province "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_province.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_province.code, "
              + "  	 mst_province.name, "
              + "  	 mst_island.code as islandCode, "
              + "  	 mst_island.name as islandName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_province.remark, "
              + "  	 mst_province.activeStatus "
              + "  FROM mst_province "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_province.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_province.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_province.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_province." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " " 
              + "  SELECT "
              + "  	 mst_province.code, "
              + "  	 mst_province.name, "
              + "  	 mst_island.code as islandCode, "
              + "  	 mst_island.name as islandName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_province.remark, "
              + "  	 mst_province.activeStatus "
              + "  FROM mst_province "
              + "  	INNER JOIN mst_island ON mst_province.IslandCode = mst_island.`Code` "
              + "  	INNER JOIN mst_country ON mst_island.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_province.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_province.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_province.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_province." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
