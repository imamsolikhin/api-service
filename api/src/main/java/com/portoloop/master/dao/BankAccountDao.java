/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class BankAccountDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_bank_account WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_bank_account "
              + "   INNER JOIN mst_bank ON mst_bank_account.BankCode = mst_bank.code "
              + "   INNER JOIN mst_chart_of_account ON mst_bank_account.ChartOfAccountCode = mst_chart_of_account.code "
              + "  WHERE mst_bank_account.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_bank_account.name LIKE '%" + obj.get("name") + "%' "
              + "  AND IFNULL(mst_bank.code,'') LIKE '%" + obj.get("bankCode") + "%' "
              + "  AND IFNULL(mst_bank.name,'') LIKE '%" + obj.get("bankName") + "%' "
              + "  " + getStatus("mst_bank_account.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_bank_account.code, "
              + "    mst_bank_account.name, "
              + "    mst_bank_account.acNo, "
              + "    mst_bank_account.acName, "
              + "    mst_bank_account.bankCode, "
              + "    IFNULL(mst_bank.name,'') as bankName, "
              + "    mst_bank_account.bankBranch, "
              + "    mst_bank_account.bbmVoucherNo, "
              + "    mst_bank_account.bbkVoucherNo, "
              + "    mst_bank_account.chartOfAccountCode, "
              + "    IFNULL(mst_chart_of_account.name,'') as chartOfAccountName, "
              + "    mst_bank_account.activeStatus, "
              + "    mst_bank_account.remark "
              + " FROM mst_bank_account "
              + " INNER JOIN mst_bank ON mst_bank_account.BankCode = mst_bank.code "
              + " INNER JOIN mst_chart_of_account ON mst_bank_account.ChartOfAccountCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_bank_account.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_bank_account.code, "
              + "    mst_bank_account.name, "
              + "    mst_bank_account.acNo, "
              + "    mst_bank_account.acName, "
              + "    mst_bank_account.bankCode, "
              + "    IFNULL(mst_bank.name,'') as bankName, "
              + "    mst_bank_account.bankBranch, "
              + "    mst_bank_account.bbmVoucherNo, "
              + "    mst_bank_account.bbkVoucherNo, "
              + "    mst_bank_account.chartOfAccountCode, "
              + "    IFNULL(mst_chart_of_account.name,'') as chartOfAccountName, "
              + "    mst_bank_account.activeStatus, "
              + "    mst_bank_account.remark "
              + " FROM mst_bank_account "
              + " INNER JOIN mst_bank ON mst_bank_account.BankCode = mst_bank.code "
              + " INNER JOIN mst_chart_of_account ON mst_bank_account.BankCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_bank_account.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_bank_account.name LIKE '%" + obj.get("name") + "%' "
              + "  AND IFNULL(mst_bank.code,'') LIKE '%" + obj.get("bankCode") + "%' "
              + "  AND IFNULL(mst_bank.name,'') LIKE '%" + obj.get("bankName") + "%' "
              + "  " + getStatus("mst_bank_account.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_bank_account." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_bank_account.code, "
              + "    mst_bank_account.name, "
              + "    mst_bank_account.acNo, "
              + "    mst_bank_account.acName, "
              + "    mst_bank_account.bankCode, "
              + "    IFNULL(mst_bank.name,'') as bankName, "
              + "    mst_bank_account.bankBranch, "
              + "    mst_bank_account.bbmVoucherNo, "
              + "    mst_bank_account.bbkVoucherNo, "
              + "    mst_bank_account.chartOfAccountCode, "
              + "    IFNULL(mst_chart_of_account.name,'') as chartOfAccountName, "
              + "    mst_bank_account.activeStatus, "
              + "    mst_bank_account.remark "
              + " FROM mst_bank_account "
              + " INNER JOIN mst_bank ON mst_bank_account.BankCode = mst_bank.code "
              + " INNER JOIN mst_chart_of_account ON mst_bank_account.ChartOfAccountCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_bank_account.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_bank_account.name LIKE '%" + obj.get("name") + "%' "
              + "  AND IFNULL(mst_bank.code,'') LIKE '%" + obj.get("bankCode") + "%' "
              + "  AND IFNULL(mst_bank.name,'') LIKE '%" + obj.get("bankName") + "%' "
              + "  " + getStatus("mst_bank_account.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_bank_account." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
