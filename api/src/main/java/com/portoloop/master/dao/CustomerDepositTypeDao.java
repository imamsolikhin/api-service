/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class CustomerDepositTypeDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_customer_deposit_type_jn_chart_of_account cudet_coa "
              + "    INNER JOIN mst_branch branch ON cudet_coa.BranchCode = branch.Code "
              + "    LEFT JOIN mst_chart_of_account coa ON cudet_coa.ChartOfAccountCode = coa.code "
              + " WHERE "
              + "  cudet_coa.CustomerDepositTypeCode = '" + obj.get("code") + "' " ;
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }
    
    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT   "
                    + "	mst_customer_deposit_type.code,  "
                    + "	mst_customer_deposit_type.name,  "
                    + "	IFNULL(mst_chart_of_account.Code,'') AS chartOfAccountCode,  "
                    + "	IFNULL(mst_chart_of_account.Name,'') AS chartOfAccountName ";
            String qry = " "
                    + "FROM mst_customer_deposit_type  "
                    + "LEFT JOIN mst_customer_deposit_type_jn_chart_of_account ON mst_customer_deposit_type_jn_chart_of_account.CustomerDepositTypeCode = mst_customer_deposit_type.Code  "
                    + "LEFT JOIN mst_chart_of_account ON mst_chart_of_account.Code = mst_customer_deposit_type_jn_chart_of_account.ChartOfAccountCode  "
                    + "WHERE mst_customer_deposit_type.Code LIKE '%" + obj.get("code") + "%'  "
                    + "AND mst_customer_deposit_type.Name LIKE '%" + obj.get("name") + "%'  "
                    + "AND mst_customer_deposit_type_jn_chart_of_account.BranchCode = '" + obj.get("branchCode") + "' "
                    + "ORDER BY mst_customer_deposit_type_jn_chart_of_account." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object dataPerBranch(JSONObject obj, Class<?> module) {
        try {
            String qry = "SELECT   "
                    + "	mst_customer_deposit_type.code,  "
                    + "	mst_customer_deposit_type.name,  "
                    + "	IFNULL(mst_chart_of_account.Code,'') AS chartOfAccountCode,  "
                    + "	IFNULL(mst_chart_of_account.Name,'') AS chartOfAccountName "
                    + "FROM mst_customer_deposit_type  "
                    + "LEFT JOIN mst_customer_deposit_type_jn_chart_of_account ON mst_customer_deposit_type_jn_chart_of_account.CustomerDepositTypeCode = mst_customer_deposit_type.Code  "
                    + "LEFT JOIN mst_chart_of_account ON mst_chart_of_account.Code = mst_customer_deposit_type_jn_chart_of_account.ChartOfAccountCode  "
                    + "WHERE mst_customer_deposit_type.Code = '" + obj.get("code") + "'  "
                    + "AND mst_customer_deposit_type_jn_chart_of_account.BranchCode = '" + obj.get("branchCode") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
  
  public PaginatedResults coa(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "    cudet_coa.code, "
              + "    cudet_coa.customerDepositTypeCode, "
              + "    cudet_coa.branchCode, "
              + "    IFNULL(branch.name,'') AS branchName, "
              + "    cudet_coa.chartOfAccountCode, "
              + "    IFNULL(coa.name,'') AS chartOfAccountName "
              + "  FROM  "
              + "    mst_customer_deposit_type_jn_chart_of_account cudet_coa "
              + "    INNER JOIN mst_branch branch ON cudet_coa.BranchCode = branch.Code "
              + "    LEFT JOIN mst_chart_of_account coa ON cudet_coa.ChartOfAccountCode = coa.code "
              + " WHERE "
              + "  cudet_coa.CustomerDepositTypeCode = '" + obj.get("code") + "' "
              + " ORDER BY cudet_coa." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
}
