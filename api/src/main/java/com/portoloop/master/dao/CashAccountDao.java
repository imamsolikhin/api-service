/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class CashAccountDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_cash_account WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_cash_account "
              + "    INNER JOIN mst_chart_of_account ON mst_cash_account.chartOfAccountCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_cash_account.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_cash_account.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_cash_account.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + " mst_cash_account.code, "
              + " mst_cash_account.name, "
              + " mst_cash_account.chartOfAccountCode, "
              + " mst_chart_of_account.name as chartOfAccountName, "
              + " mst_cash_account.bkmVoucherNo, "
              + " mst_cash_account.bkkVoucherNo, "
              + " mst_cash_account.activeStatus, "
              + " mst_cash_account.remark  "
              + "  FROM  "
              + "    mst_cash_account "
              + "    INNER JOIN mst_chart_of_account ON mst_cash_account.chartOfAccountCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_cash_account.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + " mst_cash_account.code, "
              + " mst_cash_account.name, "
              + " mst_cash_account.chartOfAccountCode, "
              + " mst_chart_of_account.name as chartOfAccountName, "
              + " mst_cash_account.bkmVoucherNo, "
              + " mst_cash_account.bkkVoucherNo, "
              + " mst_cash_account.activeStatus, "
              + " mst_cash_account.remark  "
              + "  FROM  "
              + "    mst_cash_account "
              + "    INNER JOIN mst_chart_of_account ON mst_cash_account.chartOfAccountCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_cash_account.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_cash_account.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_cash_account.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_cash_account." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + " mst_cash_account.code, "
              + " mst_cash_account.name, "
              + " mst_cash_account.chartOfAccountCode, "
              + " mst_chart_of_account.name as chartOfAccountName, "
              + " mst_cash_account.bkmVoucherNo, "
              + " mst_cash_account.bkkVoucherNo, "
              + " mst_cash_account.activeStatus, "
              + " mst_cash_account.remark  "
              + "  FROM  "
              + "    mst_cash_account "
              + "    INNER JOIN mst_chart_of_account ON mst_cash_account.chartOfAccountCode = mst_chart_of_account.code "
              + " WHERE "
              + "  mst_cash_account.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_cash_account.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_cash_account.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_cash_account." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
