/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author irfan
 */
@Repository
public class BillOfMaterialDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_bill_of_material WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT * FROM(  "
                    + " SELECT  "
                    + "     mst_bill_of_material.Code AS code , "
                    + "     mst_bill_of_material.Name AS name, "
                    + "     mst_bill_of_material.ItemCode AS itemCode, "
                    + "     mst_item.Name AS itemName, "
                    + "     mst_item.ItemSubCategoryCode AS itemSubCategoryCode, "
                    + "     mst_item_sub_category.Name AS itemSubCategoryName, "
                    + "     mst_item_sub_category.ItemCategoryCode AS itemCategoryCode, "
                    + "     mst_item_category.Name AS itemCategoryName, "
                    + "     mst_item_category.ItemDivisionCode AS itemDivisionCode, "
                    + "     mst_item_division.Name AS itemDivisionName, "
                    + "     mst_bill_of_material.Remark AS remark, "
                    + "     mst_bill_of_material.ActiveStatus AS activeStatus   "
                    + " FROM mst_bill_of_material  "
                    + "       INNER JOIN mst_item ON mst_bill_of_material.`ItemCode` = mst_item.`Code` "
                    + "       INNER JOIN mst_item_sub_category ON mst_item.`ItemSubCategoryCode` = mst_item_sub_category.`Code` "
                    + "       INNER JOIN mst_item_category ON mst_item_sub_category.`ItemCategoryCode` = mst_item_category.`Code` "
                    + "       INNER JOIN mst_item_division ON mst_item_category.`ItemDivisionCode` = mst_item_division.`Code`  "
                    + " ) AS bomDao "
                    + " WHERE "
                    + "bomDao.Code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "	mst_bill_of_material_detail.ItemCode AS itemCode, "
                    + "	mst_item.Name AS itemName, "
                    + "	mst_bill_of_material_detail.Quantity AS quantity, "
                    + "	mst_item.UnitOfMeasureCode AS unitOfMeasureCode ";
            String qry = " "
                    + "FROM mst_bill_of_material_detail "
                    + "INNER JOIN mst_item ON mst_bill_of_material_detail.ItemCode = mst_item.Code "
                    + "WHERE mst_bill_of_material_detail.HeaderCode='" + obj.get("headerCode") + "' "
                    + "ORDER BY mst_bill_of_material_detail." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    // FOR ASSEMBLY
    public PaginatedResults pagingAssembly(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "	bom.code, "
                    + "	bom.name ";
            String qry = " "
                    + "FROM mst_bill_of_material bom "
                    + "WHERE bom.ItemCode='" + obj.get("code") + "' "
                    + "ORDER BY bom."+ sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public PaginatedResults pagingBomDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "IFNULL(totalStok.ActualStock,0) AS totalStok, "
                    + "IFNULL(stockInWarehouse.ActualStock,0) AS actualStock, "
                    + "mst_bill_of_material_detail.code, "
                    + "mst_bill_of_material_detail.quantity, "
                    + "mst_bill_of_material_detail.remark, "
                    + "mst_bill_of_material_detail.itemCode, "
                    + "mst_item.name itemName, "
                    + "mst_item.unitOfMeasureCode ";
            String qry = " "
                    + "FROM mst_bill_of_material_detail "
                    + " INNER JOIN mst_item ON mst_item.code = mst_bill_of_material_detail.itemCode "
                    + " LEFT JOIN ( "
                        + " SELECT "
                            + " mst_item_jn_current_stock.Code, "
                            + " mst_item_jn_current_stock.WarehouseCode, "
                            + " mst_item_jn_current_stock.ItemCode, "
                            + " mst_item_jn_current_stock.RackCode, "
                            + " mst_item_jn_current_stock.ActualStock AS ActualStock "
                        + " FROM "
                            + " mst_item_jn_current_stock "
                        + " WHERE "
                            + " mst_item_jn_current_stock.WarehouseCode = '" + obj.get("warehouseCode") + "' "
                        + " AND mst_item_jn_current_stock.RackCode = '" + obj.get("rackCode") + "' "
                    + " ) AS stockInWarehouse ON stockInWarehouse.ItemCode = mst_bill_of_material_detail.ItemCode "
                    + " LEFT JOIN ( "
                        + " SELECT "
                                + " mst_item_jn_current_stock.Code, "
                                + " mst_item_jn_current_stock.WarehouseCode, "
                                + " mst_item_jn_current_stock.ItemCode, "
                                + " mst_item_jn_current_stock.RackCode, "
                                + " SUM(mst_item_jn_current_stock.ActualStock) AS ActualStock "
                        + " FROM "
                                + " mst_item_jn_current_stock "
                        + " WHERE "
                                + " mst_item_jn_current_stock.WarehouseCode = '" + obj.get("warehouseCode") + "' "
                                + " GROUP BY mst_item_jn_current_stock.WarehouseCode,mst_item_jn_current_stock.ItemCode "
                    + " ) AS totalStok ON totalStok.ItemCode = mst_bill_of_material_detail.ItemCode "
                    + "WHERE mst_bill_of_material_detail.HeaderCode='" + obj.get("headerCode") + "' "
                    + "ORDER BY mst_bill_of_material_detail." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    // FOR ASSEMBLY
}
