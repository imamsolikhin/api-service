/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class ItemClassificationDao extends BaseQuery{
    
     public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_item_classification WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_item_classification "
              + "  WHERE mst_item_classification.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_item_classification.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_item_classification.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item_classification.code, "
              + "    mst_item_classification.name, "
              + "    mst_item_classification.salesOrderStatus, "
              + "    mst_item_classification.assemblyStatus, "
              + "    mst_item_classification.activeStatus, "
              + "    mst_item_classification.remark "
              + " FROM mst_item_classification "
              + " WHERE "
              + "  mst_item_classification.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item_classification.code, "
              + "    mst_item_classification.name, "
              + "    mst_item_classification.salesOrderStatus, "
              + "    mst_item_classification.assemblyStatus, "
              + "    mst_item_classification.activeStatus, "
              + "    mst_item_classification.remark "
              + " FROM mst_item_classification "
              + " WHERE "
              + "  mst_item_classification.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_item_classification.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_item_classification.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_item_classification." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item_classification.code, "
              + "    mst_item_classification.name, "
              + "    mst_item_classification.salesOrderStatus, "
              + "    mst_item_classification.assemblyStatus, "
              + "    mst_item_classification.activeStatus, "
              + "    mst_item_classification.remark "
              + " FROM mst_item_classification "
              + " WHERE "
              + "  mst_item_classification.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_item_classification.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_item_classification.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_item_classification." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
    
}
