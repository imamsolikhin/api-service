/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class BranchDao extends BaseQuery{
    
    public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_branch WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_branch "
              + "   INNER JOIN mst_city ON mst_branch.CityCode = mst_city.code "
              + "   INNER JOIN mst_province ON mst_city.`ProvinceCode` = mst_province.`Code` "
              + "   INNER JOIN mst_island ON mst_province.`IslandCode` = mst_island.`Code` "
              + "   INNER JOIN mst_country ON mst_island.`CountryCode` = mst_country.`Code` "
              + "   LEFT JOIN mst_chart_of_account ON mst_branch.`FinanceForexGainLossChartOfAccountCode` = mst_chart_of_account.`Code` "
              + "   LEFT JOIN mst_journal ON mst_branch.`JournalCode` = mst_journal.`Code` "
              + "  WHERE mst_branch.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_branch.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_branch.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_branch.code, "
              + "    mst_branch.name, "
              + "    mst_branch.address, "
              + "    mst_branch.phone1, "
              + "    mst_branch.phone2, "
              + "    mst_branch.cityCode, "
              + "    IFNULL(mst_city.name,'') as cityName, "
              + "    mst_city.provinceCode, "
              + "    IFNULL(mst_province.name,'') as provinceName, "
              + "    mst_province.islandCode, "
              + "    IFNULL(mst_island.name,'') as islandName, "
              + "    mst_island.countryCode, "
              + "    IFNULL(mst_country.name,'') AS countryName,  "
              + "    mst_branch.`FinanceForexGainLossChartOfAccountCode`, "
              + "    IFNULL(mst_chart_of_account.`Name`,'') AS FinanceForexGainLossChartOfAccountName, "
              + "    mst_branch.`JournalCode`, "
              + "    IFNULL(mst_journal.`Name`,'') AS journalName, "
              + "    mst_branch.`InvoiceNote`, "
              + "    mst_branch.zipCode, "
              + "    mst_branch.emailAddress, "
              + "    mst_branch.contactPerson, "
              + "    mst_branch.activeStatus, "
              + "    mst_branch.remark "
              + " FROM mst_branch "
              + "   INNER JOIN mst_city ON mst_branch.CityCode = mst_city.code "
              + "   INNER JOIN mst_province ON mst_city.provinceCode = mst_province.code "
              + "   INNER JOIN mst_island ON mst_province.islandCode = mst_island.code "
              + "   INNER JOIN mst_country ON mst_island.countryCode = mst_country.code "
              + "   LEFT JOIN mst_chart_of_account ON mst_branch.`FinanceForexGainLossChartOfAccountCode` = mst_chart_of_account.`Code` "
              + "   LEFT JOIN mst_journal ON mst_branch.`JournalCode` = mst_journal.`Code` "
              + " WHERE "
              + "  mst_branch.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_branch.code, "
              + "    mst_branch.name, "
              + "    mst_branch.address, "
              + "    mst_branch.phone1, "
              + "    mst_branch.phone2, "
              + "    mst_branch.cityCode, "
              + "    IFNULL(mst_city.name,'') as cityName, "
              + "    mst_city.provinceCode, "
              + "    IFNULL(mst_province.name,'') as provinceName, "
              + "    mst_province.islandCode, "
              + "    IFNULL(mst_island.name,'') as islandName, "
              + "    mst_island.countryCode, "
              + "    IFNULL(mst_country.name,'') AS countryName,  "
              + "    mst_branch.`FinanceForexGainLossChartOfAccountCode`, "
              + "    IFNULL(mst_chart_of_account.`Name`,'') AS FinanceForexGainLossChartOfAccountName, "
              + "    mst_branch.`JournalCode`, "
              + "    IFNULL(mst_journal.`Name`,'') AS journalName, "
              + "    mst_branch.`InvoiceNote`, "
              + "    mst_branch.zipCode, "
              + "    mst_branch.emailAddress, "
              + "    mst_branch.contactPerson, "
              + "    mst_branch.activeStatus, "
              + "    mst_branch.remark "
              + " FROM mst_branch "
              + "   INNER JOIN mst_city ON mst_branch.CityCode = mst_city.code "
              + "   INNER JOIN mst_province ON mst_city.`ProvinceCode` = mst_province.`Code` "
              + "   INNER JOIN mst_island ON mst_province.`IslandCode` = mst_island.`Code` "
              + "   INNER JOIN mst_country ON mst_island.`CountryCode` = mst_country.`Code` "
              + "   LEFT JOIN mst_chart_of_account ON mst_branch.`FinanceForexGainLossChartOfAccountCode` = mst_chart_of_account.`Code` "
              + "   LEFT JOIN mst_journal ON mst_branch.`JournalCode` = mst_journal.`Code` "
              + " WHERE "
              + "  mst_branch.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_branch.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_branch.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_branch." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_branch.code, "
              + "    mst_branch.name, "
              + "    mst_branch.address, "
              + "    mst_branch.phone1, "
              + "    mst_branch.phone2, "
              + "    mst_branch.cityCode, "
              + "    IFNULL(mst_city.name,'') as cityName, "
              + "    mst_branch.`FinanceForexGainLossChartOfAccountCode`, "
              + "    IFNULL(mst_chart_of_account.`Name`,'') AS FinanceForexGainLossChartOfAccountName, "
              + "    mst_branch.`JournalCode`, "
              + "    IFNULL(mst_journal.`Name`,'') AS journalName, "
              + "    mst_island.`CountryCode`, "
              + "    IFNULL(mst_country.`Name`,'') AS countryName,  "
              + "    mst_branch.`InvoiceNote`, "
              + "    mst_branch.zipCode, "
              + "    mst_branch.emailAddress, "
              + "    mst_branch.contactPerson, "
              + "    mst_branch.activeStatus, "
              + "    mst_branch.remark "
              + " FROM mst_branch "
              + "   INNER JOIN mst_city ON mst_branch.CityCode = mst_city.code "
              + "   INNER JOIN mst_province ON mst_city.`ProvinceCode` = mst_province.`Code` "
              + "   INNER JOIN mst_island ON mst_province.`IslandCode` = mst_island.`Code` "
              + "   INNER JOIN mst_country ON mst_island.`CountryCode` = mst_country.`Code` "
              + "   LEFT JOIN mst_chart_of_account ON mst_branch.`FinanceForexGainLossChartOfAccountCode` = mst_chart_of_account.`Code` "
              + "   LEFT JOIN mst_journal ON mst_branch.`JournalCode` = mst_journal.`Code` "
              + " WHERE "
              + "  mst_branch.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_branch.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_branch.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_branch." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
    
}
