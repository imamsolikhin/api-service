/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class PurchaseDestinationDao extends BaseQuery{
     public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_purchase_destination WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_purchase_destination "
              + "  	INNER JOIN mst_city ON mst_purchase_destination.CityCode = mst_city.`Code` "
              + "  	INNER JOIN mst_country ON mst_purchase_destination.CountryCode = mst_country.`Code` "
              + "  WHERE mst_purchase_destination.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_purchase_destination.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_purchase_destination.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_purchase_destination.code, "
              + "  	 mst_purchase_destination.name, "
              + "  	 mst_purchase_destination.address, "
              + "  	 mst_purchase_destination.shipToStatus, "
              + "  	 mst_purchase_destination.billToStatus, "
              + "  	 mst_city.code as cityCode, "
              + "  	 mst_city.name as cityName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_purchase_destination.phone1, "
              + "  	 mst_purchase_destination.phone2, "
              + "  	 mst_purchase_destination.fax, "
              + "  	 mst_purchase_destination.emailAddress, "
              + "  	 mst_purchase_destination.zipCode, "
              + "  	 mst_purchase_destination.contactPerson, "
              + "  	 mst_purchase_destination.remark, "
              + "  	 mst_purchase_destination.activeStatus "
              + "  FROM mst_purchase_destination "
              + "  	INNER JOIN mst_city ON mst_purchase_destination.CityCode = mst_city.`Code` "
              + "  	INNER JOIN mst_country ON mst_purchase_destination.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_purchase_destination.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_purchase_destination.code, "
              + "  	 mst_purchase_destination.name, "
              + "  	 mst_purchase_destination.address, "
              + "  	 mst_city.code as cityCode, "
              + "  	 mst_city.name as cityName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_purchase_destination.phone1, "
              + "  	 mst_purchase_destination.phone2, "
              + "  	 mst_purchase_destination.fax, "
              + "  	 mst_purchase_destination.emailAddress, "
              + "  	 mst_purchase_destination.zipCode, "
              + "  	 mst_purchase_destination.contactPerson, "
              + "  	 mst_purchase_destination.remark, "
              + "  	 mst_purchase_destination.activeStatus "
              + "  FROM mst_purchase_destination "
              + "  	INNER JOIN mst_city ON mst_purchase_destination.CityCode = mst_city.`Code` "
              + "  	INNER JOIN mst_country ON mst_purchase_destination.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_purchase_destination.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_purchase_destination.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_purchase_destination.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_purchase_destination." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public Object dataBillTo(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_purchase_destination.code, "
              + "  	 mst_purchase_destination.name, "
              + "  	 mst_purchase_destination.address, "
              + "  	 mst_city.code as cityCode, "
              + "  	 mst_city.name as cityName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_purchase_destination.phone1, "
              + "  	 mst_purchase_destination.phone2, "
              + "  	 mst_purchase_destination.fax, "
              + "  	 mst_purchase_destination.emailAddress, "
              + "  	 mst_purchase_destination.zipCode, "
              + "  	 mst_purchase_destination.contactPerson, "
              + "  	 mst_purchase_destination.remark, "
              + "  	 mst_purchase_destination.activeStatus "
              + "  FROM mst_purchase_destination "
              + "  	INNER JOIN mst_city ON mst_purchase_destination.CityCode = mst_city.`Code` "
              + "  	INNER JOIN mst_country ON mst_purchase_destination.CountryCode = mst_country.`Code` "
              + "  	INNER JOIN mst_branch ON mst_purchase_destination.Code = mst_branch.DefaultPurchaseBillToCode "
              + " WHERE "
              + "  mst_branch.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public Object dataShipTo(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT "
              + "  	 mst_purchase_destination.code, "
              + "  	 mst_purchase_destination.name, "
              + "  	 mst_purchase_destination.address, "
              + "  	 mst_city.code as cityCode, "
              + "  	 mst_city.name as cityName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_purchase_destination.phone1, "
              + "  	 mst_purchase_destination.phone2, "
              + "  	 mst_purchase_destination.fax, "
              + "  	 mst_purchase_destination.emailAddress, "
              + "  	 mst_purchase_destination.zipCode, "
              + "  	 mst_purchase_destination.contactPerson, "
              + "  	 mst_purchase_destination.remark, "
              + "  	 mst_purchase_destination.activeStatus "
              + "  FROM mst_purchase_destination "
              + "  	INNER JOIN mst_city ON mst_purchase_destination.CityCode = mst_city.`Code` "
              + "  	INNER JOIN mst_country ON mst_purchase_destination.CountryCode = mst_country.`Code` "
              + "  	INNER JOIN mst_branch ON mst_purchase_destination.Code = mst_branch.DefaultPurchaseShipToCode "
              + " WHERE "
              + "  mst_branch.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " " 
              + "  SELECT "
              + "  	 mst_purchase_destination.code, "
              + "  	 mst_purchase_destination.name, "
              + "  	 mst_purchase_destination.address, "
              + "  	 mst_city.code as cityCode, "
              + "  	 mst_city.name as cityName, "
              + "  	 mst_country.code as countryCode, "
              + "  	 mst_country.name as countryName, "
              + "  	 mst_purchase_destination.phone1, "
              + "  	 mst_purchase_destination.phone2, "
              + "  	 mst_purchase_destination.fax, "
              + "  	 mst_purchase_destination.emailAddress, "
              + "  	 mst_purchase_destination.zipCode, "
              + "  	 mst_purchase_destination.contactPerson, "
              + "  	 mst_purchase_destination.remark, "
              + "  	 mst_purchase_destination.activeStatus "
              + "  FROM mst_purchase_destination "
              + "  	INNER JOIN mst_city ON mst_purchase_destination.CityCode = mst_city.`Code` "
              + "  	INNER JOIN mst_country ON mst_purchase_destination.CountryCode = mst_country.`Code` "
              + " WHERE "
              + "  mst_purchase_destination.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_purchase_destination.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_purchase_destination.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_purchase_destination." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
