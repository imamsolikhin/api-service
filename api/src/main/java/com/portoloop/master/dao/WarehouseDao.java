/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author irfaan
 */
@Repository
public class WarehouseDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_warehouse WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "	mst_warehouse.Code AS code,  "
                    + "	mst_warehouse.Name AS name,  "
                    + "	mst_warehouse.Address AS address,  "
                    + "	mst_warehouse.CityCode AS cityCode,  "
                    + "	mst_city.Name AS cityName,  "
                    + "	mst_country.Code AS countryCode,  "
                    + "	mst_country.Name AS countryName,  "
                    + "	mst_warehouse.ZipCode AS zipCode,  "
                    + "	mst_warehouse.Phone1 AS phone1,  "
                    + "	mst_warehouse.Phone2 AS phone2, "
                    + "	mst_warehouse.ContactPerson AS contactPerson, "
                    + "	mst_warehouse.EmailAddress AS emailAddress, "
                    + "	mst_warehouse.DOCK_IN_Code AS dock_IN_Code, "
                    + "	mst_warehouse.DOCK_DLN_Code AS dock_DLN_Code, "
                    + "	mst_warehouse.ActiveStatus AS activeStatus, "
                    + "	mst_warehouse.Remark AS remark "
                    + "FROM mst_warehouse "
                    + "INNER JOIN mst_city ON mst_warehouse.CityCode = mst_city.Code "
                    + "INNER JOIN mst_province ON mst_city.ProvinceCode = mst_province.Code "
                    + "INNER JOIN mst_island ON mst_province.IslandCode = mst_island.Code "
                    + "INNER JOIN mst_country ON mst_island.CountryCode = mst_country.Code "
                    + "WHERE mst_warehouse.Code='" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public Object dataForDLN(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                + "SELECT "
                + ""
                    + "mst_warehouse.branchCode, "
                    + "mst_warehouse.code, "
                    + "mst_warehouse.name, "
                    + "mst_warehouse.remark "
                + "FROM "
                    + "mst_warehouse "
                + "INNER JOIN ( "
                    + "SELECT "
                        + "ivt_picking_list_sales_order.Code, "
                        + "ivt_picking_list_sales_order.SalesOrderCode, "
                        + "ivt_picking_list_sales_order.WarehouseCode "
                    + "FROM "
                        + "ivt_picking_list_sales_order "
                + ") AS pickingListSo ON pickingListSo.WarehouseCode = mst_warehouse.Code AND pickingListSo.SalesOrderCode = '"+ obj.get("salesOrderCode") +"' "
                + "WHERE mst_warehouse.code = '"+ obj.get("code") +"' "
                    + "GROUP BY mst_warehouse.Code ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public PaginatedResults pagingForDLN(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + ""
                    + "mst_warehouse.branchCode, "
                    + "mst_warehouse.code, "
                    + "mst_warehouse.name, "
                    + "mst_warehouse.remark ";
                 String qry = " "
                + "FROM "
                    + "mst_warehouse "
                + "INNER JOIN ( "
                    + "SELECT "
                        + "ivt_picking_list_sales_order.Code, "
                        + "ivt_picking_list_sales_order.SalesOrderCode, "
                        + "ivt_picking_list_sales_order.WarehouseCode "
                    + "FROM "
                        + "ivt_picking_list_sales_order "
                + ") AS pickingListSo ON pickingListSo.WarehouseCode = mst_warehouse.Code AND pickingListSo.SalesOrderCode = '" + obj.get("salesOrderCode") +"' "    
                + "WHERE mst_warehouse.code LIKE '%"+obj.get("code") + "%' "
                + "AND mst_warehouse.Name LIKE '%"+obj.get("name") + "%' "
                + "GROUP BY mst_warehouse.Code "
                    + "ORDER BY mst_warehouse." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "	mst_warehouse.Code AS code, "
                    + "	mst_warehouse.Name AS name, "
                    + "	mst_warehouse.Address AS address, "
                    + "	mst_warehouse.CityCode AS cityCode, "
                    + "	mst_city.Name AS cityName, "
                    + "	mst_country.Code AS countryCode, "
                    + "	mst_country.Name AS countryName, "
                    + "	mst_warehouse.ZipCode AS zipCode, "
                    + "	mst_warehouse.Phone1 AS phone1, "
                    + "	mst_warehouse.Phone2 AS phone2, "
                    + "	mst_warehouse.EmailAddress AS emailAddress ";
            String qry = " "
                    + "FROM mst_warehouse "
                    + "INNER JOIN mst_city ON mst_warehouse.CityCode = mst_city.Code "
                    + "INNER JOIN mst_province ON mst_city.ProvinceCode = mst_province.Code "
                    + "INNER JOIN mst_island ON mst_province.IslandCode = mst_island.Code "
                    + "INNER JOIN mst_country ON mst_island.CountryCode = mst_country.Code "
                    + "WHERE mst_warehouse.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_warehouse.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_warehouse.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_warehouse." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "	mst_warehouse.Code AS code, "
                    + "	mst_warehouse.Name AS name, "
                    + "	mst_warehouse.Address AS address, "
                    + "	mst_warehouse.CityCode AS cityCode, "
                    + "	mst_city.Name AS cityName, "
                    + "	mst_country.Code AS countryCode, "
                    + "	mst_country.Name AS countryName, "
                    + "	mst_warehouse.ZipCode AS zipCode, "
                    + "	mst_warehouse.Phone1 AS phone1, "
                    + "	mst_warehouse.Phone2 AS phone2, "
                    + "	mst_warehouse.activeStatus, "
                    + "	mst_warehouse.EmailAddress AS emailAddress ";
            String qry = " "
                    + "FROM mst_warehouse "
                    + "INNER JOIN mst_city ON mst_warehouse.CityCode = mst_city.Code "
                    + "INNER JOIN mst_province ON mst_city.ProvinceCode = mst_province.Code "
                    + "INNER JOIN mst_island ON mst_province.IslandCode = mst_island.Code "
                    + "INNER JOIN mst_country ON mst_island.CountryCode = mst_country.Code "
                    + "WHERE mst_warehouse.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_warehouse.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_warehouse.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_warehouse." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countlookup(JSONObject obj, Class<?> module) {
        try {
            String qry = "SELECT COUNT(mst_warehouse.Code) "
                    + "FROM mst_warehouse "
                    + "INNER JOIN mst_rack rackDockIn ON mst_warehouse.DOCK_IN_Code=rackDockIn.Code "
                    + "INNER JOIN mst_rack rackDockDln ON mst_warehouse.DOCK_DLN_Code=rackDockDln.Code "
                    + "WHERE mst_warehouse.Code LIKE '%" + obj.get("code") + "%' "
                    + "     AND mst_warehouse.Name LIKE '%" + obj.get("name") + "%' "
                    + "     AND( "
                    + "		CASE "
                    + "		WHEN 'true'='" + obj.get("activeStatus") + "' THEN "
                    + "			mst_warehouse.ActiveStatus=1 "
                    + "		WHEN 'false'='" + obj.get("activeStatus") + "' THEN "
                    + "			mst_warehouse.ActiveStatus=0 "
                    + "		ELSE 1=1 "
                    + "		END  "
                    + "     )";
            long countlookupData = countResult(qry);
            return countlookupData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults lookup(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry
                    = "SELECT "
                    + "     mst_warehouse.Code code, "
                    + "     mst_warehouse.Name name, "
                    + "     mst_warehouse.BranchCode branchCode, "
                    + "     mst_warehouse.Address address, "
                    + "     mst_warehouse.Phone1 phone1, "
                    + "     mst_warehouse.Phone2 phone2, "
                    + "     mst_warehouse.EmailAddress emailAddress, "
                    + "     mst_warehouse.ZipCode zipCode, "
                    + "     mst_warehouse.ContactPerson contactPerson, "
                    + "     mst_warehouse.CityCode cityCode, "
                    + "     mst_warehouse.DOCK_IN_Code rackDockInCode, "
                    + "     rackDockIn.Name rackDockInName, "
                    + "     mst_warehouse.DOCK_DLN_Code rackDockDlnCode, "
                    + "     rackDockDln.Name rackDockDlnName "
                    + "FROM mst_warehouse "
                    + "INNER JOIN mst_rack rackDockIn ON mst_warehouse.DOCK_IN_Code=rackDockIn.Code "
                    + "INNER JOIN mst_rack rackDockDln ON mst_warehouse.DOCK_DLN_Code=rackDockDln.Code "
                    + "WHERE mst_warehouse.Code LIKE '%" + obj.get("code") + "%' "
                    + "     AND mst_warehouse.Name LIKE '%" + obj.get("name") + "%' "
                    + "     AND( "
                    + "		CASE "
                    + "		WHEN 'true'='" + obj.get("activeStatus") + "' THEN "
                    + "			mst_warehouse.ActiveStatus=1 "
                    + "		WHEN 'false'='" + obj.get("activeStatus") + "' THEN "
                    + "			mst_warehouse.ActiveStatus=0 "
                    + "		ELSE 1=1 "
                    + "		END  "
                    + "     ) "
                    + "ORDER BY mst_warehouse." + sort[0] + " " + sort[1];
            long countlookupData = this.countlookup(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countlookupData, totalPage(countlookupData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
