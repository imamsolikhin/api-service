/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class ItemSubCategoryDao extends BaseQuery{
    
    public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_item_sub_category WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_item_sub_category "
              + " INNER JOIN mst_item_category ON mst_item_sub_category.`ItemCategoryCode` = mst_item_category.`Code` "
              + " INNER JOIN mst_item_division ON mst_item_division.Code = mst_item_category.ItemDivisionCode "
              + "  WHERE mst_item_sub_category.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_item_sub_category.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_item_sub_category.activeStatus", obj.get("activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item_sub_category.code, "
              + "    mst_item_sub_category.name, "
              + "    mst_item_sub_category.itemCategoryCode, "
              + "    mst_item_category.`Name` AS itemCategoryName, "
              + "    mst_item_sub_category.activeStatus, "
              + "    mst_item_sub_category.remark "
              + " FROM mst_item_sub_category "
              + " INNER JOIN mst_item_category ON mst_item_sub_category.`ItemCategoryCode` = mst_item_category.`Code` "
              + " WHERE "
              + "  mst_item_sub_category.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item_sub_category.code, "
              + "    mst_item_sub_category.name, "
              + "    mst_item_sub_category.itemCategoryCode, "
              + "    mst_item_division.`Name` AS itemCategoryName, "
              + "    mst_item_sub_category.activeStatus, "
              + "    mst_item_sub_category.remark "
              + " FROM mst_item_sub_category "
              + " INNER JOIN mst_item_category ON mst_item_sub_category.`ItemCategoryCode` = mst_item_category.`Code` "
              + " WHERE "
              + "  mst_item_sub_category.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_item_sub_category.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_item_sub_category.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_item_sub_category." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item_sub_category.code, "
              + "    mst_item_sub_category.name, "
              + "    mst_item_sub_category.itemCategoryCode, "
              + "    mst_item_category.`Name` AS itemCategoryName, "
              + "    mst_item_category.itemDivisionCode, "
              + "    mst_item_division.`Name` AS itemDivisionName, "
              + "    mst_item_sub_category.activeStatus, "
              + "    mst_item_sub_category.remark "
              + " FROM mst_item_sub_category "
              + " INNER JOIN mst_item_category ON mst_item_sub_category.`ItemCategoryCode` = mst_item_category.`Code` "
              + " INNER JOIN mst_item_division ON mst_item_division.Code = mst_item_category.ItemDivisionCode "
              + " WHERE "
              + "  mst_item_sub_category.code LIKE '%" + obj.get("code") + "%' "
              + "  AND mst_item_sub_category.name LIKE '%" + obj.get("name") + "%' "
              + "  " + getStatus("mst_item_sub_category.activeStatus", obj.get("activeStatus").toString()) + " "
              + " ORDER BY mst_item_sub_category." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
    
}
