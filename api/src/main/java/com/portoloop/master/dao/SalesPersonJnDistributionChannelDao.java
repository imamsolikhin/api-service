/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class SalesPersonJnDistributionChannelDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_sales_person_jn_distribution_channel WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "	mst_sales_person_jn_distribution_channel.DistributionChannelCode AS distributionCode, "
                    + "  mst_distribution_channel.Name AS distributionName ";
            String qry = " "
                    + "FROM mst_sales_person_jn_distribution_channel "
                    + "INNER JOIN mst_distribution_channel "
                    + "    ON mst_sales_person_jn_distribution_channel.DistributionChannelCode = mst_distribution_channel.Code "
                    + "WHERE mst_sales_person_jn_distribution_channel.SalesPersonCode='" + obj.get("salesPersonCode") + "' "
                    + "ORDER BY mst_sales_person_jn_distribution_channel." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
