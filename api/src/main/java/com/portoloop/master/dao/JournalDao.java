/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class JournalDao extends BaseQuery {

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT  "
                    + "  mst_journal_chart_of_account.currencyCode, "
                    + "  mst_journal_chart_of_account.journalPostingType, "
                    + "  mst_journal_type.journalPosition, "
                    + "  mst_journal_chart_of_account.accountCode, "
                    + "  mst_chart_of_account.name AS accountName ";
            String qry = " "
                    + "FROM "
                    + "  mst_journal_chart_of_account "
                    + "  INNER JOIN mst_chart_of_account "
                    + "    ON mst_journal_chart_of_account.AccountCode = mst_chart_of_account.Code "
                    + "  INNER JOIN mst_journal_type "
                    + "    ON mst_journal_chart_of_account.JournalPostingSetupCode = mst_journal_type.code "
                    + "WHERE mst_journal_chart_of_account.JournalCode='"+ obj.get("headerCode")+"' "
                    + "ORDER BY mst_journal_chart_of_account." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
