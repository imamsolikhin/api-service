/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.portoloop.master.dao;

import com.portoloop.core.base.BaseQuery;
import com.portoloop.core.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class CustomerDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM mst_customer WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  COUNT(mst_customer.code) "
                    + "  FROM mst_customer "
                    + "  INNER JOIN mst_price_type ON mst_price_type.code = mst_customer.priceTypeCode "
                    + "  WHERE mst_customer.Code LIKE '%" + obj.get("code") + "%' "
                    + "  AND mst_customer.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_customer.activeStatus", obj.get("activeStatus").toString()) + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT mst_customer.code,"
                    + " mst_customer.name ,"
                    + " mst_customer.priceTypeCode,"
                    + " mst_price_type.Name AS priceTypeName, "
                    + " mst_customer.creditLimit "
                    + " FROM mst_customer"
                    + " INNER JOIN mst_price_type ON mst_price_type.code = mst_customer.priceTypeCode "
                    + " WHERE mst_customer.Code LIKE '%" + obj.get("code") + "%' "
                    + " AND mst_customer.Name LIKE '%" + obj.get("name") + "%' "
                    + " ORDER BY " + getTableName(module) + "." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // api for sales order
    public PaginatedResults lookUpForSo(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(customer.code) FROM( SELECT";
            String list = "SELECT customer.* FROM( SELECT ";
            String select = " "
                    + "         cus.code, "
                    + "		cus.name, "
                    + "		cus.activeStatus, "
                    + "		cus.priceTypeCode, "
                    + "		cus.Name AS priceTypeName, "
                    + "		IFNULL(cus.creditLimit,0) creditLimit ,"
                    + "		IFNULL(inv.grandTotal,0) AS outstandingAmountCustomer ";
            String qry = " "
                    + "	FROM mst_customer cus "
                    + " LEFT JOIN ( "
                    + "	SELECT "
                    + "		inv.CustomerCode, "
                    + "		SUM(inv.GrandTotalAmount) AS grandTotal "
                    + "	FROM fin_customer_invoice_sales_order inv "
                    + "	GROUP BY inv.CustomerCode "
                    + ") AS inv ON inv.CustomerCode = cus.Code "
                    + ")customer "
                    + " WHERE customer.Code LIKE '%" + obj.get("code") + "%' "
                    + " AND customer.Name LIKE '%" + obj.get("name") + "%' "
                    + " AND customer.activeStatus = " + obj.get("activeStatus") + " "
                    + "	AND (CASE WHEN TRUE = " + obj.get("activeStatus") + " THEN  customer.activeStatus = TRUE "
                    + " WHEN FALSE = " + obj.get("activeStatus") + " THEN customer.activeStatus = FALSE "
                    + "	ELSE  1=1 END) "
                    + " ORDER BY customer." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object dataForSoLookUp(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  cus.code, "
                    + "  cus.name, "
                    + "	 cus.priceTypeCode, "
                    + "	 cus.Name priceTypeName, "
                    + "	 IFNULL(cus.creditLimit,0) creditLimit"
                    + "	 IFNULL(inv.grandTotal,0) outstandingAmountCustomer "
                    + "FROM mst_customer cus "
                    + " LEFT JOIN ( "
                    + "	SELECT "
                    + "		inv.CustomerCode, "
                    + "		SUM(inv.GrandTotalAmount) AS grandTotal "
                    + "	FROM fin_customer_invoice_sales_order inv "
                    + "	GROUP BY inv.CustomerCode "
                    + ") AS inv ON inv.CustomerCode = cus.Code "
                    + " WHERE cus.code = '" + obj.get("code") + "' "
                    + " AND customer.activeStatus = true ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    // api for sales order

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "mst_customer.Code AS code, "
                    + "  mst_customer.Name AS name, "
                    + "  mst_customer.Address AS address, "
                    + "  mst_customer.DefaultContactPersonCode AS defaultContactPersonCode, "
                    + "  mst_customer.CityCode AS cityCode, "
                    + "  mst_city.Name AS cityName, "
                    + "  mst_country.Code AS countryCode, "
                    + "  mst_country.Name AS countryName, "
                    + "  mst_customer.customerCategoryCode, "
                    + "  mst_customer_category.Name AS customerCategoryName, "
                    + "  mst_customer.ZipCode AS zipCode, "
                    + "  mst_customer.Phone1 AS phone1, "
                    + "  mst_customer.Phone2 AS phone2, "
                    + "  mst_customer.EmailAddress AS emailAddress ";
            String qry = " "
                    + "FROM "
                    + "  mst_customer "
                    + "  INNER JOIN mst_customer_category ON mst_customer.CustomerCategoryCode = mst_customer_category.Code "
                    + "  INNER JOIN mst_city "
                    + "    ON mst_customer.CityCode = mst_city.Code "
                    + "  INNER JOIN mst_province "
                    + "    ON mst_city.`ProvinceCode` = mst_province.`Code` "
                    + "  INNER JOIN mst_island "
                    + "    ON mst_province.`IslandCode` = mst_island.`Code` "
                    + "  INNER JOIN mst_country "
                    + "    ON mst_island.`CountryCode` = mst_country.`Code` "
                    + "WHERE mst_customer.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_customer.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_customer.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_customer." + sort[0] + " " + sort[1];
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "  mst_customer.Code AS code, "
                    + "  mst_customer.Name AS name, "
                    + "  mst_customer.Address AS address, "
                    + "  mst_customer.BusinessEntityCode AS businessEntityCode, "
                    + "  mst_business_entity.Name AS businessEntityName, "
                    + "  mst_customer.CityCode AS cityCode, "
                    + "  mst_city.Name AS cityName, "
                    + "  mst_city.provinceCode, "
                    + "  IFNULL(mst_province.name, '') AS provinceName, "
                    + "  mst_province.islandCode, "
                    + "  IFNULL(mst_island.name, '') AS islandName, "
                    + "  mst_country.Code AS countryCode, "
                    + "  mst_country.Name AS countryName, "
                    + "  mst_customer.ZipCode AS zipCode, "
                    + "  mst_customer.Phone1 AS phone1, "
                    + "  mst_customer.Phone2 AS phone2, "
                    + "  mst_customer.EmailAddress AS emailAddress, "
                    + "  mst_customer.DefaultContactPersonCode AS defaultContactPersonCode, "
                    + "  mst_customer_jn_contact.Name AS defaultContactPersonName, "
                    + "  mst_customer.PaymentTermCode AS paymentTermCode, "
                    + "  mst_payment_term.Name AS paymentTermName, "
                    + "  mst_customer.DistributionChannelCode AS distributionChannelCode, "
                    + "  mst_distribution_channel.Name AS distributionChannelName, "
                    + "  mst_customer.ARAccountCode AS arAccountCode, "
                    + "  mst_chart_of_account.Name AS arAccountName, "
                    + "  mst_customer.CreditLimit AS creditLimit, "
                    + "  mst_customer.CustomerCategoryCode AS customerCategoryCode, "
                    + "  mst_customer_category.Name AS customerCategoryName, "
                    + "  mst_customer.PriceTypeCode AS priceTypeCode, "
                    + "  mst_price_type.Name AS priceTypeName, "
                    + "  mst_customer.DefaultDiscountTypeCode AS defaultDiscountTypeCode, "
                    + "  mst_discount_type.Name AS defaultDiscountTypeName, "
                    + "  mst_customer.DefaultBillToCode AS defaultBillToCode, "
                    + "  billTo.Name AS defaultBillToName, "
                    + "  mst_customer.DefaultShipToCode AS defaultShipToCode, "
                    + "  shipTo.Name AS defaultShipToName, "
                    + "  mst_customer.WAPUStatus AS wapuStatus, "
                    + "  mst_customer.ActiveStatus AS activeStatus, "
                    + "  mst_customer.Remark AS remark "
                    + "FROM "
                    + "  mst_customer "
                    + "  LEFT JOIN mst_customer_jn_address shipTo "
                    + "    ON mst_customer.DefaultBillToCode = shipTo.code "
                    + "  LEFT JOIN mst_customer_jn_address billTo "
                    + "    ON mst_customer.DefaultBillToCode = billTo.code "
                    + "  LEFT  JOIN mst_discount_type "
                    + "    ON mst_customer.DefaultDiscountTypeCode = mst_discount_type.Code "
                    + "  INNER JOIN mst_price_type "
                    + "    ON mst_customer.PriceTypeCode = mst_price_type.Code "
                    + "  INNER JOIN mst_customer_category "
                    + "    ON mst_customer.CustomerCategoryCode = mst_customer_category.Code "
                    + "  INNER JOIN mst_chart_of_account "
                    + "    ON mst_customer.ARAccountCode = mst_chart_of_account.Code "
                    + "  INNER JOIN mst_distribution_channel "
                    + "    ON mst_customer.DistributionChannelCode = mst_distribution_channel.Code "
                    + "  INNER JOIN mst_payment_term "
                    + "    ON mst_customer.PaymentTermCode = mst_payment_term.Code "
                    + "  LEFT JOIN mst_customer_jn_contact "
                    + "    ON mst_customer.DefaultContactPersonCode = mst_customer_jn_contact.Code "
                    + "  INNER JOIN mst_business_entity "
                    + "    ON mst_customer.BusinessEntityCode = mst_business_entity.Code "
                    + "  INNER JOIN mst_city "
                    + "    ON mst_customer.CityCode = mst_city.Code "
                    + "  INNER JOIN mst_province "
                    + "    ON mst_city.`ProvinceCode` = mst_province.`Code` "
                    + "  INNER JOIN mst_island "
                    + "    ON mst_province.`IslandCode` = mst_island.`Code` "
                    + "  INNER JOIN mst_country "
                    + "    ON mst_island.`CountryCode` = mst_country.`Code` "
                    + " WHERE "
                    + "  mst_customer.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT "
                    + "mst_customer.Code AS code, "
                    + "  mst_customer.Name AS name, "
                    + "  mst_customer.Address AS address, "
                    + "  mst_customer.CityCode AS cityCode, "
                    + "  mst_city.Name AS cityName, "
                    + "  mst_country.Code AS countryCode, "
                    + "  mst_country.Name AS countryName, "
                    + "  mst_customer.ZipCode AS zipCode, "
                    + "  mst_customer.Phone1 AS phone1, "
                    + "  mst_customer.Phone2 AS phone2, "
                    + "  mst_customer.EmailAddress AS emailAddress ";
            String qry = " "
                    + "FROM "
                    + "  mst_customer "
                    + "  INNER JOIN mst_city "
                    + "    ON mst_customer.CityCode = mst_city.Code "
                    + "  INNER JOIN mst_province "
                    + "    ON mst_city.`ProvinceCode` = mst_province.`Code` "
                    + "  INNER JOIN mst_island "
                    + "    ON mst_province.`IslandCode` = mst_island.`Code` "
                    + "  INNER JOIN mst_country "
                    + "    ON mst_island.`CountryCode` = mst_country.`Code` "
                    + "WHERE mst_customer.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND mst_customer.Name LIKE '%" + obj.get("name") + "%' "
                    + "  " + getStatus("mst_customer.activeStatus", obj.get("activeStatus").toString()) + " "
                    + "ORDER BY mst_customer." + sort[0] + " " + sort[1];
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
