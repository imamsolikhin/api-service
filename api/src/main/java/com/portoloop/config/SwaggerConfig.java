package com.portoloop.core.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {

    registry
            .addResourceHandler("swagger-ui.html")
            .addResourceLocations("classpath:/META-INF/resources/");

    registry
            .addResourceHandler("/webjars/**")
            .addResourceLocations("classpath:/META-INF/resources/webjars/");
  }

  @Bean
  public Docket oauth() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("DEFAULT")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.portoloop.oauth.controller"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .useDefaultResponseMessages(false)
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }
  
  @Bean
  public Docket master() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("MASTER")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.portoloop.master.controller"))
            //            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .useDefaultResponseMessages(false)
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }
  
  @Bean
  public Docket purchase() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("PURCHASE")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.portoloop.purchase.controller"))
            //            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .useDefaultResponseMessages(false)
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }
  
  @Bean
  public Docket sales() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("SALES")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.portoloop.sales.controller"))
            //            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .useDefaultResponseMessages(false)
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }
  
  @Bean
  public Docket inventory() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("INVENTORY")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.portoloop.inventory.controller"))
            //            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .useDefaultResponseMessages(false)
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }
  
  @Bean
  public Docket finance() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("FINANCE")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.portoloop.finance.controller"))
            //            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .useDefaultResponseMessages(false)
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()));
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
            .title("BACKOFFICE")
            .description("JWT Authentication Service. You can find out more about JWT at [https://jwt.io/](https://jwt.io/). For this sample, you can use user `user001` and pass `password` to test the authorization filters. Once you have successfully logged in and obtained the token, you should click on the right top button `Authorize` and introduce it with the prefix \"Bearer \".")
            .version("1.0.0")
            .license("MIT License").licenseUrl("http://opensource.org/licenses/MIT")
            .contact(new Contact(null, null, "portoloop@gmail.com"))
            .build();
  }

  private ApiKey apiKey() {
    return new ApiKey("Authorization", "Authorization", "header");
  }

  private SecurityContext securityContext() {
    return SecurityContext.builder().securityReferences(defaultAuth()).build();
  }

  private List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Arrays.asList(new SecurityReference("Authorization", authorizationScopes));
  }
}
