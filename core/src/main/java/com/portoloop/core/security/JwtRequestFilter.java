package com.portoloop.core.security;

import com.portoloop.core.model.UserDetails;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.portoloop.core.enums.CustomException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.ArrayList;
import org.springframework.http.HttpStatus;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

  @Autowired
  private JwtSessionDataPoint jwtSessionDataPoint;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
          throws ServletException, IOException {

    final String requestTokenHeader = request.getHeader("Authorization");

    String username = null;
    String jwtToken = null;

    if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
      jwtToken = requestTokenHeader.substring(7);
      try {
        username = jwtTokenUtil.getUsernameFromToken(jwtToken);
      } catch (SignatureException e) {
        throw new CustomException("Invalid JWT signature", HttpStatus.FORBIDDEN);
      } catch (MalformedJwtException e) {
        throw new CustomException("Invalid JWT token", HttpStatus.FORBIDDEN);
      } catch (ExpiredJwtException e) {
        throw new CustomException("Expired JWT token", HttpStatus.FORBIDDEN);
      } catch (UnsupportedJwtException e) {
        throw new CustomException("Unsupported JWT token", HttpStatus.FORBIDDEN);
      } catch (IllegalArgumentException e) {
        throw new CustomException("JWT claims string is empty.", HttpStatus.FORBIDDEN);
      }
    }

    // Once we get the token validate it.
    if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

      UserDetails userDetails = new UserDetails(username, "", new ArrayList<>());
      userDetails.setSetup(jwtSessionDataPoint.data(username, "AGN"));

      if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
        usernamePasswordAuthenticationToken
                .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        usernamePasswordAuthenticationToken.setDetails(userDetails);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
      }
    }
    chain.doFilter(request, response);
  }

}
