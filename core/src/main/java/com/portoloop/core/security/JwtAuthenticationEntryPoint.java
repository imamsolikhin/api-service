package com.portoloop.core.security;

import com.portoloop.core.enums.CustomException;
import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

  private static final long serialVersionUID = -7858869558953243875L;

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response,
          AuthenticationException authException) throws IOException {

    System.out.println(response.getStatus());
//    System.out.println(response.);
    System.out.println(authException.getMessage());
    final String expired = (String) request.getAttribute("expired");
    System.out.println(expired);
    
    if (expired.contains("expired")) {
      throw new CustomException("Session expired", HttpStatus.FORBIDDEN);
//      response.sendError(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION, "Session is expired");
    } else {
      throw new CustomException("you don't have authority", HttpStatus.FORBIDDEN);
//      response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }
  }
}
