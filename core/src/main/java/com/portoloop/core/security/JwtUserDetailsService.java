package com.portoloop.core.security;

import com.portoloop.core.enums.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    throw new CustomException("User not found with username: " + username, HttpStatus.FORBIDDEN);
//    throw new UsernameNotFoundException("User not found with username: " + username);
  }
}
