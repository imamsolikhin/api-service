package com.portoloop.core.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Setup {

  private String code = "";
  private String companyName = "";
  private String companyAcronym = "";
  private String logoPath = "";
  private String webTitle = "";
  private String currencyCode = "";
  private String currencyName = "";
  private String defaultPriceTypeCode = "";
  private String defaultPriceTypeName = "";
  private BigDecimal vatPercent = BigDecimal.ZERO;
  private BigDecimal vatDivision = BigDecimal.ZERO;
  private BigDecimal arAging1 = BigDecimal.ZERO;
  private BigDecimal arAging2 = BigDecimal.ZERO;
  private BigDecimal arAging3 = BigDecimal.ZERO;
  private BigDecimal arAging4 = BigDecimal.ZERO;
  private BigDecimal apAging1 = BigDecimal.ZERO;
  private BigDecimal apAging2 = BigDecimal.ZERO;
  private BigDecimal apAging3 = BigDecimal.ZERO;
  private BigDecimal apAging4 = BigDecimal.ZERO;
  private String roleCode = "";
  private String roleName = "";
  private String fullName = "";
  private String username = "";
  private String emailAddress = "";
  private String employeeCode = "";
  private String employeeName = "";
  private String defaultBranchCode = "";
  private String defaultBranchName = "";
  private String defaultWarehouseCode = "";
  private String defaultWarehouseName = "";
  private String defaultCashAccountCode = "";
  private String defaultCashAccountName = "";
  private String defaultBankAccountCode = "";
  private String defaultBankAccountName = "";
  private String defaultDivisionCode = "";
  private String defaultDivisionName = "";
  private byte activeStatus = 0;
  private byte superUser = 0;
  private int status = 0;
  private String error = "";
  private String message = "";
}
