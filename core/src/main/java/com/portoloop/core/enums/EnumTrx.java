package com.portoloop.core.enums;

public enum EnumTrx {
    SAVE, UPDATE, DELETE, VIEW, PRINT
}
